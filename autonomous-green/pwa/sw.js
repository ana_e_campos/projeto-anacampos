// Cache on install

var staticCacheName = 'autonomous_cg_ms_2019_07_13_22_37'
this.addEventListener("install", event => {
    this.skipWaiting();

    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll([
                    '/offline',
                    '/assets/manifest.json',
                    '../images/logo-branco.png',
                    '../images/offline.png',
                    '../css/materialize.min.css',
					'../css/style.css',
                    '../js/materialize.min.js',
                    '../js/alert.js'
                ]);
            })
    )
});


// Clear cache on activate
this.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith('autonomous_cg_ms_')))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});


// Serve from Cache
this.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                return response || fetch(event.request);
            })
            .catch(() => {
                return caches.match('/offline');
            })
    )
});