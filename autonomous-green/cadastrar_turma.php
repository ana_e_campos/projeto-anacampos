
<?php
session_start();
if($_SESSION['usuarionivel'] == 2){
      //session_destroy();
  header("Location: comecar_plantio.php"); exit;
}

include ("header.php");
?>

<div class="nav-content">
  <span class="nav-title">  </span>
  <a class="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="listar_turmas.php">
    <i class="material-icons">list</i>
  </a>

</div>
</nav>

<div class="row">
  <div class="col s12 m12">
    <div class="card white">
      <div class="card-action light-green lighten-1">
        <a href="#" style="color:#fff;">Cadastrar Turma</a>
      </div>

      <div class="card-content white-text">
        <div class="row">

          <form class="col s12" method="POST" novalidate action="classes/turma_insert.php">

            <div class="row">
              <div class="input-field col s12">
                <input id="nome" name="nome" type="text" class="validate" required>
                <label for="first_name">Nome/Nº</label>
                <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
              </div>
            </div>

            <button class="btn waves-effect waves-light" type="submit" name="botaoSalvar">Salvar
              <i class="material-icons right">send</i>
            </button>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<?php
include ("footer.php");
?>
