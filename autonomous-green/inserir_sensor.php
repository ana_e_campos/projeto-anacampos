
<?php
session_start();
if($_SESSION['usuarionivel'] == 1){

  header("Location: login.php"); exit;

}
include ("header.php");

?>

<div class="nav-content">
  <span class="nav-title">  </span>


</div>
</nav>

<div class="row">
  <div class="col s12 m12">
    <div class="card white">
      <div class="card-action light-green lighten-1">

        <a href="#" style="color:#fff;">Inserir número do sensor</a>
      </div>



      <div class="card-content white-text">
        <div class="row">

          <form class="col s12" method="POST" novalidate action="classes/plantio_update.php">
            <input type="hidden" class="form-control" name="id" id="id" placeholder="" value="<?php echo $_SESSION['ultimoidcadastrado']; ?>" required>
            <div class="row">
              <div class="input-field col s12">
                <input id="numero_sensor" name="numero_sensor" type="text" class="validate" required>
                <label for="first_name">Nº do sensor</label>
                <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
              </div>
            </div>

            <button class="btn waves-effect waves-light" type="submit" name="botaoAlterar">Próximo
              <i class="material-icons right">send</i>
            </button>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<?php
include ("footer.php");
?>
