<!DOCTYPE html>
<html>
<head>
  <?php
  session_start();
  if(isset($_SESSION['mensagem'])){

    ?>
    <script>
      window.onload = function(){
        M.toast({html: ' <?php echo $_SESSION['mensagem'];?>'});
      }
    </script>

    <?php
  }
  unset($_SESSION['mensagem']);
  ?>

  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>

  <div class="col s12 card center">
    <span class="card-title"> </span>
    
    <div class="card-content">
      <img class="responsive-img" style="max-width:150px;" src="images/logo2.png">
      
      
      <div class="card-content white-text">
        <div class="row">

          <form class="col s12" method="POST" novalidate action="classes/validacao.php">

            <div class="row">
              <div class="input-field col s12">
                <input id="login" name="login" type="text" class="validate">
                <label for="login">Login</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input id="senha" name="senha" type="password" class="validate">
                <label for="senha">senha</label>
              </div>
            </div>


            <button class="btn waves-effect waves-light" type="submit" name="entrar">Logar
              <i class="material-icons right">send</i>
            </button>
          </form>
        </div>
      </div>
    </div>

  </div>


<?php
include ("footer.php");

?>
</body>
</html>

