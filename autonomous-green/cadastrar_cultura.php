
<?php
include ("header.php");
?>

<div class="nav-content">
  <span class="nav-title">  </span>
  <a class="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="listar_culturas.php">
    <i class="material-icons">list</i>
  </a>

</div>
</nav>

<div class="row">
  <div class="col s12 m12">
    <div class="card white">
      <div class="card-action light-green lighten-1">
        <a href="#" style="color:#fff;">Cadastrar Cultura</a>
      </div>

      <div class="card-content white-text">
        <div class="row">

          <form class="col s12" method="POST" novalidate action="classes/cultura_insert.php">

            <div class="row">
              <div class="input-field col s12">
                <input id="nome" name="nome" type="text" class="validate" required>
                <label for="first_name">Nome</label>
                <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
              </div>
            </div>


            <div class="row">
              <div class="input-field col s12">
                <select name="temperatura" required>
                 <option value="">selecione..</option>
                 <option value="1">Baixa</option>
                 <option value="2">Média</option>
                 <option value="3">Alta</option>
               </select>
               <label>Temperatura</label>
               <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
             </div> 
           </div>

            <div class="row">
              <div class="input-field col s12">
                <select name="umidade" required>
                 <option value="">selecione..</option>
                 <option value="1">Seco</option>
                 <option value="2">Umidade moderada</option>
                 <option value="3">Úmido</option>
               </select>
               <label>Solo</label>
               <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
             </div> 
           </div>

          <div class="row">
              <div class="input-field col s12">
                <select name="luminosidade" required>
                 <option value="">selecione..</option>
                 <option value="1">Baixa</option>
                 <option value="2">Média</option>
                 <option value="3">Alta</option>
               </select>
               <label>Luminosidade</label>
               <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
             </div> 
           </div>

    

            <div class="row">
              <div class="input-field col s12">
                <input id="tempo_colheita" name="tempo_colheita" type="text" class="validate" required>
                <label for="text">Tempo aproximado para início da colheita (dias)</label>
                <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
              </div>
            </div>

            <button class="btn waves-effect waves-light" type="submit" name="botaoSalvar">Salvar
              <i class="material-icons right">send</i>
            </button>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<?php
include ("footer.php");
?>
