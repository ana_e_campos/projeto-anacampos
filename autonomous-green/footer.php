
<!--JavaScript at end of body for optimized loading-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<script type="text/javascript" src="js/materialize.min.js"></script>
<script>

 document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.sidenav');
  var instances = M.Sidenav.init(elems, options);
});

  // Or with jQuery

  $(document).ready(function(){
    $('.sidenav').sidenav();
  });

//aqui select form
document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('select');
  var instances = M.FormSelect.init(elems, options);
});

$(document).ready(function(){
  $('select').formSelect();
});
  //fim select form


  //aqui 
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.modal').modal();
    $(".dropdown-trigger").dropdown();
  });


  //inicio campo data

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, options);
    instance.setDate(new Date());
  });

  
  $(document).ready(function() {
    $('#data_inicio').datepicker({

      format: 'dd/mm/yyyy',
      setDefaultDate: true,
      defaultDate: new Date(Date.now()),
//disableWeekends: true,
firstDay: 1,

i18n:{
  cancel: 'Cancelar',
  clear:  'Limpar',
  done: 'Ok',
  previousMonth:  '‹',
  nextMonth:  '›',

  months: 
  [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro'
  ],

  monthsShort:  
  [
  'Jan',
  'Fev',
  'Mar',
  'Abr',
  'Mai',
  'Jun',
  'Jul',
  'Ago',
  'Set',
  'Out',
  'Nov',
  'Dez'
  ],

  weekdays: 
  [
  'Domingo',
  'Segunda',
  'Terça',
  'Quarta',
  'Quinta',
  'Sexta',
  'Sábado'
  ],

  weekdaysShort:
  [
  'Dom',
  'Seg',
  'Ter',
  'Qua',
  'Qui',
  'Sex',
  'Sáb'
  ],

  weekdaysAbbrev: ['D','S','T','Q','Q','S','S']
}
});
  });

          //fim campo data
          document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.slider');
            var instances = M.Slider.init(elems, options);
          });

  // Or with jQuery

  $(document).ready(function(){
    $('.slider').slider();
  });


  //menu bottom
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.tap-target');
    var instances = M.TapTarget.init(elems, options);
  });
  $(document).ready(function(){
    $('.tap-target').tapTarget();
  });
  //fim menu bottom


//tooltip
document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.tooltipped');
  var instances = M.Tooltip.init(elems, options);
});

$(document).ready(function(){
  $('.tooltipped').tooltip();
});
//fim tooltip


 document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.tap-target');
    var instances = M.TapTarget.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.tap-target').tapTarget();
  });


   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.modal').modal();
  });
     


$(document).ready(function(){
//AQUI TROCA IMAGEM AO CLICAR NA TELA
$('#lamp').on('dblclick', function () {

    //var imagem = document.getElementById('lamp').src;
    var imagem = $('#lamp').attr('src');
    var nome_imagem = "<?php print $avatar;?>";
    var nome_pasta = "<?php print $pasta;?>";
    var nome_reacao = "<?php print $reacao;?>";
    var imagem_ligado = 'images/gif/'+nome_pasta+'/'+nome_imagem+nome_reacao+'.gif';
    var imagem_desligado = 'images/gif/'+nome_pasta+'/'+nome_imagem+nome_reacao+'-toque.gif';
    
    if(imagem == imagem_ligado){
      //document.getElementById('lamp').src = imagem_desligado;
      $('#lamp').attr('src', imagem_desligado);



    }
    if(imagem == imagem_desligado){
      //document.getElementById('lamp').src = imagem_ligado;
      $('#lamp').attr('src', imagem_ligado);


    }
   setInterval(function(){
   window.location.reload(1);
}, 1000);
  });

/*function ligarDesliga(){

    var imagem = document.getElementById('lamp').src;
    var imagem_ligado = 'images/cenoura-feliz.png';
    var imagem_desligado = 'images/cenoura-triste.png';
    
    if(imagem == imagem_ligado){
      document.getElementById('lamp').src = imagem_desligado;
    }
    if(imagem == imagem_desligado){
      document.getElementById('lamp').src = imagem_ligado;
    }

  }*/

  var nrImagem = 0;
  var imagens = [];
var refrescar = 1; // mudar imagem de 1 em 1 segundo

// colocar aqui todas as imagens, seguindo a ordem numérica
imagens[0] = "images/dialogo/dialogo3.png";
imagens[1] = "images/dialogo/dialogo.png";
imagens[2] = "images/dialogo/dialogo.png";
imagens[3] = "images/dialogo/dialogo.png";
imagens[4] = "images/dialogo/dialogo3.png";
imagens[5] = "images/dialogo/dialogo.png";
imagens[6] = "images/dialogo/dialogo.png";
imagens[7] = "images/dialogo/dialogo4.png";
imagens[8] = "images/dialogo/dialogo.png";
imagens[9] = "images/dialogo/dialogo.png";
//...

rodarImagens = function () {
 document.images["misto"].src = imagens[nrImagem];

 nrImagem = (nrImagem + 1) % imagens.length; 
}
var intervalControl = setInterval(rodarImagens, 10000 * refrescar);

});

//document.getElementById("lamp").addEventListener("click", ligarDesliga);

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('pwa/sw.js')
  .then(reg => console.info('registered sw', reg))
  .catch(err => console.error('error registering sw', err));
} else {
  console.log('ServiceWorker não é suportado.');
}





</script>





</body>