
<?php
session_start();

if($_SESSION['usuarionivel'] == 1){

  header("Location: login.php"); exit;

}
include ("header.php");
include ("classes/cultura_select.php");
//include ("classes/turma_select.php");

?>
<style>
  
  .white-text {
    color: #666 !important;
}
</style>

<div class="nav-content">
  <span class="nav-title">  </span>


</div>
</nav>

<div class="row">
  <div class="col s12 m12">
    <div class="card white">
      <div class="card-action light-green lighten-1">
        <a href="#" style="color:#fff;">Plantio</a>
      </div>

      <div class="card-content white-text">
        <div class="row">

          <form class="col s12" method="POST" novalidate action="classes/plantio_insert.php">

           <div class="row">
            <div class="input-field col s12">
              <input id="data_inicio" name="data_inicio" type="text" class="datepicker" required > <!--value='<?php //echo date('d/m/Y');?>'-->
              <label for="data_inicio">Início plantio</label>
              <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <select name="idturma" required>

                <?php

                $optTurma = "";
                $i = 0;
                while ($i < count($comboturma))
                {
                  $id = $comboturma[$i]['id'];
                  $nome = $comboturma[$i]['nome'];

                  if($id == $item['idturma'])
                  {
                    $optTurma = $optTurma. "<option value='$id' selected>$nome</option>";
                  }
                  else
                  {
                    $optTurma = $optTurma. "<option value='$id'>$nome</option>";
                  }
                  $i++;
                }
                ?>
                <option value="" disabled>selecione..</option>
                <?php echo $optTurma; ?>

              </select>
              <label>Qual a turma?</label>
              <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
            </div> 
          </div>


          <div class="row">
            <div class="input-field col s12">
              <select name="idcultura" required>

                <?php

                $optCultura = "";
                $i = 0;
                while ($i < count($combocultura))
                {
                  $id = $combocultura[$i]['id'];
                  $nome = $combocultura[$i]['nome'];

                  if($id == $item['idcultura'])
                  {
                    $optCultura = $optCultura. "<option value='$id' selected>$nome</option>";
                  }
                  else
                  {
                    $optCultura = $optCultura. "<option value='$id'>$nome</option>";
                  }
                  $i++;
                }
                ?>
                <option value="" disabled>selecione..</option>
                <?php echo $optCultura; ?>

              </select>
              <label>O que você deseja plantar?</label>
              <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
            </div> 
          </div>

          <button class="btn waves-effect waves-light" type="submit" name="botaoSalvar">Próximo
            <i class="material-icons right">send</i>
          </button>

        </form>
      </div>
    </div>
  </div>
</div>
</div>


<?php
include ("footer.php");
?>
