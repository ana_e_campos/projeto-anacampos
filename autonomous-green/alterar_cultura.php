

<?php


include ("header.php");
include ("classes/cultura_select.php");
?>

<div class="nav-content">
	<span class="nav-title">  </span>
	<a class="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="listar_culturas.php">
		<i class="material-icons">list</i>
	</a>

</div>
</nav>

<div class="row">
	<div class="col s12 m12">
		<div class="card white">
			<div class="card-action light-green lighten-1">
				<a href="#" style="color:#fff;">Alterar Cultura</a>
			</div>

			<div class="card-content white-text">
				<div class="row">

					<form class="col s12" method="POST" novalidate action="classes/cultura_update.php">
						<input type="hidden" class="form-control" name="id" id="id" placeholder="" value="<?php echo $campoalterar[0]['id'] ?>" required>
						<div class="row">
							<div class="input-field col s12">
								<input id="nome" name="nome" type="text" class="validate" value="<?php echo $campoalterar[0]['nome'] ?>" required>
								<label for="first_name">Nome</label>
								<span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="temperatura" name="temperatura" type="text" class="validate" value="<?php echo $campoalterar[0]['temperatura'] ?>" required>
								<label for="temperatura">Temperatura</label>
								<span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="umidade" name="umidade" type="text" class="validate" value="<?php echo $campoalterar[0]['umidade'] ?>" required>
								<label for="umidade">Umidade</label>
								<span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="luminosidade" name="luminosidade" type="text" class="validate" value="<?php echo $campoalterar[0]['luminosidade'] ?>" required>
								<label for="luminosidade">Luminosidade</label>
								<span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="tempo_colheita" name="tempo_colheita" type="text" class="validate" value="<?php echo $campoalterar[0]['tempo_colheita'] ?>" required>
								<label for="tempo_colheita">Tempo aproximado para início da colheita (em dias)</label>
								<span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
							</div>
						</div>

						<button class="btn waves-effect waves-light" type="submit" name="botaoAlterar">Salvar
							<i class="material-icons right">send</i>
						</button>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include ("footer.php");

?>