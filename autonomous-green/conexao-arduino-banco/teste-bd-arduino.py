#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
import serial
comport = serial.Serial('COM8', 9600) 
print 'Serial Iniciada...\n'

import psycopg2
cnx = psycopg2.connect(user='postgres', password='admin10', host='localhost', database='autonomous-green')
cursor = cnx.cursor()
add_sinais = ("INSERT INTO sinais (sin_temperatura,sin_umidade,sin_luminosidade,sin_sensor) VALUES (%s, %s, %s, %s)")

while (True):
  serialValue = comport.readline()
  data_sinais = serialValue.split("|")
  print data_sinais
  cursor.execute(add_sinais, data_sinais)
  cnx.commit()

cursor.close()
cnx.close()
comport.close()