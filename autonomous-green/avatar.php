 <?php

 session_start();
 include ("classes/plantio_select.php");

 if($_SESSION['usuarionivel'] == 1 || $_SESSION['id'] != $linha[0]['idpessoa']){

  header("Location: login.php"); exit;

}

include ("header.php");
include ("clima/examples/5-city.php");


switch ($linha[0]['idcultura']) {
  case 2:
  $avatar = "cenoura-";
  break;
  case 3:
  $avatar = "tomate-";
  break;
  case 4:
  $avatar = "cenoura-";
  break;
  case 5:
  $avatar = "espinafre-";
  break;
  case 6:
  $avatar = "couve-";
  break;
  case 7:
  $avatar = "salsa-";
  break;
}
//$luminosidade = 400;
$luminosidade = $sinais[0]['sin_luminosidade'];
//$temperatura = 300;
$temperatura = $sinais[0]['sin_temperatura'];
//$umidade = 300;
$umidade = $sinais[0]['sin_umidade'];

/////aqui luminosidade
if($luminosidade > 0 && $luminosidade <= 400)
{
	$l = 3; //alta
}
if($luminosidade > 400 && $luminosidade < 700)
{
	$l = 2; //moderada
}
if($luminosidade >= 700)
{
	$l = 1; //baixa
}
///aqui temperatura
if($temperatura > 0 && $temperatura <= 400)
{
	$t = 3;//alta
}
if($temperatura > 400 && $temperatura < 700)
{
	$t = 2; //media
}
if($temperatura >= 700)
{
	$t = 1; //baixa
}


///aqui temperatura
if($umidade > 0 && $umidade <= 400)
{
	$u = 3;
}
if($umidade > 400 && $umidade <= 800)
{
	$u = 2; //moderado
}
if($umidade > 800 && $umidade <= 1024)
{
	$u = 1; ///seco
}


if ($linha[0]['umidade'] == $u)
{
  $aviso_nivel_agua = "Obrigado! Tenho água suficiente para meu crescimento.";


}
elseif($linha[0]['umidade'] != $u)
{
  if ($u == 1)
  {
    $aviso_nivel_agua = "Estou precisando de água!";
    $pulseumidade = "pulse";
    $i++;
  }
  if ($u == 3)
  {
    $aviso_nivel_agua = "Pare de regar, estou encharcada!";
    $pulseumidade = "pulse";
    $i++;
  }
}
else
{
  $aviso_nivel_agua = "erro";
}

if ($linha[0]['luminosidade'] == $l)
{
  $aviso_nivel_luminosidade = "Obrigado! Estou recebendo a luminosidade necessária.";

}
elseif($linha[0]['luminosidade'] != $l)
{
  if ($l == 1)
  {
    $aviso_nivel_luminosidade = "Estou recebendo pouca luminosidade";
    $pulseluminosidade = "pulse";
    $i++;
  }

  if ($l == 3)
  {
    $aviso_nivel_luminosidade = "Estou recebendo muita luminosidade";
    $pulseluminosidade = "pulse";
    $i++;
  }
}
else
{
  $aviso_nivel_luminosidade = "erro";
}
if ($linha[0]['temperatura'] == $t)
{
  $aviso_nivel_temperatura = "Obrigado! Estou recebendo a temperatura necessária.";
  
}
elseif($linha[0]['temperatura'] != $t )
{

  if ($t == 1)
  {
   $aviso_nivel_temperatura = "Estou recebendo temperatura muito baixa";
   $pulsetemperatura = "pulse";
   $i++;
 }
 if ($t == 3)
 {
  $aviso_nivel_temperatura = "Estou recebendo temperatura muito alta";
  $pulsetemperatura = "pulse";
  $i++;
}
else
{
  $aviso_nivel_temperatura = "erro";
}
}

if($i >= 1){
 $reacao = "triste";
}
else{
  $reacao = "feliz";
}



//previsao do tempo

if($_SESSION['clima'] == 1){
  $aviso_chuva = "Previsão de chuva para amanhã. Para evitar o desperdício não preciso ser regada hoje.";
  $pulse = "pulse";

}
if($_SESSION['clima'] == 0){
  $aviso_chuva = "Não há previsão de chuva para amanhã.";
  $pulse = "";

}

if($_SESSION['climahojesol'] === 1){
  $pasta = "sol";
}
if($_SESSION['climahojechuva'] === 1){
  $pasta = "chuva";
}
if($_SESSION['climahojenublado'] === 1){
  $pasta = "passaros";
}
if($_SESSION['climahojegeada'] === 1){
  $pasta = "geada";
}


$dataatual = date('Y-m-d');
$data2 = $linha[0]['data_inicio'];

// converte as datas para o formato timestamp
$d1 = strtotime($dataatual); 
$d2 = strtotime($data2);

// verifica a diferença em segundos entre as duas datas e divide pelo número de segundos que um dia possui
$dataFinal = ($d2 - $d1) /86400;

// caso a data 2 seja menor que a data 1
if($dataFinal < 0)
  $dataFinal = $dataFinal * -1;

if ($linha[0]['tempo_colheita'] <= $dataFinal)
{
  $div_colheita = '';

}
elseif ($linha[0]['tempo_colheita'] >= $dataFinal) {
  $div_colheita = 'display:none';
}


?>

<div class="nav-content">
  <span class="nav-title">  </span>
  <a class="btn-floating btn-large halfway-fab waves-effect cyan darken-3 tooltipped <?php echo $pulse; ?>" data-position="left" data-tooltip="<?php echo $aviso_chuva; ?>">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="26" height="26"
    viewBox="0 0 192 192"
    style=" fill:#000000;margin-top: 15;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,192v-192h192v192z" fill="none"></path><g fill="#ffffff"><g id="surface1"><path d="M114.46154,0c-16.90384,0 -30.92308,9.54808 -39.46154,22.84615c-1.61538,-0.23077 -3.11538,-0.69231 -4.84615,-0.69231c-18.49038,0 -33.23077,12.80769 -38.07692,29.76923c-17.71153,0.63462 -32.07692,15.14423 -32.07692,33c0,18.25961 14.97116,33.23077 33.23077,33.23077h118.15385c22.35577,0 40.61538,-18.25961 40.61538,-40.61538c0,-19.00962 -13.4423,-34.4423 -31.15385,-38.76923c-4.41346,-21.95192 -23.19231,-38.76923 -46.38462,-38.76923zM114.46154,14.76923c17.50962,0 31.61538,13.35577 33,30.46154l0.69231,6.23077l6,0.46154c13.00962,1.38462 23.07692,12.20192 23.07692,25.61538c0,14.36539 -11.48077,25.84615 -25.84615,25.84615h-118.15385c-10.26923,0 -18.46154,-8.19231 -18.46154,-18.46154c0,-10.26923 8.19231,-18.46154 18.46154,-18.46154c0.66346,0 1.47116,0.02884 2.76923,0.23077l7.84615,1.38462l0.69231,-7.84615c1.29808,-13.125 12.14423,-23.30769 25.61538,-23.30769c1.93269,0 3.92308,0.20192 6,0.69231l5.76923,1.38462l2.53846,-5.30769c5.33654,-11.19231 16.70192,-18.92308 30,-18.92308zM154.84615,124.38462c-7.00962,3.63462 -23.07692,9.49038 -27.23077,16.15385c-4.18269,6.66346 -2.27885,15.43269 4.38462,19.61538c6.63462,4.18269 14.91346,1.67308 19.61538,-4.61538c4.18269,-5.59615 3.23077,-16.09615 3.23077,-31.15385zM51.46154,131.76923c-7.00962,3.63462 -23.07692,9.49038 -27.23077,16.15385c-4.18269,6.66346 -2.27885,15.43269 4.38462,19.61538c6.63462,4.18269 14.91346,1.67308 19.61538,-4.61538c4.18269,-5.59615 3.23077,-16.09615 3.23077,-31.15385zM103.15385,153.92308c-7.00962,3.63462 -23.07692,9.49038 -27.23077,16.15385c-4.18269,6.66346 -2.27885,15.43269 4.38462,19.61538c6.63462,4.18269 14.91346,1.67308 19.61538,-4.61538c4.18269,-5.59615 3.23077,-16.09615 3.23077,-31.15385z"></path></g></g></g></svg>
  </a>
</div>



</nav>
<div>

  <div class="">
    <div class="col s12 m12">
      <div class="">

        <div class="card-content white-text" style='position:relative; top:0px; left:0px;'>
          <?php
          echo "<img class='responsive-img' src='images/gif/".$pasta."/".$avatar.$reacao.".gif' id='lamp'>";
          //echo "<img class='responsive-img' src='images/cenoura-triste.png' id='lamp'>";
          ?>
        </div>
        <div style='position:absolute; top:110px; left:6px;'>
          <img id='misto' style=" width: 25%;margin-left: 60%;margin-top: 30%;">
        </div>
        <?php echo "<div style='position:absolute; bottom:30%; left:2px; $div_colheita '>" ?>
        <a  href="#modal1" class="modal-trigger">
          <img src="images/colheita.png" style="width: 20%;margin-left: 50px;">
        </a>
        <!-- Modal Structure -->
        <div id="modal1" class="modal">
          <div class="modal-content">
            <h4>Chegou o melhor momento: a colheita</h4>
            <p>Vocês realizaram um ótimo trabalho por aqui! Estou muito orgulhosa de vocês...</p>
            <p>Mas não acabou por aqui, agora é hora de escolher uma receita e fazer um prato delicioso...</p>
          </div>
          <form class="col s12" method="POST" novalidate action="classes/plantio_finalizar.php">
            <input id="data_fim" name="data_fim" type="hidden" value="<?php echo date('Y-m-d');?>">
            <input id="id" name="id" type="hidden" value="<?php echo $linha[0]['id'];?>">
           
            <div class="modal-footer">
             <button class="btn waves-effect waves-light" type="submit" name="botaoFinalizar">Colher
              <i class="material-icons right">send</i>
            </button>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

<div class="row" >
  <div class="col s12 m12 center">

    <div class="card-content">

      <a class="btn-floating btn-large waves-effect waves-light cyan darken-1 tooltipped <?php echo $pulseumidade; ?>" data-position="top" data-tooltip="<?php echo $aviso_nivel_agua; ?>">
        <i class="material-icons">opacity</i>
      </a>

      <a class="btn-floating btn-large waves-effect waves-light amber darken-2 tooltipped <?php echo $pulseluminosidade; ?>" data-position="top" data-tooltip="<?php echo $aviso_nivel_luminosidade; ?>"><i class="material-icons">brightness_low</i></a>

      <a class="btn-floating btn-large waves-effect waves-light red tooltipped <?php echo $pulsetemperatura; ?>" data-position="top" data-tooltip="<?php echo $aviso_nivel_temperatura; ?>">
        <svg xmlns="http://www.w3.org/2000/svg" style="margin-top: 15;" x="0px" y="0px"
        width="26" height="26"
        viewBox="0 0 192 192"
        style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,192v-192h192v192z" fill="none"></path><g fill="#ffffff"><g id="surface1"><path d="M119.04,106.05v-2.37h11.52c2.13,0 3.84,-1.71 3.84,-3.84c0,-2.115 -1.71,-3.84 -3.84,-3.84h-11.52v-7.68h11.52c2.13,0 3.84,-1.725 3.84,-3.84c0,-2.115 -1.71,-3.84 -3.84,-3.84h-11.52v-7.68h11.52c2.13,0 3.84,-1.725 3.84,-3.84c0,-2.115 -1.71,-3.84 -3.84,-3.84h-11.52v-7.68h11.52c2.13,0 3.84,-1.725 3.84,-3.84c0,-2.115 -1.71,-3.84 -3.84,-3.84h-11.52v-7.68h11.52c2.13,0 3.84,-1.725 3.84,-3.84c0,-2.115 -1.71,-3.84 -3.84,-3.84h-11.52v-15.36c0,-11.67 -9.045,-19.2 -23.04,-19.2c-13.995,0 -23.04,7.53 -23.04,19.2v86.85c-14.265,8.235 -23.04,23.295 -23.04,39.87c0,25.41 20.67,46.08 46.08,46.08c25.41,0 46.08,-20.67 46.08,-46.08c0,-16.56 -8.76,-31.635 -23.04,-39.87zM96,7.68c4.62,0 15.36,1.125 15.36,11.52v15.36h-30.72v-15.36c0,-10.395 10.74,-11.52 15.36,-11.52z"></path></g></g></g></svg>
      </a></div>

    </div>
  </div>
</div>



<?php
include ("footer.php");
?>

