

<?php

include ("header.php");
include ("classes/turma_select.php");
?>

<div class="nav-content">
	<span class="nav-title">  </span>
	<a class="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="listar_turmas.php">
		<i class="material-icons">list</i>
	</a>

</div>
</nav>

<div class="row">
	<div class="col s12 m12">
		<div class="card white">
			<div class="card-action light-green lighten-1">
				<a href="#" style="color:#fff;">Alterar Nome/Nº da turma</a>
			</div>

			<div class="card-content white-text">
				<div class="row">

					<form class="col s12" method="POST" novalidate action="classes/turma_update.php">
						<input type="hidden" class="form-control" name="id" id="id" placeholder="" value="<?php echo $campoalterar[0]['id'] ?>" required>
						<div class="row">
							<div class="input-field col s12">
								<input id="nome" name="nome" type="text" class="validate" value="<?php echo $campoalterar[0]['nome'] ?>" required>
								<label for="first_name">Nome</label>
								<span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
							</div>
						</div>

						<button class="btn waves-effect waves-light" type="submit" name="botaoAlterar">Salvar
							<i class="material-icons right">send</i>
						</button>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include ("footer.php");

?>