<?php

session_start(); 
define( 'PG_HOST', 'localhost' );
define( 'PG_DBNAME', 'autonomous-green' );//autonomous-green
define( 'PG_PORT', '5432' );
define( 'PG_USER', 'postgres' );
define( 'PG_PASSWORD', 'admin10' ); //admin10

class Crud_Pessoa extends PDO
{
    public function __construct(){

    }
    private function conexao(){
        try{
            $PDO = new PDO('pgsql:host='.PG_HOST.';dbname='.PG_DBNAME.';port='. PG_PORT.';user= '.PG_USER.';password='.PG_PASSWORD);
            $PDO->exec("set names utf8");

            return $PDO;
        }
        catch ( PDOException $e ){
            echo 'Erro ao conectar com o PgAdmin: ' . $e->getMessage();
        }
    }


    //uso
 //USUÁRIO E SERVIDOR
    function pessoa_insert($nome,$login,$senha,$nivel){
        try{
            $this->nome = $nome;
            $this->login = $login;
            $this->senha = $senha;
            $this->nivel = $nivel;
            
            $bd = Crud_Pessoa::conexao();
            $sql = "INSERT INTO pessoa(nome, login, senha, nivel) VALUES (:nome,:login,:senha,:nivel)";
            $stmt = $bd->prepare( $sql );
            $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
            $stmt->bindParam(':login', $this->login, PDO::PARAM_STR);
            $stmt->bindParam(':senha', $this->senha, PDO::PARAM_STR);
            $stmt->bindParam(':nivel', $this->nivel, PDO::PARAM_INT);
            $result = $stmt->execute();
            if($result == TRUE)
            {
             $_SESSION['mensagem'] = "Cadastrado com sucesso!";
             $url='../listar_professores.php';
             echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
             exit();
         }
         else{
            $_SESSION['mensagem'] = "Erro ao cadastrar!";
            $url='../listar_professores.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }

    }
    catch ( PDOException $e ){
        echo 'Erro ao inserir: ' . $e->getMessage();
    }    
}

function pessoa_select_table(){
        //$this->id = $id;
    $bd = Crud_Pessoa::conexao();
    $sql = "SELECT p.id as id, p.nome, p.login, p.senha, p.nivel
    FROM pessoa p
    order by id desc"; 
    $stmt = $bd->prepare( $sql );
        //$stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function pessoa_select($id){
    $this->id = $id;
    $bd = Crud_Pessoa::conexao();
    $sql = "SELECT * FROM pessoa where id = :id";
    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function pessoa_update($id,$nome,$login,$senha,$nivel){
    try{
        $this->id = $id;
        $this->nome = $nome;
        $this->login = $login;
        $this->senha = $senha;
        $this->nivel = $nivel;
        $bd = Crud_Pessoa::conexao();
        $sql = "UPDATE pessoa set nome = :nome, login = :login, senha = :senha, nivel = :nivel WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
        $stmt->bindParam(':login',$this->login,PDO::PARAM_STR);
        $stmt->bindParam(':senha',$this->senha,PDO::PARAM_STR);
        $stmt->bindParam(':nivel', $this->nivel,PDO::PARAM_INT);
        $result = $stmt->execute();
        if($result == TRUE)
        {
            $_SESSION['mensagem'] = "Alterado com sucesso!";
            $url='../listar_professores.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }
        else{
            $_SESSION['mensagem'] = "Erro ao alterar!";
            $url='../listar_professores.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}

function pessoa_delete($id){
    $bd = Crud_Pessoa::conexao();

    $sql = "DELETE FROM pessoa WHERE id = :id ";
    $stmt = $bd->prepare($sql);
    $stmt->bindParam(':id',$id, PDO::PARAM_INT);

    $result = $stmt->execute();
    if($result == TRUE){
        $_SESSION['mensagem'] = "Excluído com sucesso!";
        $url='../listar_professores.php';

        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } 
    else{
        $_SESSION['mensagem'] = "Erro ao excluir!";
        $url='../listar_professores.php';
        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } 
}


function login_select($login, $senha){
    $this->login = $login;
    $this->senha = md5($senha);
    $bd = Crud_Pessoa::conexao();

    $sql = "SELECT id, nome, login, senha, nivel FROM pessoa WHERE (login = :login) AND (senha = :senha) LIMIT 1";
    $stmt = $bd->prepare( $sql );

    $login1 = $stmt->bindParam(':login',$this->login, PDO::PARAM_STR);
    $senha2 = $stmt->bindParam(':senha',$this->senha, PDO::PARAM_STR);
    
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

}

function login_plantio_select($id){
    $this->id = $id;
    $bd = Crud_Pessoa::conexao();
    $sql = "SELECT * FROM plantio p
    INNER JOIN plantio_pessoa pp on p.id = pp.idplantio
    WHERE p.data_fim is null and idpessoa = :id"; 

    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

}

function login_select2($login, $senha){
    $this->login = $login;
    $this->senha = md5($senha);
    $bd = Crud_Pessoa::conexao();

    $sql = "SELECT id, nome, login, senha, nivel FROM pessoa WHERE (login = :login) AND (senha = :senha) LIMIT 1";
    $stmt = $bd->prepare( $sql );

    $login1 = $stmt->bindParam(':login',$this->login, PDO::PARAM_STR);
    $senha2 = $stmt->bindParam(':senha',$this->senha, PDO::PARAM_STR);
    
    /*$stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $_SESSION['id2'] = $result['id'];
    $id = $_SESSION['id2'];*/
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $_SESSION['id2'] = $result['id'];
    $id = $_SESSION['id2'];

    /**$stmt = $bd->prepare( $sql );
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);**/
    //$resultado = $stmt->execute();

    //$count = $stmt->rowCount();  

    $bd2 = Crud_Pessoa::conexao();
    $sql2 = "SELECT * FROM plantio p
    INNER JOIN plantio_pessoa pp on p.id = pp.idplantio
    WHERE p.data_fim is null and idpessoa = $id";
    $stmt2 = $bd2->prepare( $sql2 );
    $stmt2->execute();
    $_SESSION['count'] = count($sql2);



    if($stmt->rowCount() == 1)
    {

      while($ln = $stmt->fetch(PDO::FETCH_ASSOC))
      {

        $_SESSION['login'] = $ln['login'];
        $_SESSION['usuarionivel'] = $ln['nivel'];
        $_SESSION['usuarioLogin'] = $ln['login'];
        $_SESSION['usuarioNome'] = $ln['nome'];
        $_SESSION['id'] = $ln['id'];
        //if($_SESSION['usuarioNiveisAcessoId'] == "1"){
            //header("Location: administrativo.php");
        //}elseif($_SESSION['usuarioNiveisAcessoId'] == "2"){
            //header("Location: colaborador.php");
        //}else{
       // header("Location: agenda.php");
        if($_SESSION['usuarionivel'] == 1){
            $url='../default.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();

        }
        if($_SESSION['usuarionivel'] == 2){

            if($_SESSION['count'] == 1){

                $url='../avatar.php';
                echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
                exit();
            }

            else{
                $url='../inserir_plantio.php';
                echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
                exit();
            }
        }

        else{
            $url='../.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }
    };
        //}
        //Não foi encontrado um usuario na tabela usuário com os mesmos dados digitado no formulário
        //redireciona o usuario para a página de login
}
else
{    
    $_SESSION['mensagem'] = "Não foi possível realizar o login! Verificar login e/ou senha.";
        //header("Location: login.php");
        $url='../login.php'; //.$senha.'&'.$login
        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();

    }


}


    //fim uso








    //PESSOA
function servidor_select_all(){
    $bd = Crud_Pessoa::conexao();
    $sql = "SELECT * FROM pessoa";
    $result = $bd->query($sql);
    return $result->fetchAll(PDO::FETCH_ASSOC);
}


function servidor_select_ultimo(){
    $bd = Crud_Pessoa::conexao();
    $sql = "SELECT id FROM pessoa ORDER BY id DESC LIMIT 1";
    $stmt = $bd->prepare( $sql );
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}



function select_servidor_combo(){
    $bd = Crud_Pessoa::conexao();
    $sql = "SELECT * FROM pessoa order by nome";
    $stmt = $bd->prepare( $sql );
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

}

function servidor_update($id,$nome,$telefone,$cpf,$email,$login,$senha,$tipopessoa,$idsetor,$nivel){
    try{
        $this->id = $id;
        $this->nome = $nome;
        $this->telefone = $telefone;
        $this->cpf = $cpf;
        $this->email = $email;
        $this->login = $login;
        $this->senha = $senha;
        $this->tipopessoa = $tipopessoa;
        $this->idsetor = $idsetor;
        $this->nivel = $nivel;
        $bd = Crud_Pessoa::conexao();
        $sql = "UPDATE pessoa set nome = :nome, telefone = :telefone, cpf = :cpf, email = :email, login = :login, senha = :senha, tipopessoa = :tipopessoa, idsetor = :idsetor, nivel = :nivel WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
        $stmt->bindParam(':telefone',$this->telefone,PDO::PARAM_STR);
        $stmt->bindParam(':cpf',$this->cpf,PDO::PARAM_STR);
        $stmt->bindParam(':email',$this->email,PDO::PARAM_STR);
        $stmt->bindParam(':login',$this->login,PDO::PARAM_STR);
        $stmt->bindParam(':senha',$this->senha,PDO::PARAM_STR);
        $stmt->bindParam(':tipopessoa', $this->tipopessoa, PDO::PARAM_INT);
        $stmt->bindParam(':idsetor', $this->idsetor,PDO::PARAM_INT);
        $stmt->bindParam(':nivel', $this->nivel,PDO::PARAM_INT);
        $result = $stmt->execute();
        if($result == TRUE)
        {
            $url='../listar_servidores.php?alt';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}




function servidor_delete($id){
    $bd = Crud_Pessoa::conexao();

    $sql = "DELETE FROM pessoa WHERE id = :id ";
    $stmt = $bd->prepare($sql);
    $stmt->bindParam(':id',$id, PDO::PARAM_INT);

    $result = $stmt->execute();
    if($result == TRUE){

        $url='../listar_servidores.php?excluir';

        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } 
}

function servidor_setor_vinculo($id){
    $this->id = $id;
    $bd = Crud_Pessoa::conexao();
    $sql = "SELECT * from pessoa where idsetor = :id"; 

    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function login_select_troca_senha($login, $senha, $id){
    $this->login = $login;
    $this->senha = $senha;
    $this->id = $id;
    $bd = Crud_Pessoa::conexao();

    $sql = "SELECT login, senha, id FROM pessoa WHERE (login = :login) AND (senha = :senha) AND (id = :id) LIMIT 1";
    $stmt = $bd->prepare( $sql );

    $login1 = $stmt->bindParam(':login',$this->login, PDO::PARAM_STR);
    $senha1 = $stmt->bindParam(':senha',$this->senha, PDO::PARAM_STR);
    $id = $stmt->bindParam(':id',$this->id, PDO::PARAM_STR);

    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

}






}  

class Crud_Setor extends PDO{
    public function __construct(){

    }
    private function conexao(){
        try{
            $PDO = new PDO('pgsql:host='.PG_HOST.';dbname='.PG_DBNAME.';port='. PG_PORT.';user= '.PG_USER.';password='.PG_PASSWORD);
            $PDO->exec("set names utf8");
            return $PDO;
        }
        catch ( PDOException $e ){
            echo 'Erro ao conectar com o PgAdmin: ' . $e->getMessage();
        }
    }

    function setor_insert($nome){
        try{
            $this->nome = $nome;

            $bd = Crud_Setor::conexao();
            $sql = "INSERT INTO setor(nome) VALUES (:nome)";
            $stmt = $bd->prepare( $sql );
            $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);

            $result = $stmt->execute();
            if($result == TRUE)
            {
             $url='../listar_setor.php?cad';
             echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
             exit();
         }

     }
     catch ( PDOException $e ){
        echo 'Erro ao inserir: ' . $e->getMessage();
    }    
}
function setor_update($id,$nome){
    try{
        $this->id = $id;
        $this->nome = $nome;
        $bd = Crud_Setor::conexao();
        $sql = "UPDATE setor set nome = :nome WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);

        $result = $stmt->execute();
        if($result == TRUE)
        {
            $url='../listar_setor.php?alt';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}

function setor_delete($id){
    $bd = Crud_Setor::conexao();

    $sql = "DELETE FROM setor WHERE id = :id ";
    $stmt = $bd->prepare($sql);
    $stmt->bindParam(':id',$id, PDO::PARAM_INT);

    $result = $stmt->execute();
    if($result == TRUE){

        $url='../listar_setor.php?excluir';

        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } 
}  

function setor_select_table(){
        //$this->id = $id;
    $bd = Crud_Setor::conexao();
    $sql = " SELECT * from setor
    where nome != 'Ninguém'
    order by id desc"; 

    $stmt = $bd->prepare( $sql );
        //$stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function setor_select_ultimo(){
    $bd = Crud_Setor::conexao();
    $sql = "SELECT id FROM setor ORDER BY id DESC LIMIT 1";
    $stmt = $bd->prepare( $sql );
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function setor_select($id){
    $this->id = $id;
    $bd = Crud_Setor::conexao();
    $sql = "SELECT * from setor where id = :id";

    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function select_setor_combo(){
    $bd = Crud_Setor::conexao();
    $sql = "SELECT * FROM setor order by nome";
    $stmt = $bd->prepare( $sql );
        //$stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

}
}





class Crud_Evento extends PDO{
    public function __construct(){

    }
    private function conexao(){
        try{
            $PDO = new PDO('pgsql:host='.PG_HOST.';dbname='.PG_DBNAME.';port='. PG_PORT.';user= '.PG_USER.';password='.PG_PASSWORD);
            $PDO->exec("set names utf8");
            return $PDO;
        }
        catch ( PDOException $e ){
            echo 'Erro ao conectar com o PgAdmin: ' . $e->getMessage();
        }
    }


    function evento_insert($nome,$idcor,$datainicio,$datafim){
        try{
            $this->nome = $nome;
            $this->idcor = $idcor;
            $this->datainicio = $datainicio;
            $this->datafim = $datafim;
            $this->status = 1;

            
            $bd = Crud_Evento::conexao();
            $sql = "INSERT INTO evento(nome, idcor, datainicio,datafim,status) VALUES (:nome,:idcor,:datainicio,:datafim,:status)";
            $stmt = $bd->prepare( $sql );
            $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
            $stmt->bindParam(':idcor',$this->idcor,PDO::PARAM_INT);
            $stmt->bindParam(':datainicio', $this->datainicio, PDO::PARAM_INT);
            $stmt->bindParam(':datafim', $this->datafim, PDO::PARAM_INT);
            $stmt->bindParam(':status', $this->status, PDO::PARAM_INT);

            $result = $stmt->execute();

            /*if($result == TRUE)
            {

              $this->idevento = $idevento;
              $this->idpessoa = $campos;

              $bd2 = Crud_Evento::conexao();
              $sql2 = "INSERT INTO evento_pessoa(idevento, idpessoa) VALUES (:idevento,:idpessoa)";
              $stmt2 = $bd2->prepare( $sql2 );
              $stmt2->bindParam(':idevento',$this->idevento,PDO::PARAM_INT);
              $stmt2->bindParam(':idpessoa',$this->idpessoa,PDO::PARAM_INT);
              $result2 = $stmt2->execute();
              if($result2 == TRUE)
              {

                  $url='../agenda.php?cad';
                  echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
                  exit();
              }
          }*/

      }
      catch ( PDOException $e ){
        echo 'Erro ao inserir: ' . $e->getMessage();
    }    
}


function evento_insert2($nome, $datainicio,$datafim,$cor){
    try{
        $this->nome = $nome;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->idcor = $cor;
        $this->status = 1;

        $bd = Crud_Evento::conexao();
        $sql = "INSERT INTO evento(nome, idcor, datainicio,datafim,status) VALUES (:nome,:idcor,:datainicio,:datafim,:status)";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
        $stmt->bindParam(':datainicio', $this->datainicio, PDO::PARAM_INT);
        $stmt->bindParam(':datafim', $this->datafim, PDO::PARAM_INT);
        $stmt->bindParam(':idcor', $this->idcor, PDO::PARAM_STR);
        $stmt->bindParam(':status', $this->status, PDO::PARAM_INT);

        $result = $stmt->execute();

   /*     if($result == TRUE)
        {


          $url='../agenda.php?cad';
          echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
          exit();
      }*/


  }
  catch ( PDOException $e ){
    echo 'Erro ao inserir: ' . $e->getMessage();
}    
}

function evento_pessoa_insert($idevento, $idpessoa){
    try{
        $this->idevento = $idevento;
        $this->idpessoa = $idpessoa;

        $bd = Crud_Evento::conexao();
        $sql = "INSERT INTO evento_pessoa(idevento, idpessoa) VALUES (:idevento,:idpessoa)";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':idevento',$this->idevento,PDO::PARAM_INT);
        $stmt->bindParam(':idpessoa',$this->idpessoa,PDO::PARAM_INT);
        $result = $stmt->execute();


    }
    catch ( PDOException $e ){
        echo 'Erro ao inserir: ' . $e->getMessage();
    }    
}
//deletar evento pessoa quando alterar evento
function evento_pessoa_delete($idevento){

    $this->idevento = $idevento;
    $this->status = 2;
    $bd = Crud_Evento::conexao();
    $sql = "UPDATE evento set status = :status WHERE idevento = :idevento";
    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':idevento',$this->idevento,PDO::PARAM_INT);
    $stmt->bindParam(':status', $this->status, PDO::PARAM_INT);

    

    $bd = Crud_Evento::conexao();

    $sql = "DELETE FROM evento_pessoa WHERE idevento = :idevento ";
    $stmt = $bd->prepare($sql);
    $stmt->bindParam(':idevento',$idevento, PDO::PARAM_INT);

    $result = $stmt->execute();

}  

function evento_update($id,$nome,$idcor,$datainicio,$datafim){
    try{
        $this->id = $id;
        $this->nome = $nome;
        $this->idcor = $idcor;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $bd = Crud_Evento::conexao();
        $sql = "UPDATE evento set nome = :nome, idcor = :idcor, datainicio = :datainicio, datafim = :datafim WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
        $stmt->bindParam(':idcor',$this->idcor,PDO::PARAM_INT);
        $stmt->bindParam(':datainicio', $this->datainicio, PDO::PARAM_INT);
        $stmt->bindParam(':datafim', $this->datafim, PDO::PARAM_INT);

        $result = $stmt->execute();
        //if($result == TRUE)
        //{
           // $url='../agenda.php?alt';
           // echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
           // exit();
       // } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}

function evento_update2($id,$datainicio,$datafim){
    try{
        $this->id = $id;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $bd = Crud_Evento::conexao();
        $sql = "UPDATE evento set datainicio = :datainicio, datafim = :datafim WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':datainicio', $this->datainicio, PDO::PARAM_INT);
        $stmt->bindParam(':datafim', $this->datafim, PDO::PARAM_INT);

        $result = $stmt->execute();
        //if($result == TRUE)
        //{
         //   $url='../agenda.php';
         //   echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
         //   exit();
       // } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}

function evento_delete($id){

    try{
        $this->id = $id;
        $this->status = 2;
        $bd = Crud_Evento::conexao();
        $sql = "UPDATE evento set status = :status WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':status', $this->status, PDO::PARAM_INT);

        $result = $stmt->execute();
        //if($result == TRUE)
        //{
         //   $url='../agenda.php?excluir';
         //   echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
         //  exit();
        //} 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao excluir: ' . $e->getMessage();
    }   
   /* $bd = Crud_Evento::conexao();

    $sql = "DELETE FROM evento WHERE id = :id";
    $stmt = $bd->prepare($sql);
    $stmt->bindParam(':id',$id, PDO::PARAM_INT);

    $result = $stmt->execute();
    if($result == TRUE){

        $url='../agenda.php?excluir';

        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } */
}  

function evento_delete_minha_agenda($id){

    try{
        $this->id = $id;
        $this->status = 2;
        $bd = Crud_Evento::conexao();
        $sql = "UPDATE evento set status = :status WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':status', $this->status, PDO::PARAM_INT);

        $result = $stmt->execute();
        if($result == TRUE)
        {
            $url='../minha_agenda.php?excluir';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao excluir: ' . $e->getMessage();
    }   
/*    $bd = Crud_Evento::conexao();

    $sql = "DELETE FROM evento WHERE id = :id ";
    $stmt = $bd->prepare($sql);
    $stmt->bindParam(':id',$id, PDO::PARAM_INT);

    $result = $stmt->execute();
    if($result == TRUE){

        $url='../minha_agenda.php?excluir';

        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    }*/ 
}  

function evento_select_table(){
        //$this->id = $id;
    $bd = Crud_Evento::conexao();
    $sql = "SELECT e.id, e.nome, e.idcor, e.datainicio, e.datafim, e.status,
    array_to_string(array_agg(p.nome), ', ') as nomepessoa,
    array_to_string(array_agg(p.id), ', ') as idpessoa 



    from evento e
    left join evento_pessoa ep on e.id = ep.idevento
    left join pessoa p on ep.idpessoa = p.id
    where e.status = 1
    group by e.id, e.idcor, e.datainicio, e.datafim, e.nome, e.status
    ORDER BY E.ID DESC


    "; 



    



//RECORRENCIA
/*
SELECT e.id, e.nome, e.idcor, e.datainicio, e.datafim, e.status,
    array_to_string(array_agg(p.nome), ', ') as nomepessoa,
    array_to_string(array_agg(p.id), ', ') as idpessoa 



    from evento e
    left join evento_pessoa ep on e.id = ep.idevento
    left join pessoa p on ep.idpessoa = p.id
    where e.status = 1
    group by e.id, e.idcor, e.datainicio, e.datafim, e.nome, e.status
    ORDER BY E.ID DESC



SELECT e.id, e.nome, e.idcor, e.status,
    array_to_string(array_agg(p.nome), ', ') as nomepessoa,
    array_to_string(array_agg(p.id), ', ') as idpessoa, 

GENERATE_SERIES(datainicio, '2025-01-01'::timestamptz, '1 years'::INTERVAL) as datainicio,
GENERATE_SERIES(datafim, '2025-01-01'::timestamptz, '1 years'::INTERVAL) as datafim

    from evento e
    left join evento_pessoa ep on e.id = ep.idevento
    left join pessoa p on ep.idpessoa = p.id
    where e.status = 1
    group by e.id, e.idcor, e.datainicio, e.datafim, e.nome, e.status
    ORDER BY E.ID DESC*/


    $stmt = $bd->prepare( $sql );
        //$stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function evento_pessoa_vinculo($id){
    $this->id = $id;
    $bd = Crud_Evento::conexao();
    $sql = "SELECT * from evento_pessoa where idpessoa = :id"; 

    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function ultimo_evento_cadastrado_select(){
        //$this->id = $id;
    $bd = Crud_Evento::conexao();
    $sql = " SELECT * from evento order by id desc limit 1"; 

    $stmt = $bd->prepare( $sql );
        //$stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function evento_pessoas_minha_agenda($idpessoa){
    $this->idpessoa = $idpessoa;
    $bd = Crud_Evento::conexao();
    $sql = "SELECT distinct (idpessoa), idevento
    FROM evento_pessoa
    WHERE idevento IN ( SELECT idevento
    FROM evento_pessoa
    WHERE idpessoa = $idpessoa and status = 1)"; 

    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':idpessoa',$this->idpessoa, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}



function evento_select_table_minhaagenda($idpessoa){
    $this->idpessoa = $idpessoa;
    $bd = Crud_Evento::conexao();
    $sql = "SELECT e.id, e.nome, e.idcor, e.datainicio, e.datafim,
    array_to_string(array_agg(p.nome), ', ') as nomepessoa,
    array_to_string(array_agg(p.id), ', ') as idpessoa 


    from evento e
    left join evento_pessoa ep on e.id = ep.idevento
    left join pessoa p on ep.idpessoa = p.id
    where idpessoa = :idpessoa and status = 1
    group by e.id, e.idcor, e.datainicio, e.datafim, e.nome
    ORDER BY E.ID DESC";

    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':idpessoa',$this->idpessoa, PDO::PARAM_INT);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function evento_select_ultimo(){
    $bd = Crud_Evento::conexao();
    $sql = "SELECT id FROM evento ORDER BY id DESC LIMIT 1";
    $stmt = $bd->prepare( $sql );
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function evento_select($id){
    $this->id = $id;
    $bd = Crud_Evento::conexao();
    $sql = "SELECT ep.idpessoa from evento e 
    inner join evento_pessoa ep on e.id = ep.idevento
    where e.id = :id";

    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
    
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function cor_combo(){
    $bd = Crud_Evento::conexao();
    $sql = "SELECT * FROM cor order by nome";
    $stmt = $bd->prepare( $sql );
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

}

}



class Crud_Log extends PDO{
    public function __construct(){

    }
    private function conexao(){
        try{
            $PDO = new PDO('pgsql:host='.PG_HOST.';dbname='.PG_DBNAME.';port='. PG_PORT.';user= '.PG_USER.';password='.PG_PASSWORD);
            $PDO->exec("set names utf8");
            return $PDO;
        }
        catch ( PDOException $e ){
            echo 'Erro ao conectar com o PgAdmin: ' . $e->getMessage();
        }
    }


    function log_insert($idevento, $idpessoa, $dataatualizacao, $operacao){
        try{
            $this->idevento = $idevento;
            $this->idpessoa = $idpessoa;
            $this->dataatualizacao = $dataatualizacao;
            $this->operacao = $operacao;

            $bd = Crud_Log::conexao();
            $sql = "INSERT INTO log(idevento, idpessoa, dataatualizacao, operacao) VALUES (:idevento, :idpessoa, :dataatualizacao, :operacao)";
            $stmt = $bd->prepare( $sql );
            $stmt->bindParam(':idevento',$this->idevento,PDO::PARAM_INT);
            $stmt->bindParam(':idpessoa',$this->idpessoa,PDO::PARAM_INT);
            $stmt->bindParam(':dataatualizacao',$this->dataatualizacao,PDO::PARAM_STR);
            $stmt->bindParam(':operacao',$this->operacao,PDO::PARAM_STR);

            $result = $stmt->execute();

        }
        catch ( PDOException $e ){
            echo 'Erro ao inserir: ' . $e->getMessage();
        }    
    }

    function log_select_table(){
        //$this->id = $id;
        $bd = Crud_Log::conexao();
        $sql = "SELECT l.id, l.idevento, l.idpessoa, l.dataatualizacao, l.operacao, e.nome as nomeevento, p.nome as nomepessoa from log l
        left join evento e on e.id = l.idevento
        left join pessoa p on p.id = l.idpessoa
        order by id desc"; 
        $stmt = $bd->prepare( $sql );
        //$stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
        $result = $stmt->execute();
        return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    function log_select_ultimo(){
        $bd = Crud_Log::conexao();
        $sql = "SELECT id FROM log ORDER BY id DESC LIMIT 1";
        $stmt = $bd->prepare( $sql );
        $result = $stmt->execute();
        return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }



}



///////aquiii começa cultura
class Crud_Cultura extends PDO{
    public function __construct(){

    }
    private function conexao(){
        try{
            $PDO = new PDO('pgsql:host='.PG_HOST.';dbname='.PG_DBNAME.';port='. PG_PORT.';user= '.PG_USER.';password='.PG_PASSWORD);
            $PDO->exec("set names utf8");
            return $PDO;
        }
        catch ( PDOException $e ){
            echo 'Erro ao conectar com o PgAdmin: ' . $e->getMessage();
        }
    }

    function cultura_select_combo(){
        $bd = Crud_Cultura::conexao();
        $sql = "SELECT * FROM cultura order by nome";
        $stmt = $bd->prepare( $sql );
        $result = $stmt->execute();
        return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    }



    function cultura_insert($nome,$temperatura,$umidade,$luminosidade,$tempo_colheita){
        try{

            $this->nome = $nome;
            $this->temperatura = $temperatura;
            $this->umidade = $umidade;
            $this->luminosidade = $luminosidade;
            $this->tempo_colheita = $tempo_colheita;

            $bd = Crud_Cultura::conexao();
            $sql = "INSERT INTO cultura(nome, temperatura, umidade, luminosidade,tempo_colheita) VALUES (:nome,:temperatura,:umidade,:luminosidade,:tempo_colheita)";
            $stmt = $bd->prepare( $sql );
            $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
            $stmt->bindParam(':temperatura',$this->temperatura,PDO::PARAM_STR);
            $stmt->bindParam(':umidade',$this->umidade,PDO::PARAM_STR);
            $stmt->bindParam(':luminosidade', $this->luminosidade,PDO::PARAM_INT);
            $stmt->bindParam(':tempo_colheita', $this->tempo_colheita,PDO::PARAM_INT);
            $result = $stmt->execute();
            if($result == TRUE)
            {
               $_SESSION['mensagem'] = "Cadastrado com sucesso!";
               $url='../listar_culturas.php';
               echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
               exit();
           }
           else{
            $_SESSION['mensagem'] = "Erro ao cadastrar!";
            $url='../listar_culturas.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }

    }
    catch ( PDOException $e ){
        echo 'Erro ao inserir: ' . $e->getMessage();
    }    
}

function cultura_select_table(){
        //$this->id = $id;
    $bd = Crud_Cultura::conexao();
    $sql = "SELECT c.id as id, c.nome, c.temperatura, c.umidade, c.luminosidade, c.tempo_colheita
    FROM cultura c
    order by id desc"; 
    $stmt = $bd->prepare( $sql );
        //$stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function cultura_select($id){
    $this->id = $id;
    $bd = Crud_Cultura::conexao();
    $sql = "SELECT * FROM cultura where id = :id";
    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function cultura_update($id,$nome,$temperatura,$umidade,$luminosidade,$tempo_colheita){
    try{
        $this->id = $id;
        $this->nome = $nome;
        $this->temperatura = $temperatura;
        $this->umidade = $umidade;
        $this->luminosidade = $luminosidade;
        $this->tempo_colheita = $tempo_colheita;
        $bd = Crud_Cultura::conexao();
        $sql = "UPDATE cultura set nome = :nome, temperatura = :temperatura, umidade = :umidade, luminosidade = :luminosidade, tempo_colheita = :tempo_colheita WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
        $stmt->bindParam(':temperatura',$this->temperatura,PDO::PARAM_STR);
        $stmt->bindParam(':umidade',$this->umidade,PDO::PARAM_STR);
        $stmt->bindParam(':luminosidade', $this->luminosidade,PDO::PARAM_INT);
        $stmt->bindParam(':tempo_colheita', $this->tempo_colheita,PDO::PARAM_INT);
        $result = $stmt->execute();
        if($result == TRUE)
        {
            $_SESSION['mensagem'] = "Alterado com sucesso!";
            $url='../listar_culturas.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }
        else{
            $_SESSION['mensagem'] = "Erro ao alterar!";
            $url='../listar_culturas.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}

function cultura_delete($id){
    $bd = Crud_Cultura::conexao();

    $sql = "DELETE FROM cultura WHERE id = :id ";
    $stmt = $bd->prepare($sql);
    $stmt->bindParam(':id',$id, PDO::PARAM_INT);

    $result = $stmt->execute();
    if($result == TRUE){
        $_SESSION['mensagem'] = "Excluído com sucesso!";
        $url='../listar_culturas.php';

        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } 
    else{
        $_SESSION['mensagem'] = "Erro ao excluir!";
        $url='../listar_culturas.php';
        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } 
}



}


///fim cultura



///////aquiii começa turma
class Crud_Turma extends PDO{
    public function __construct(){

    }
    private function conexao(){
        try{
            $PDO = new PDO('pgsql:host='.PG_HOST.';dbname='.PG_DBNAME.';port='. PG_PORT.';user= '.PG_USER.';password='.PG_PASSWORD);
            $PDO->exec("set names utf8");
            return $PDO;
        }
        catch ( PDOException $e ){
            echo 'Erro ao conectar com o PgAdmin: ' . $e->getMessage();
        }
    }

    function turma_select_combo(){
        $bd = Crud_Turma::conexao();
        $sql = "SELECT * FROM turma order by nome";
        $stmt = $bd->prepare( $sql );
        $result = $stmt->execute();
        return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    }



    function turma_insert($nome){
        try{

            $this->nome = $nome;

            $bd = Crud_Turma::conexao();
            $sql = "INSERT INTO turma(nome) VALUES (:nome)";
            $stmt = $bd->prepare( $sql );
            $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);
            $result = $stmt->execute();
            if($result == TRUE)
            {
               $_SESSION['mensagem'] = "Cadastrado com sucesso!";
               $url='../listar_turmas.php';
               echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
               exit();
           }
           else{
            $_SESSION['mensagem'] = "Erro ao cadastrar!";
            $url='../listar_turmas.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }

    }
    catch ( PDOException $e ){
        echo 'Erro ao inserir: ' . $e->getMessage();
    }    
}

function turma_select_table(){
        //$this->id = $id;
    $bd = Crud_Turma::conexao();
    $sql = "SELECT t.id as id, t.nome
    FROM turma t
    order by id desc"; 
    $stmt = $bd->prepare( $sql );
        //$stmt->bindParam(':id',$this->id, PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function turma_select($id){
    $this->id = $id;
    $bd = Crud_Turma::conexao();
    $sql = "SELECT * FROM turma where id = :id";
    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function turma_update($id,$nome){
    try{
        $this->id = $id;
        $this->nome = $nome;
        $bd = Crud_Turma::conexao();
        $sql = "UPDATE turma set nome = :nome WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':nome',$this->nome,PDO::PARAM_STR);

        $result = $stmt->execute();
        if($result == TRUE)
        {
            $_SESSION['mensagem'] = "Alterado com sucesso!";
            $url='../listar_turmas.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }
        else{
            $_SESSION['mensagem'] = "Erro ao alterar!";
            $url='../listar_turmas.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}

function turma_delete($id){
    $bd = Crud_Turma::conexao();

    $sql = "DELETE FROM turma WHERE id = :id ";
    $stmt = $bd->prepare($sql);
    $stmt->bindParam(':id',$id, PDO::PARAM_INT);

    $result = $stmt->execute();
    if($result == TRUE){
        $_SESSION['mensagem'] = "Excluído com sucesso!";
        $url='../listar_turmas.php';

        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } 
    else{
        $_SESSION['mensagem'] = "Erro ao excluir!";
        $url='../listar_turmas.php';
        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    } 
}



}


///fim turma

///////aquiii começa plantio
class Crud_Sinais extends PDO{
    public function __construct(){

    }
    private function conexao(){
        try{
            $PDO = new PDO('pgsql:host='.PG_HOST.';dbname='.PG_DBNAME.';port='. PG_PORT.';user= '.PG_USER.';password='.PG_PASSWORD);
            $PDO->exec("set names utf8");
            return $PDO;
        }
        catch ( PDOException $e ){
            echo 'Erro ao conectar com o PgAdmin: ' . $e->getMessage();
        }
    }

    function sinais_select_table($idsinais){
      $this->idsinais = $idsinais;
      $bd = Crud_Sinais::conexao();
      $sql = "select * from sinais where sin_sensor = :idsinais order by sin_id desc limit 1";
      $stmt = $bd->prepare( $sql );
      $stmt->bindParam(':idsinais',$this->idsinais, PDO::PARAM_STR);
      $result = $stmt->execute();
      return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


  }

}

///////aquiii começa plantio
class Crud_Plantio extends PDO{
    public function __construct(){

    }
    private function conexao(){
        try{
            $PDO = new PDO('pgsql:host='.PG_HOST.';dbname='.PG_DBNAME.';port='. PG_PORT.';user= '.PG_USER.';password='.PG_PASSWORD);
            $PDO->exec("set names utf8");
            return $PDO;
        }
        catch ( PDOException $e ){
            echo 'Erro ao conectar com o PgAdmin: ' . $e->getMessage();
        }
    }

    

    function plantio_insert($data_inicio,$idcultura,$idturma){
        try{

            $this->data_inicio = $data_inicio;
            $this->idcultura = $idcultura;
            $this->idturma = $idturma;


            $bd = Crud_Plantio::conexao();
            $sql = "INSERT INTO plantio(data_inicio, idcultura,idturma) VALUES (:data_inicio,:idcultura,:idturma)";
            $stmt = $bd->prepare( $sql );
            $stmt->bindParam(':data_inicio',$this->data_inicio,PDO::PARAM_STR);
            $stmt->bindParam(':idcultura',$this->idcultura,PDO::PARAM_STR);
            $stmt->bindParam(':idturma',$this->idturma,PDO::PARAM_STR);

            $result = $stmt->execute();
            $_SESSION['ultimoidcadastrado'] = $bd->lastInsertId();
            

            if($result == TRUE)
            {
             $this->idpessoa = $_SESSION['id'];
             $this->idplantio = $_SESSION['ultimoidcadastrado'];
             $bd2 = Crud_Plantio::conexao();
             $sql2 = "INSERT INTO plantio_pessoa(idplantio, idpessoa) VALUES (:idplantio,:idpessoa)";
             $stmt2 = $bd2->prepare( $sql2 );
             $stmt2->bindParam(':idplantio',$this->idplantio,PDO::PARAM_INT);
             $stmt2->bindParam(':idpessoa',$this->idpessoa,PDO::PARAM_INT);
             $result2 = $stmt2->execute();
             if($result2 == TRUE)
             {
              $_SESSION['mensagem'] = "Cadastrado com sucesso!";
              $url='../inserir_sensor.php';
              echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
              exit();
          }

      }
      else{
        $_SESSION['mensagem'] = "Erro ao cadastrar!";
        $url='../tela_erro.php';
        echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
        exit();
    }

}
catch ( PDOException $e ){
    echo 'Erro ao inserir: ' . $e->getMessage();
}    
}

function plantio_select_table($id){
  $this->id = $id;
  $bd = Crud_Plantio::conexao();
  $sql = "SELECT pl.id as idplantio, pl.data_inicio, pl.idcultura, pl.idturma, pl.numero_sensor, 
  pl.data_fim, pp.id as idplantiopessoa, pp.idpessoa, p.nome, c.nome as nome_cultura, c.temperatura, c.umidade, c.luminosidade, c.tempo_colheita, t.nome as nome_turma
  FROM plantio pl 
  inner join plantio_pessoa pp on pl.id = pp.idplantio
  inner join pessoa p on pp.idpessoa = p.id
  inner join cultura c on pl.idcultura = c.id
  inner join turma t on pl.idturma = t.id
  where pp.idpessoa = :id";
  $stmt = $bd->prepare( $sql );
  $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
  $result = $stmt->execute();
  return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


}

function plantioall_select_table(){
  //$this->id = $id;
  $bd = Crud_Plantio::conexao();
  $sql = "SELECT pl.id as idplantio, pl.data_inicio, pl.idcultura, pl.idturma, pl.numero_sensor, 
  pl.data_fim, pp.id as idplantiopessoa, pp.idpessoa, p.nome, c.nome as nome_cultura, t.nome as nome_turma
  FROM plantio pl 
  inner join plantio_pessoa pp on pl.id = pp.idplantio
  inner join pessoa p on pp.idpessoa = p.id
  inner join cultura c on pl.idcultura = c.id
  inner join turma t on pl.idturma = t.id";
  $stmt = $bd->prepare( $sql );
  //$stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
  $result = $stmt->execute();
  return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


}

function plantio_select($id){
    $this->id = $id;
    $bd = Crud_Plantio::conexao();
    $sql = "SELECT * FROM plantio where id = :id";
    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function meus_plantios_select_table($id){
    $this->id = $id;
    $bd = Crud_Plantio::conexao();
    $sql = "SELECT pl.id as idplantio, pl.data_inicio, pl.idcultura, pl.idturma, pl.numero_sensor, 
    pl.data_fim, pp.id as idplantiopessoa, pp.idpessoa, p.nome, c.nome as nome_cultura, t.nome as nome_turma
    FROM plantio pl 
    inner join plantio_pessoa pp on pl.id = pp.idplantio
    inner join pessoa p on pp.idpessoa = p.id
    inner join cultura c on pl.idcultura = c.id
    inner join turma t on pl.idturma = t.id
    where pp.idpessoa = :id";
    $stmt = $bd->prepare( $sql );
    $stmt->bindParam(':id',$this->id, PDO::PARAM_INT);
    $result = $stmt->execute();
    return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function plantio_update($id,$numero_sensor){
    try{
        $this->id = $id;
        $this->numero_sensor = $numero_sensor;
        $bd = Crud_Plantio::conexao();
        $sql = "UPDATE plantio set numero_sensor = :numero_sensor WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':numero_sensor',$this->numero_sensor,PDO::PARAM_STR);

        $result = $stmt->execute();
        if($result == TRUE)
        {
            $_SESSION['mensagem'] = "Planta digital criada com sucesso!";
            $url='../avatar.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }
        else{
            $_SESSION['mensagem'] = "Erro ao criar avatar!";
            $url='../tela_erro.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}

function plantio_update_finalizar($id,$data_fim){
    try{
        $this->id = $id;
        $this->data_fim = $data_fim;
        $bd = Crud_Plantio::conexao();
        $sql = "UPDATE plantio set data_fim = :data_fim WHERE id = :id";
        $stmt = $bd->prepare( $sql );
        $stmt->bindParam(':id',$this->id,PDO::PARAM_INT);
        $stmt->bindParam(':data_fim',$this->data_fim,PDO::PARAM_STR);

        $result = $stmt->execute();
        if($result == TRUE)
        {
            $_SESSION['mensagem'] = "Plantio encerrado com sucesso!";
            $url='../default.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        }
        else{
            $_SESSION['mensagem'] = "Erro ao encerrar plantio!";
            $url='../tela_erro.php';
            echo("<META HTTP-EQUIV=REFRESH CONTENT = '0;URL=$url'>");
            exit();
        } 
    }          

    catch ( PDOException $e ){
        echo 'Erro ao alterar: ' . $e->getMessage();
    }   
}




}




///fim cultura

?>