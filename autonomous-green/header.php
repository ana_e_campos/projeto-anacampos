<!DOCTYPE html>
<html>
<head>
  <?php 
  if (!isset($_SESSION)){ 
    session_start();
  }

    // Verifica se não há a variável da sessão que identifica o usuário
  if (!isset($_SESSION['id'])) {
        // Destrói a sessão por segurança
    session_destroy();
        // Redireciona o visitante de volta pro login
    header("Location: login.php"); exit;
  }

  if($_SESSION['usuarionivel'] == 1){

    $visibilidade1 = "";
    $visibilidade2 = "display: none;";

  }

  if($_SESSION['usuarionivel'] == 2){

    $visibilidade1 = "display: none;";
    $visibilidade2 = "";

  }



  ?>
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <link rel="shortcut icon" href="pwa/assets/ico/favicon.png" />
  <link rel="manifest" href="pwa/assets/manifest.json">


  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style>
    .material-tooltip .backdrop{
      background-color: #DB0000;
    }

    #lamp{

      color: inherit; 
      filter: blur(0px);

    }
  </style>
</head>

<?php
$url = $_SERVER['REQUEST_URI'];
if($url=='/autonomous-green/avatar.php')
{
  $cor_body = "background-color:#c7e48a;";
}
else{
  $cor_body = "background-color:#fff;";
}
?>

<body style="<?php echo $cor_body;?>"> 
  <!-- style="background-color:#c7e48a;" header-->

  <!-- MENU MOBILE-->
  <ul class="sidenav" id="mobile-demo">
    <!--administrador-->
    <li style="<?php echo $visibilidade1 ?>"><a class="dropdown-trigger" href="#!" data-target="dropdownprofessor2">Professor<i class="material-icons right">arrow_drop_down</i></a></li>
    <li><a class="dropdown-trigger" href="#!" data-target="dropdowncultura2">Cultura<i class="material-icons right">arrow_drop_down</i></a></li>
    <li style="<?php echo $visibilidade1 ?>"><a class="dropdown-trigger" href="#!" data-target="dropdownturma2">Turma<i class="material-icons right">arrow_drop_down</i></a></li>


    <!--turma-->


    <li style="<?php echo $visibilidade2 ?>"><a class="dropdown-trigger" href="#!" data-target="dropdownmeusplantios2">Meus Plantios<i class="material-icons right">arrow_drop_down</i></a></li>

    <li style="<?php echo $visibilidade1 ?>"><a class="dropdown-trigger" href="#!" data-target="dropdownplantios2">Plantios<i class="material-icons right">arrow_drop_down</i></a></li>

    <li style="<?php echo $visibilidade2 ?>"><a href="avatar.php">Ver plantinha<i class="material-icons right">filter_vintage</i></a></li>


    <li><a class="dropdown-trigger" href="#!" data-target="dropdownconfig2">
     <?php
     session_start();
     echo $_SESSION['usuarioNome']; 
     ?>  <i class="material-icons right">account_box</i></a></li>
   </ul>

   <ul id="dropdownprofessor2" class="dropdown-content">
    <li><a href="cadastrar_professor.php">Cadastrar</a></li>
    <li><a href="listar_professores.php">Pesquisar</a></li>
  </ul>

  <ul id="dropdowncultura2" class="dropdown-content">
    <li><a href="cadastrar_cultura.php">Cadastrar</a></li>
    <li><a href="listar_culturas.php">Pesquisar</a></li>
  </ul>

  <ul id="dropdownturma2" class="dropdown-content">
    <li><a href="cadastrar_turma.php">Cadastrar</a></li>
    <li><a href="listar_turmas.php">Pesquisar</a></li>
  </ul>
  <!--turma-->
  <ul id="dropdownplantios2" class="dropdown-content">
    <li><a href="listar_plantios.php">Pesquisar</a></li>
  </ul>


  <ul id="dropdownmeusplantios2" class="dropdown-content">
    <li><a href="meus_plantios.php">Ver</a></li>
  </ul>


  <ul id="dropdownconfig2" class="dropdown-content">
    <li><a href="sair.php">Logout</a></li>
  </ul>

  <!-- FIM MENU MOBILE-->





  <ul id="dropdownprofessor1" class="dropdown-content">
    <li><a href="cadastrar_professor.php">Cadastrar</a></li>
    <li><a href="listar_professores.php">Pesquisar</a></li>
  </ul>

  <ul id="dropdowncultura1" class="dropdown-content">
    <li><a href="cadastrar_cultura.php">Cadastrar</a></li>
    <li><a href="listar_culturas.php">Pesquisar</a></li>
  </ul>

  <ul id="dropdownturma1" class="dropdown-content">
    <li><a href="cadastrar_turma.php">Cadastrar</a></li>
    <li><a href="listar_turmas.php">Pesquisar</a></li>
  </ul>

  <ul id="dropdownplantios1" class="dropdown-content">
    <li><a href="listar_plantios.php">Pesquisar</a></li>
  </ul> 

  <ul id="dropdownmeusplantios1" class="dropdown-content">
    <li><a href="meus_plantios.php">Ver</a></li>
  </ul>

  <ul id="dropdownconfig1" class="dropdown-content">
    <li><a href="sair.php">Logout</a></li>
  </ul>

  <nav class="nav-extended  light-green lighten-1">
    <div class="nav-wrapper">
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>

      <a href="#!" class="brand-logo" style="padding: 17px;"><img class="responsive-img" style="max-width:200px;" src="images/logo-branco.png"></a>

      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li style="<?php echo $visibilidade1; ?>">
          <a class="dropdown-trigger" href="#!" data-target="dropdownprofessor1">Professor<i class="material-icons right">arrow_drop_down</i></a>
        </li>

        <li>
          <a class="dropdown-trigger" href="#!" data-target="dropdowncultura1">Cultura<i class="material-icons right">arrow_drop_down</i></a>
        </li>

        <li style="<?php echo $visibilidade1; ?>">
          <a class="dropdown-trigger" href="#!" data-target="dropdownturma1">Turma<i class="material-icons right">arrow_drop_down</i></a>
        </li>

        <li style="<?php echo $visibilidade1; ?>">
          <a class="dropdown-trigger" href="#!" data-target="dropdownplantios1">Plantios<i class="material-icons right">arrow_drop_down</i></a>
        </li>


        <li style="<?php echo $visibilidade2 ?>">
          <a class="dropdown-trigger" href="#!" data-target="dropdownmeusplantios1">Meus Plantios<i class="material-icons right">arrow_drop_down</i></a>
        </li>

        <li style="<?php echo $visibilidade2 ?>">
          <a href="avatar.php">Ver plantinha<i class="material-icons right">filter_vintage</i></a>
        </li>

        <li>
          <a class="dropdown-trigger" href="#!" data-target="dropdownconfig1"> 

            <?php
            session_start();
            echo $_SESSION['usuarioNome']; 
            ?>  <i class="material-icons right">account_box</i></a>
          </li>
        </ul>

      </div>




      <!--fim header-->


