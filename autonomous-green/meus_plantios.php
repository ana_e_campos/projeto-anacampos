<?php

session_start();
if(isset($_SESSION['mensagem'])){?>


  <script>
    window.onload = function(){
      M.toast({html: ' <?php echo $_SESSION['mensagem'];?>'});
    }
  </script>

  <?php
}
unset($_SESSION['mensagem']);

include ("header.php");
include ("classes/meus_plantios_select.php");

?>

<div class="nav-content">
  <span class="nav-title">  </span>


</div>
</nav>

<div class="row">
  <div class="col s12 m12">
    <div class="card blue-grey darken-1">
      <div class="card-action light-green lighten-1">
        <a href="#" style="color:#fff">Meus Plantios</a>
      </div>
      <div class="card-content white-text">
        <?php 
        if (isset($_GET["excluir"])){
          echo $mensagem_sucesso_excluir;
        }
        if (isset($_GET["erro_vinculo"])){
          echo $mensagem_erro_vinculo;
        }
        ?>
        <table>
          <thead>
            <tr>
              <th>Início</th>
              <th>Cultura</th>
              <th>Turma</th>
              <th>Nº Sensor</th>
              <th>Data Colheita</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>

          <tbody>

            <tr>
              <?php
              $i =0;
              while ($i < count($linha)) {
                ?>

                <td>    
                 <?php echo date('d/m/Y', strtotime($linha[$i]['data_inicio'])) ?>
               </td>
               <td>
                <?php echo $linha[$i]['nome_cultura'] ?>
              </td>
              <td>
                <?php echo $linha[$i]['nome_turma'] ?>
              </td>
              <td>
                <?php echo $linha[$i]['numero_sensor'] ?>
              </td>
              <td>
               <?php if($linha[$i]['data_fim'] != null){
                echo date('d/m/Y', strtotime($linha[$i]['data_fim']));
              }else{echo "-";} ?>
            </td>
            <td>
             <?php if($linha[$i]['data_fim'] != null){
              echo "Inativo";
            }else{echo "Ativo";} ?>
          </td>
        </tr>
        <?php
        $i++;
      }
      ?>


    </tbody>
  </table>
</div>

</div>
</div>
</div>


<?php
include ("footer.php");

?>