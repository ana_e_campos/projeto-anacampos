<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//session_start();

/*-----------------------------*/

require 'clima/src/Climatempo.php';
require 'clima/src/Wrapper.php';
require 'clima/src/Forecast.php';
require 'clima/src/FifteenDays.php';
require 'clima/src/Search.php';
require 'clima/src/City.php';

use AdinanCenci\Climatempo\Search;

/*-----------------------------*/

$token  = 'd3e8a374dbff8fe8debee15d8cef93b2';
$search = new Search('bagé');
$bh     = $search->find()[0];

/*-----------------------------*/

require 'resources/header.html';


try {
    $forecast = $bh->fifteenDays($token);
} catch (Exception $e) {
    echo '<b>Error: </b>'.$e->getMessage();
    die();
}

echo 
'<div style="display:none"><table>
    <tr>
        <th>Date</th>
        <th title="Lower temperature">Low</th>
        <th title="Higher temperature">Hight</th>
        <th title="Probability of precipitation">Pop</th>
        <th title="Precipitation">MM</th>
        <th>Icon</th>
        <th>Phrase</th>
    </tr>';
    
        $i = 0;
        $j = 0;
        $l = 0;
        $m = 0;
        $n = 0;

    foreach ($forecast->days as $day) {
        
//if(strtotime($day) == strtotime($date)){
        echo "
        <tr>
            <td> $day->dateBr </td>                
            <td> $day->minTemp °C </td>
            <td> $day->maxTemp °C </td>
            <td> $day->pop % </td>
            <td> $day->mm mm </td>
            <td> 
                <img width=\"50\" src=\"resources/images/$day->dawnIcon.png\" />
                <img width=\"50\" src=\"resources/images/$day->morningIcon.png\" />
                <img width=\"50\" src=\"resources/images/$day->dayIcon.png\" />
                <img width=\"50\" src=\"resources/images/$day->afternoonIcon.png\" />
                <img width=\"50\" src=\"resources/images/$day->nightIcon.png\" /> 
            </td>
            <td> $day->textPt </td>
        </tr>"; 
        //////aqui data amanhã
        $frase = $day->textPt;
        $termo = 'chuva';
        $pos = strpos($frase, $termo);


        $date = new DateTime('+1 day');
        $dataamanha = $date->format('d/m/Y');
        
        if ($pos === false){
            echo '<td> Não encontrado  </td>';
        }
        
        if($pos !== false and (($day->dateBr) === $dataamanha)){
            echo '<td> Encontrado222 </td>';
            
            $i++;
            echo $date->format('d/m/Y');
        }

        //////aqui data de hoje
        $frasehoje = $day->textPt;
        $termosol = 'Sol';
        $termochuva = 'chuva';
        $termonublado = 'nublado';
        $termogeada = 'Geada';
        $possol = strpos($frasehoje, $termosol);
        $poschuva = strpos($frasehoje, $termochuva); 
        $posnublado = strpos($frasehoje, $termonublado); 
        $posgeada = strpos($frasehoje, $termogeada); 

        $datahoje = new DateTime();
        $datahojehoje = $datahoje->format('d/m/Y');

         if ($possol === false){
           echo '<td> Não encontrado  </td>';
      }
        
        if($possol !== false and (($day->dateBr) === $datahojehoje)){
            echo '<td> Encontrado sol </td>';
            
            $j++;
            echo $datahoje->format('d/m/Y');
        }
        if($poschuva !== false and (($day->dateBr) === $datahojehoje)){
            echo '<td> Encontrado chuva </td>';
            
            $l++;
            echo $datahoje->format('d/m/Y');
        }
        if($posnublado !== false and (($day->dateBr) === $datahojehoje)){
            echo '<td> Encontrado nublado </td>';
            
            $m++;
            echo $datahoje->format('d/m/Y');
        }
         if($posgeada !== false and (($day->dateBr) === $datahojehoje)){
            echo '<td> Encontrado geada </td>';
            
            $n++;
            echo $datahoje->format('d/m/Y');
        }


    }

echo 
'</table></div>';
$_SESSION['clima'] = $i;
$_SESSION['climahojesol'] = $j;
$_SESSION['climahojechuva'] = $l;
$_SESSION['climahojenublado'] = $m;
$_SESSION['climahojegeada'] = $n;
