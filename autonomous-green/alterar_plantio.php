

<?php
session_start();

if($_SESSION['usuarionivel'] == 2){
      //session_destroy();
	header("Location: comecar_plantio.php"); exit;
}


include ("header.php");
include ("classes/cultura_select.php");

?>

<div class="nav-content">
	<span class="nav-title">  </span>
	<a class="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="listar_plantios.php">
		<i class="material-icons">list</i>
	</a>

</div>
</nav>

<div class="row">
	<div class="col s12 m12">
		<div class="card white">
			<div class="card-action light-green lighten-1">
				<a href="#" style="color:#fff;">Alterar Plantio</a>
			</div>

			<div class="card-content white-text">
				<div class="row">

					<form class="col s12" method="POST" novalidate action="classes/plantio_update.php">
						<input type="hidden" class="form-control" name="id" id="id" placeholder="" value="<?php echo $campoalterar[0]['id'] ?>" required>
						<div class="row">
							<div class="input-field col s12">
								<input id="data_inicio" name="data_inicio" type="text" class="datepicker">
								<label for="data_inicio">Início plantio</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input id="temperatura" name="temperatura" type="text" class="validate" value="<?php echo $campoalterar[0]['temperatura'] ?>">
								<label for="temperatura">Temperatura</label>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="umidade" name="umidade" type="text" class="validate" value="<?php echo $campoalterar[0]['umidade'] ?>">
								<label for="umidade">Umidade</label>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="luminosidade" name="luminosidade" type="text" class="validate" value="<?php echo $campoalterar[0]['luminosidade'] ?>">
								<label for="luminosidade">Luminosidade</label>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="tempo_colheita" name="tempo_colheita" type="text" class="validate" value="<?php echo $campoalterar[0]['tempo_colheita'] ?>">
								<label for="tempo_colheita">Tempo aproximado para início da colheita (em dias)</label>
							</div>
						</div>

						<button class="btn waves-effect waves-light" type="submit" name="botaoAlterar">Salvar
							<i class="material-icons right">send</i>
						</button>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include ("footer.php");

?>