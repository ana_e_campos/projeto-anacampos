
<?php
include ("header.php");
?>

<div class="nav-content">
  <span class="nav-title">  </span>
  <a class="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="listar_plantios.php">
    <i class="material-icons">list</i>
  </a>

</div>
</nav>

<div class="row">
  <div class="col s12 m12">
    <div class="card white">
      <div class="card-action light-green lighten-1">
        <a href="#" style="color:#fff;">Cadastrar Plantio</a>
      </div>

      <div class="card-content white-text">
        <div class="row">

          <form class="col s12" method="POST" novalidate action="classes/plantio_insert.php">

           <div class="row">
            <div class="input-field col s12">
              <input id="data_inicio" name="data_inicio" type="text" class="datepicker" required>
              <label for="data_inicio">Início plantio</label>

            </div>
          </div>

  
          <button class="btn waves-effect waves-light" type="submit" name="botaoSalvar">Salvar
            <i class="material-icons right">send</i>
          </button>

        </form>
      </div>
    </div>
  </div>
</div>
</div>


<?php
include ("footer.php");
?>
