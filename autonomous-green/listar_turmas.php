<?php

session_start();

if($_SESSION['usuarionivel'] == 2){
      //session_destroy();
  header("Location: comecar_plantio.php"); exit;
}

if(isset($_SESSION['mensagem'])){?>


  <script>
    window.onload = function(){
      M.toast({html: ' <?php echo $_SESSION['mensagem'];?>'});
    }
  </script>

  <?php
}
unset($_SESSION['mensagem']);

include ("header.php");
include ("classes/turma_select.php");

?>

<div class="nav-content">
  <span class="nav-title">  </span>
  <a class="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="cadastrar_turma.php">
    <i class="material-icons">add</i>
  </a>

</div>
</nav>

<div class="row">
  <div class="col s12 m12">
    <div class="card blue-grey darken-1">
      <div class="card-action light-green lighten-1">
        <a href="#" style="color:#fff">Turmas</a>
      </div>
      <div class="card-content white-text">
        <?php 
        if (isset($_GET["excluir"])){
          echo $mensagem_sucesso_excluir;
        }
        if (isset($_GET["erro_vinculo"])){
          echo $mensagem_erro_vinculo;
        }
        ?>
        <table>
          <thead>
            <tr>
              <th>Nome</th>
              <th></th>
            </tr>
          </thead>

          <tbody>

            <tr>
              <?php
              $i =0;
              while ($i < count($linha)) {
                ?>

                <td>    
                  <a class="waves-effect" style="text-decoration: underline;color: #fff;" title="Alterar" href="alterar_turma.php?id=<?php echo $linha[$i]["id"]?>"><?php echo  $linha[$i]['nome'] ?></a>
                </td>

                <td>

                  <a style="<?php echo $style ?>" data-toggle="tooltip" data-placement="top" onclick="return confirm('Tem certeza que deseja excluir?');" class="btn btn-danger btn-lg" title="Excluir" href="classes/turma_delete.php?id=<?php echo $linha[$i]['id']?>"> 
                    <i class="material-icons">delete</i>
                  </a>



                </td>
              </tr>
              <?php
              $i++;
            }
            ?>


          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>


<?php
include ("footer.php");

?>