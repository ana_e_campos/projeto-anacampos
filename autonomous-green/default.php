
<?php
include ("header.php");
?>

<div class="nav-content">
  <span class="nav-title">  </span>
  
</div>
</nav>


<div class="slider">
  <ul class="slides">
    <li>
      <img src="images/slide/tomate-feliz.png"> <!-- random image -->
      <div class="caption center-align">
        <h3>Bem vindo(a) ao Autonomous Green!</h3>
        <h5 class="light grey-text text-lighten-3">Alimentação saudável de maneira divertida.</h5>
      </div>
    </li>
    <li>
      <img src="images/slide/slide4.png"> <!-- random image -->
      <div class="caption left-align">
        <h3>A infância é o tempo de maior criatividade na vida de um ser humano.</h3>
        <h5 class="light grey-text text-lighten-3">(J. Piaget)</h5>
      </div>
    </li>
    <li>
      <img src="images/slide/slide1.png"> <!-- random image -->
      <div class="caption left-align">
        <h3>Que a alimentação saudável seja teu único remédio.</h3>
        <h5 class="light grey-text text-lighten-3">(Hipócrates)</h5>
      </div>
    </li>
    <li>
      <img src="images/slide/slide2.png"> <!-- random image -->
      <div class="caption left-align">
        <h3>Dia Mundial da Água</h3>
        <h5 class="light grey-text text-lighten-3">22 de Março</h5>
      </div>
    </li>
      <li>
      <img src="images/slide/slide3.png"> <!-- random image -->
      <div class="caption left-align">
        <h3>Dia da Saúde e Nutrição</h3>
        <h5 class="light grey-text text-lighten-3">31 de Março</h5>
      </div>
    </li>
  </ul>
</div>

<?php
include ("footer.php");
?>
