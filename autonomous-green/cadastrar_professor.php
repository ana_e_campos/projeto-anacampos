
<?php

session_start();
if($_SESSION['usuarionivel'] == 2){
      //session_destroy();
  header("Location: comecar_plantio.php"); exit;
}


include ("header.php");
?>

<div class="nav-content">
  <span class="nav-title">  </span>
  <a class="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="listar_professores.php">
    <i class="material-icons">list</i>
  </a>

</div>
</nav>

<div class="row">
  <div class="col s12 m12">
    <div class="card white">
      <div class="card-action light-green lighten-1">
        <a href="#" style="color:#fff;">Cadastrar Professor</a>
      </div>

      <div class="card-content white-text">
        <div class="row">

          <form class="col s12" method="POST" novalidate action="classes/pessoa_insert.php">

            <div class="row">
              <div class="input-field col s12">
                <input id="nome" name="nome" type="text" class="validate" required>
                <label for="first_name">Nome</label>
                <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
                <input id="login" name="login" type="text" class="validate" required>
                <label for="login">Login</label>
                <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
                <input id="senha" name="senha" type="password" class="validate" required>
                <label for="password">senha</label>
                <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
                <select name="nivel" required>
                 <option value="" disabled>selecione..</option>
                 <option value="1" disabled>Direção</option>
                 <option value="2" selected>Professor</option>
                 <option value="3" disabled>Aluno</option>
               </select>
               <label>Nível de acesso</label>
               <span class="helper-text" data-error="preencha o campo corretamente" data-success="preenchimento correto">Preenchimento obrigatório</span>
             </div> 
           </div>

           <button class="btn waves-effect waves-light" type="submit" name="botaoSalvar">Salvar
            <i class="material-icons right">send</i>
          </button>

        </form>
      </div>
    </div>
  </div>
</div>
</div>


<?php
include ("footer.php");
?>
