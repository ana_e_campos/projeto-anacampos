<?php

try {
	require 'config.php';

	if(!$conexao){
		echo "Não foi possivel conectar com Banco de Dados!";
	}

	$query = $conexao->prepare('SELECT * FROM `unidade` where ativo = 0 order by idunidade asc');

		$query->execute();

		$out = "[";

		while($result = $query->fetch()){
			if ($out != "[") {
				$out .= ",";
			}
			$out .= '{"codigo": "'.$result["idunidade"].'",';
			$out .= '"unidade": "'.$result["unidade"].'",';
      $out .= '"logradouro": "'.$result["logradouro"].'",';
      $out .= '"bairro": "'.$result["bairro"].'",';
      $out .= '"cidade": "'.$result["cidade"].'",';
			$out .= '"uf": "'.$result["uf"].'",';
			$out .= '"numero": "'.$result["numero"].'",';
			$out .= '"foto": "'.$result["foto"].'"}';
		}
		$out .= "]";
		echo utf8_encode($out);



} catch (Exception $e) {
	echo "Erro: ". $e->getMessage();
};
