<?php

try {
	require 'config.php';

	if(!$conexao){
		echo "Não foi possivel conectar com Banco de Dados!";
	}
	$query = $conexao->prepare('SELECT * FROM `treino` where ativo = 1 order by idtreino asc');

		$query->execute();

		$out = "[";

		while($result = $query->fetch()){
			if ($out != "[") {
				$out .= ",";
			}
			$out .= '{"idtreino": "'.$result["idtreino"].'",';
			$out .= '"titulo": "'.$result["titulo"].'",';
			$out .= '"idusuario": "'.$result["idusuario"].'",';
      $out .= '"observacoes": "'.$result["observacoes"].'"}';
		}
		$out .= "]";
		echo utf8_encode($out);



} catch (Exception $e) {
	echo "Erro: ". $e->getMessage();
};
