<?php

try {
	require 'config.php';

	if(!$conexao){
		echo "Não foi possivel conectar com Banco de Dados!";
	}

	$query = $conexao->prepare('SELECT * FROM `musculo` order by idmusculo asc');

		$query->execute();

		$out = "[";

		while($result = $query->fetch()){
			if ($out != "[") {
				$out .= ",";
			}
			$out .= '{"codigo": "'.$result["idmusculo"].'",';
			$out .= '"titulo": "'.$result["titulo"].'",';
			$out .= '"ativo": "'.$result["ativo"].'",';
			$out .= '"foto": "'.$result["foto"].'"}';
		}
		$out .= "]";
		echo utf8_encode($out);



} catch (Exception $e) {
	echo "Erro: ". $e->getMessage();
};
