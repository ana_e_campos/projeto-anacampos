import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'introducao',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./paginas/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./paginas/list/list.module').then(m => m.ListPageModule)
  },
  {
    path: 'listar-exercicios',
    loadChildren: './paginas/listar-exercicios/listar-exercicios.module#ListarExerciciosPageModule'
  },
  { path: 'login', loadChildren: './paginas/login/login.module#LoginPageModule' },
  {
    path: 'introducao',
    loadChildren: './paginas/introducao/introducao.module#IntroducaoPageModule'
  },
  {
    path: 'listar-unidades',
    loadChildren: './paginas/listar-unidades/listar-unidades.module#ListarUnidadesPageModule'
  },
  {
    path: 'cadastro-usuario',
    loadChildren: './paginas/cadastro-usuario/cadastro-usuario.module#CadastroUsuarioPageModule'
  },
  {
    path: 'cadastro-treino',
    loadChildren: './paginas/cadastro-treino/cadastro-treino.module#CadastroTreinoPageModule'
  },
  {
    path: 'listar-produtos',
    loadChildren: './paginas/listar-produtos/listar-produtos.module#ListarProdutosPageModule'
  },
  {
    path: 'listar-treinos',
    loadChildren: './paginas/listar-treinos/listar-treinos.module#ListarTreinosPageModule'
  },
  {
    path: 'adicionar-exercicio-treino',
    loadChildren:
      './paginas/adicionar-exercicio-treino/adicionar-exercicio-treino.module#AdicionarExercicioTreinoPageModule'
  },
  {
    path: 'visualizar-treino/:id',
    loadChildren: './paginas/visualizar-treino/visualizar-treino.module#VisualizarTreinoPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
