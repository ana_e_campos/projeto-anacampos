import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { UrlService } from '../../provider/url.service';
import { MenuService } from '../../provider/menu.service';
import { ServiceUserService } from '../../provider/service-user.service';

@Component({
  selector: 'app-listar-unidades',
  templateUrl: './listar-unidades.page.html',
  styleUrls: ['./listar-unidades.page.scss']
})
export class ListarUnidadesPage implements OnInit {
  unidades: any;

  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public menuService: MenuService,
    public serviceUserService: ServiceUserService
  ) {
    this.listaUnidades();
  }

  listaUnidades() {
    this.http
      .get(this.serviceUrl.getUrl() + 'listUnidades.php')
      .pipe(map(res => res.json()))
      .subscribe(unidade => {
        this.unidades = unidade;
      });

    console.log(this.unidades);
  }

  ngOnInit() {}
}
