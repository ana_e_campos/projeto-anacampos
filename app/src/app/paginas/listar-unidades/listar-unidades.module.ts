import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListarUnidadesPage } from './listar-unidades.page';

const routes: Routes = [
  {
    path: '',
    component: ListarUnidadesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListarUnidadesPage]
})
export class ListarUnidadesPageModule {}
