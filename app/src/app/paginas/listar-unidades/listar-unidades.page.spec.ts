import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarUnidadesPage } from './listar-unidades.page';

describe('ListarUnidadesPage', () => {
  let component: ListarUnidadesPage;
  let fixture: ComponentFixture<ListarUnidadesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListarUnidadesPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarUnidadesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
