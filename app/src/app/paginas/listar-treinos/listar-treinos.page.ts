import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Headers } from '@angular/http';
import { UrlService } from '../../provider/url.service';
import { MenuService } from '../../provider/menu.service';
import { ServiceUserService } from '../../provider/service-user.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-listar-treinos',
  templateUrl: './listar-treinos.page.html',
  styleUrls: ['./listar-treinos.page.scss']
})
export class ListarTreinosPage implements OnInit {
  treinos: any;
  titulo: any;
  loading: any;
  detalhe: NavigationExtras;
  idtreino: any;

  treinoItem: Array<{ idtreino: any; titulo: any; idusuario: any }>;
  treinoItemTodos: Array<{ idtreino: any; titulo: any; idusuario: any }>;

  constructor(
    // tslint:disable-next-line: deprecation
    public http: Http,
    public serviceUrl: UrlService,
    public menuService: MenuService,
    public serviceUserService: ServiceUserService,
    public navController: NavController,
    public router: Router
  ) {
    this.listaTreinos();
  }

  listaTreinos() {
    this.treinoItem = [];
    this.serviceUrl.exibirLoading();
    this.http
      .get(this.serviceUrl.getUrl() + 'listTreinos.php')
      .pipe(map(res => res.json()))
      .subscribe(
        treino => {
          this.treinos = treino;
          for (let i = 0; i < treino.length; i++) {
            this.treinoItem.push({
              idtreino: treino[i].idtreino,
              titulo: treino[i].titulo,
              idusuario: treino[i].idusuario
            });
          }
          this.treinoItemTodos = this.treinoItem;
        },
        error => {
          this.serviceUrl.alertas(
            'Atenção',
            'não possível carregar os produtos, verifique sua conexão!'
          );
          this.serviceUrl.fecharLoading();
        }
      );
    this.serviceUrl.fecharLoading();
    console.log(this.treinos);
  }
  toProdutos() {
    this.navController.navigateForward('/cadastro-treino');
  }

  go(id) {
    console.log(id);
    this.router.navigateByUrl('/listar-treinos/id');
  }

  getItems(ev: any) {
    // Reset items back to all of the items

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
      this.treinoItem = this.treinoItemTodos.filter(treino => {
        return treino.titulo.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.treinoItem = this.treinoItemTodos;
    }
    console.log(this.treinoItem);
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.listaTreinos();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }

  delete(idtreino) {
    // tslint:disable-next-line: deprecation
    const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http
      .post(this.serviceUrl.url + 'deleteTreino.php', idtreino, {
        headers,
        method: 'POST'
      })
      .pipe(map(res => res));
  }

  deleteTreino(treino) {
    this.delete(treino.idtreino).subscribe(
      data => {
        console.log('Treino deletado com sucesso!');
        this.listaTreinos();
      },
      error => {
        console.log('erro ao tentar excluir' + error);
      }
    );
  }

  editTreino(treino) {
    this.detalhe = {
      queryParams: {
        idtreino: treino.idtreino,
        titulo: treino.titulo
      }
    };
    this.router.navigate(['cadastro-treino'], this.detalhe);
  }

  ngOnInit() {}
}
