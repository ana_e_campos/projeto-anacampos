import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarTreinosPage } from './listar-treinos.page';

describe('ListarTreinosPage', () => {
  let component: ListarTreinosPage;
  let fixture: ComponentFixture<ListarTreinosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarTreinosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarTreinosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
