import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { UrlService } from 'src/app/provider/url.service';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { ServiceUserService } from '../../provider/service-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  email: string;
  senha: string;
  constructor(
    public alertController: AlertController,
    public urlService: UrlService,
    public http: Http,
    public nav: NavController,
    public loading: LoadingController,
    public serviceUserService: ServiceUserService
  ) {
    if (localStorage.getItem('deslogado') === 'sim') {
      localStorage.setItem('deslogado', 'não');
      location.reload();
    }
    if (localStorage.getItem('userLogado') != null) {
      this.autenticar();
      console.log('autenticação');
      this.nav.navigateForward('home');
    }
  }

  ngOnInit() {}

  async logar() {
    if (this.email === undefined || this.senha === undefined) {
      this.urlService.alertas('Atenção', 'Preencha todos os campos');
    } else {
      this.urlService.exibirLoading();
      this.http
        .get(this.urlService.getUrl() + 'login.php?email=' + this.email + '&senha=' + this.senha)
        .pipe(map(res => res.json()))
        .subscribe(
          data => {
            if (data.msg.logado === 'sim') {
              if (data.dados.ativo === '1') {
                this.serviceUserService.setUserId(data.dados.idusuario);
                this.serviceUserService.setUserNome(data.dados.nome);
                this.serviceUserService.setUserPerfil(data.dados.perfil);
                this.serviceUserService.setUserFoto(data.dados.foto);

                localStorage.setItem('idUser', data.dados.idusuario);
                localStorage.setItem('nomeUser', data.dados.nome);
                localStorage.setItem('perfilUser', data.dados.perfil);
                localStorage.setItem('fotoUser', data.dados.foto);

                localStorage.setItem('userLogado', data);
                this.urlService.fecharLoading();
                this.nav.navigateBack('home');
              } else {
                this.urlService.fecharLoading();
                this.urlService.alertas('Atenção', 'Usuário bloqueado!');
              }
            } else {
              this.urlService.fecharLoading();
              this.urlService.alertas('Atenção', 'Usuário ou senha incorreto');
            }
          },
          error => {
            this.urlService.fecharLoading();
            this.urlService.alertas('Algo deu errado :(', 'verifique sua conexão com a internet');
          }
        );
    }
  }
  autenticar() {
    this.serviceUserService.setUserId(localStorage.getItem('idUser'));
    this.serviceUserService.setUserNome(localStorage.getItem('nomeUser'));
    this.serviceUserService.setUserPerfil(localStorage.getItem('perfilUser'));
    this.serviceUserService.setUserFoto(localStorage.getItem('fotoUser'));
  }
}
