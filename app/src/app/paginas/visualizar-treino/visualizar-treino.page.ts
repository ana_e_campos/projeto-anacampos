import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { UrlService } from '../../provider/url.service';
import { MenuService } from '../../provider/menu.service';
import { ServiceUserService } from '../../provider/service-user.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-visualizar-treino',
  templateUrl: './visualizar-treino.page.html',
  styleUrls: ['./visualizar-treino.page.scss']
})
export class VisualizarTreinoPage implements OnInit {
  exercicios: any;
  titulo: any;
  loading: any;
  detalhe: NavigationExtras;
  detalhes: any;
  id: any;

  dados: Array<{ idtreino: any; titulo: any }>;

  exercicioItem: Array<{
    idexercicio: any;
    titulo: any;
  }>;
  exercicioItemTodos: Array<{
    idexercicio: any;
    titulo: any;
  }>;

  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public menuService: MenuService,
    public serviceUserService: ServiceUserService,
    private route: Router,
    private ativeRouter: ActivatedRoute
  ) {
    this.listaExercicios();
    this.ativeRouter.params.subscribe(paramsId => {
      this.id = paramsId.id;
    });

    this.listDetalhes();
    this.dados = [];
  }

  listDetalhes() {
    this.http
      .get(this.serviceUrl.getUrl() + 'detalheTreino.php?idtreino=' + this.id)
      .pipe(map(res => res.json()))
      .subscribe(data => {
        this.detalhes = data;

        for (let i = 0; i < data.length; i++) {
          this.dados.push({
            idtreino: data[i]['idtreino'],
            titulo: data[i]['titulo']
          });
        }
        console.log(this.dados);
        console.log(this.dados[0].titulo);
      });
  }

  listaExercicios() {
    this.exercicioItem = [];
    this.serviceUrl.exibirLoading();
    this.http
      .get(this.serviceUrl.getUrl() + 'listExercicios.php')
      .pipe(map(res => res.json()))
      .subscribe(
        execicio => {
          this.exercicios = execicio;
          // tslint:disable-next-line: prefer-for-of
          for (let i = 0; i < execicio.length; i++) {
            this.exercicioItem.push({
              idexercicio: execicio[i]['idexercicio'],
              titulo: execicio[i]['titulo']
            });
          }

          this.exercicioItemTodos = this.exercicioItem;
        },
        error => {
          this.serviceUrl.alertas(
            'Atenção',
            'não possível carregar os produtos, verifique sua conexão!'
          );
          this.serviceUrl.fecharLoading();
        }
      );
    this.serviceUrl.fecharLoading();
  }
  getItems(ev: any) {
    // Reset items back to all of the items

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
      this.exercicioItem = this.exercicioItemTodos.filter(exercicio => {
        return exercicio.titulo.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.exercicioItem = this.exercicioItemTodos;
    }
    console.log(this.exercicioItem);
  }

  //toProdutos() {
  // this.nav.navigateForward('/cadastro_produtos');
  // }

  go(id) {
    console.log(id);
    this.route.navigateByUrl('/visualizar-treino/id');
  }

  ngOnInit() {}
}
