import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ServiceUserService } from '../../provider/service-user.service';
import { MenuService } from '../../provider/menu.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(
    public serviceUserService: ServiceUserService,
    public menuService: MenuService,
    public nav: NavController
  ) {
    this.validarMenu();
    console.log(this.serviceUserService.userNome);
    console.log(this.serviceUserService.userId);
  }

  /*1 - adm | 4 - aluno |  3 - funcionário | 5 - gerente*/
  validarMenu() {
    if (localStorage.getItem('userLogado') != null) {
      console.log('Usuário logado');
      this.serviceUserService.setUserId(localStorage.getItem('idUser'));
      this.serviceUserService.setUserNome(localStorage.getItem('nomeUser'));
      this.serviceUserService.setUserPerfil(localStorage.getItem('perfilUser'));
      this.serviceUserService.setUserFoto(localStorage.getItem('fotoUser'));

      if (this.serviceUserService.userPerfil === '4') {
        this.menuService.perfilMenu.push(this.menuService.menu[0]);
        this.menuService.perfilMenu.push(this.menuService.menu[2]);
        this.menuService.perfilMenu.push(this.menuService.menu[3]);
        this.menuService.perfilMenu.push(this.menuService.menu[4]);
        this.menuService.perfilMenu.push(this.menuService.menu[5]);
      }
      if (this.serviceUserService.userPerfil === '1') {
        this.menuService.perfilMenu.push(this.menuService.menu[0]);
        this.menuService.perfilMenu.push(this.menuService.menu[1]);
        this.menuService.perfilMenu.push(this.menuService.menu[2]);
        this.menuService.perfilMenu.push(this.menuService.menu[3]);
        this.menuService.perfilMenu.push(this.menuService.menu[4]);
        this.menuService.perfilMenu.push(this.menuService.menu[5]);
      }
    } else {
      this.nav.navigateBack('login');
    }
  }
}
