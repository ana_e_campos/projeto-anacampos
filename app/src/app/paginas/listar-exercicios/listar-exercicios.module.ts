import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListarExerciciosPage } from './listar-exercicios.page';

const routes: Routes = [
  {
    path: '',
    component: ListarExerciciosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListarExerciciosPage]
})
export class ListarExerciciosPageModule {}
