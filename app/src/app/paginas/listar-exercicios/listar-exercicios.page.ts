import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { UrlService } from '../../provider/url.service';
import { MenuService } from '../../provider/menu.service';
import { ServiceUserService } from '../../provider/service-user.service';
@Component({
  selector: 'app-listar-exercicios',
  templateUrl: './listar-exercicios.page.html',
  styleUrls: ['./listar-exercicios.page.scss']
})
export class ListarExerciciosPage implements OnInit {
  exercicios: any;
  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public menuService: MenuService,
    public serviceUserService: ServiceUserService
  ) {
    this.listaExercicios();
  }

  listaExercicios() {
    this.http
      .get(this.serviceUrl.getUrl() + 'listDados.php')
      .pipe(map(res => res.json()))
      .subscribe(execicio => {
        this.exercicios = execicio;
      });

    console.log(this.exercicios);
  }


  ngOnInit() {}
}
