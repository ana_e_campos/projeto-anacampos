import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { UrlService } from '../../provider/url.service';
import { MenuService } from '../../provider/menu.service';
import { ServiceUserService } from '../../provider/service-user.service';

@Component({
  selector: 'app-adicionar-exercicio-treino',
  templateUrl: './adicionar-exercicio-treino.page.html',
  styleUrls: ['./adicionar-exercicio-treino.page.scss']
})
export class AdicionarExercicioTreinoPage implements OnInit {
  exercicios: any;
  constructor(
    // tslint:disable-next-line: deprecation
    public http: Http,
    public serviceUrl: UrlService,
    public menuService: MenuService,
    public serviceUserService: ServiceUserService
  ) {
    this.listaExercicios();
  }

  listaExercicios() {
    this.http
      .get(this.serviceUrl.getUrl() + 'listDados.php')
      .pipe(map(res => res.json()))
      .subscribe(execicio => {
        this.exercicios = execicio;
      });

    console.log(this.exercicios);
  }

  ngOnInit() {}
}
