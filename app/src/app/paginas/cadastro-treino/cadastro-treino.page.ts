import { Component, OnInit } from '@angular/core';
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from '@angular/forms';
import { Http, Headers, Response, ResponseOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { UrlService } from '../../provider/url.service';
import { NavController } from '@ionic/angular';
import { ServiceUserService } from '../../provider/service-user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cadastro-treino',
  templateUrl: './cadastro-treino.page.html',
  styleUrls: ['./cadastro-treino.page.scss']
})
export class CadastroTreinoPage implements OnInit {
  postagem: any;
  titulo: any;
  idusuario: any;
  idtreino: any;

  constructor(
    public navController: NavController,
    public formBuilder: FormBuilder,
    public http: Http,
    private activedRoute: ActivatedRoute,
    public urlService: UrlService,
    public serviceUserService: ServiceUserService
  ) {
    this.preencher();
    this.postagem = this.formBuilder.group({
      idusuario: [this.serviceUserService.getUserId(), Validators.required],
      titulo: ['', Validators.required],
      idtreino: [this.idtreino, Validators.required]
    });
  }

  preencher() {
    this.activedRoute.queryParams.subscribe(parametros => {
      this.idtreino = parametros.idtreino;
      this.titulo = parametros.titulo;
    });
  }
  postarTreino() {
    if (this.titulo === undefined) {
      this.urlService.alertas('Atenção', 'Preencha todos os campos');
    } else {
      this.postData(this.postagem.value).subscribe(
        data => {
          console.log('Inserido com sucesso');
        },
        err => {
          console.log('Erro ao tentar inserir');
        }
      );
    }
  }

  postData(valor) {
    if (this.idtreino != null) {
      console.log('atualizar');
      // tslint:disable-next-line: deprecation
      const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
      return this.http
        .post(this.urlService.url + 'editTreino.php', valor, {
          headers,
          method: 'POST'
        })
        .pipe(map(res => res.json()));
    } else {
      // tslint:disable-next-line: deprecation
      const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
      return this.http
        .post(this.urlService.url + 'insertTreino.php', valor, {
          headers,
          method: 'POST'
        })
        .pipe(map(res => res.json()));
    }
  }
  ngOnInit() {}
}
