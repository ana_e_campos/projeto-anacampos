import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceUserService {
  userNome = '';
  userId = '';
  userPerfil = '';
  userFoto = '';

  constructor() {}

  setUserNome(valor) {
    this.userNome = valor;
  }
  getUserNome() {
    return this.userNome;
  }

  setUserId(valor) {
    this.userId = valor;
  }
  getUserId() {
    return this.userId;
  }

  setUserPerfil(valor) {
    this.userPerfil = valor;
  }
  getUserPerfil() {
    return this.userPerfil;
  }

  setUserFoto(valor) {
    this.userFoto = valor;
  }
  getUserFoto() {
    return this.userFoto;
  }
}
