import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  public menu = [
    {
      label: 'Home',
      icone: 'home',
      acao: 'home',
      menu: '0',
      exibirMenu: 'true'
    },
    {
      label: 'Exercícios',
      icone: 'bicycle',
      acao: 'listar-exercicios',
      menu: '1',
      exibirMenu: 'true'
    },
    {
      label: 'Cadastrar usuário',
      icone: 'contact',
      acao: 'cadastro_usuario',
      menu: '2',
      exibirMenu: 'true'
    },
    {
      label: 'Nossas Unidades',
      icone: 'home',
      acao: 'listar-unidades',
      menu: '3',
      exibirMenu: 'true'
    },
    {
      label: 'Novo Treino',
      icone: 'contact',
      acao: 'cadastro-treino',
      menu: '4',
      exibirMenu: 'true'
    },
    {
      label: 'Meus Treinos',
      icone: 'contact',
      acao: 'listar-treinos',
      menu: '5',
      exibirMenu: 'true'
    }
  ];

  public perfilMenu = [];

  constructor() {}
}
