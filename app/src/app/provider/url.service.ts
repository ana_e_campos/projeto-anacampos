import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  //url = 'http://localhost:8888/PROJETOS/projeto-anacampos/app/php/';
  url = 'http://localhost:8888/PROJETOS/projeto-anacampos/app/php/';
  loading = false;
  constructor(
    public alertController: AlertController,
    public loadingController: LoadingController
  ) {}

  getUrl() {
    return this.url;
  }

  async alertas(titulo, msg) {
    const alert = await this.alertController.create({
      header: titulo,
      message: msg,
      buttons: ['OK']
    });
    await alert.present();
  }
  async exibirLoading() {
    this.loading = true;
    return await this.loadingController
      .create({
        message: 'Fazendo conexão'
      })
      .then(a => {
        a.present().then(() => {
          if (!this.loading) {
            a.dismiss().then(() => {});
          }
        });
      });
  }

  async fecharLoading() {
    this.loading = false;
    return this.loadingController.dismiss().then(() => {});
  }
}
