import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ServiceUserService } from '../app/provider/service-user.service';
import { UrlService } from '../app/provider/url.service';
import { MenuService } from '../app/provider/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public serviceUserService: ServiceUserService,
    public urlService: UrlService,
    public navController: NavController,
    public menuService: MenuService
  ) {
    this.serviceUserService.getUserNome();
    this.serviceUserService.getUserFoto();
    this.serviceUserService.getUserId();
    this.urlService.getUrl();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  acaoMenu(menu) {
    switch (menu) {
      case 'home':
        this.home();
        break;

      case 'listar-exercicios':
        this.listarExercicios();
        break;

      case 'cadastro-usuario':
        this.cadastrarUsuario();
        break;

      case 'listar-unidades':
        this.listarUnidades();
        break;

      case 'cadastro-treino':
        this.cadastrarTreino();
        break;

      case 'listar-treinos':
        this.listarTreinos();
        break;

      default:
        break;
    }
  }

  home() {
    this.navController.navigateForward('home');
  }
  listarExercicios() {
    this.navController.navigateForward('listar-exercicios');
  }
  cadastrarUsuario() {
    this.navController.navigateForward('cadastro-usuario');
  }
  cadastrarTreino() {
    this.navController.navigateForward('cadastro-treino');
  }
  listarUnidades() {
    this.navController.navigateForward('listar-unidades');
  }
  listarTreinos() {
    this.navController.navigateForward('listar-treinos');
  }
  sairSessao() {
    localStorage.clear();
    location.reload();
    this.navController.navigateRoot('login');
    localStorage.setItem('deslogado', 'sim');
  }
}
