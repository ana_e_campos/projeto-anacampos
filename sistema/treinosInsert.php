
		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=treinos">Treinos</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=treinosInsert">Cadastrar</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["treinos"]["cadastrar"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-calendar"></i>
							<span class="break"></span>
							Treinos
						</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="php/treinoInsert.php" name="cadtreino">
						  <fieldset>


									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label text-bold">DADOS GERAIS </label>
										</div>
									</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label">Título </label>
										</div>
										<div class="controls">
											<input class="input-large" id="titulo" type="text" name="titulo" maxlength="200" >
										</div>
									</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label">Observações </label>
										</div>
										<div class="controls">
											<textarea class="input-large" type="text" name="observacoes" ></textarea>
										</div>
									</div>

							<div class="form-actions">
							  <input type="submit" id='btcadtreino' class="btn btn-large btn-evolucao text-transform-none" value="Salvar">
								</form>
							  <a href="admin.php?action=treinos"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
