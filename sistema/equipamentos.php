
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="admin.php?action=equipamentos">Equipamentos</a></li>
			</ul>

			<?php include_once('include/status.php'); ?>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["equipamentos"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-tags"></i>
							<span class="break"></span>
							Equipamentos
							<?php
							//VERIFICA A PERMISSÃO
							if($_SESSION["equipamentos"]["cadastrar"] == 1){
							?>
								<a data-rel="tooltip" data-original-title="Novo cadastro" href="admin.php?action=equipamentosInsert">
									<i class="halflings-icon white plus"></i>
								</a>
							<?php
								} //FIM VERIFICA A PERMISSÃO
							?>
						</h2>
						<div class="box-icon">
							<!--
							<a href="relatorios.php?tipo=Equipamentos"
							data-rel="tooltip" data-original-title="Gerar PDF"><i class="halflings-icon white white download-alt"></i></a>
						  -->
						</div>

					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th style="width: 50px">Foto</th>
									<th>Descrição</th>
									<th>Lotação</th>
								  <th>Ações</th>
							  </tr>
						  </thead>
						  <tbody>

								<?php
									$sql_equipamentos = $db->query("SELECT p.*, e.unidade
																								FROM equipamento p
																								LEFT JOIN unidade e
																								ON p.idunidade = e.idunidade
																								WHERE p.ativo = 1
																								GROUP BY p.idequipamento
																								ORDER BY p.idequipamento");
									#print $sql_equipamentos;break;
									//echo nl2br($db->getDebug());
									while($linha_equipamentos = $db->fetchArray($sql_equipamentos)){
								?>

								<tr>
									<td style="vertical-align: middle;">
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_equipamentos["foto"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/mini_<?php print($linha_equipamentos["foto"]); ?>" class="foto-mini" data-original="media/<?php print($linha_equipamentos["foto"]); ?>" alt="" />
											</a>
										</span>
									</td>
									<td style="vertical-align: middle;">
										<i class="icon-tags"></i>
										<?php print_db($linha_equipamentos["etiqueta"]); ?> <br />
										<strong><?php print_db($linha_equipamentos["descricao"]); ?></strong>
									</td>
									<td style="vertical-align: middle;">
										<i class="icon-home"></i>
										<?php print_db($linha_equipamentos["unidade"]); ?>
									</td>
									<td style="vertical-align: middle;">
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["equipamentos"]["ver"] == 1){
										?>
										<a class="btn btn-warning" data-rel="tooltip" data-original-title="Ver" href="admin.php?action=equipamentosSelect&idequipamento=<?php print_db($linha_equipamentos["idequipamento"]); ?>">
											<i class="halflings-icon white zoom-in"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["equipamentos"]["editar"] == 1){
										?>
										<a class="btn btn-info" data-rel="tooltip" data-original-title="Editar" href="admin.php?action=equipamentosUpdate&idequipamento=<?php print_db($linha_equipamentos["idequipamento"]); ?>">
											<i class="halflings-icon white edit"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["equipamentos"]["excluir"] == 1){
										?>
										<a class="btn btn-danger" data-rel="tooltip" data-original-title="Excluir" onclick="return confirm('Confirma a exclusão?');" href="php/equipamentoDelete.php?idequipamento=<?php print_db($linha_equipamentos["idequipamento"]); ?>">
											<i class="halflings-icon white trash"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
									</td>
								</tr>

								<?php
									} // fecha consulta equipamentos
								?>

						  </tbody>
					  </table>
					</div>
				</div><!--/span-->

						<a href="admin.php?action=inicio" style="float:right;"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

</div><!--/fluid-row-->
