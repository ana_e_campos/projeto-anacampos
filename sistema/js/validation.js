
datepicker( '.datepicker', {
        firstDayOfWeek: 1,
        months: {
            short: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            long: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro']
        },
        weekdays: {
            short: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            long: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado']
        },
        outputFormat:'%d/%m/%Y',
        navigateYear: false
    } );

/*

$(document).ready(function() {
    $("input.hasDatepicker").live("click", function() {
        $(this)
            .removeClass("hasDatepicker")
            .datepicker({
							showOn:"focus",
							dateFormat: 'dd/mm/yy',
    					dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
    					dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
    					dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
    					monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    					monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
    					nextText: 'Próximo',
    					prevText: 'Anterior'
						})
            .focus();
    });
});
*/

$('.data').mask('00/00/0000');
$('.hora').mask('00:00');
//$('.cpf').mask('000.000.000-00');
$('.cnpj').mask('00.000.000/0000-00');
$('.telefone_fixo').mask('(00) 0000-0000');
$('.telefone_celular').mask('(00) 0 0000-0000');
$('.cep').mask('00000-000');
$('.dinheiro').mask("#.##0,00" , { reverse:true});
$('.numero').mask("#");

var options = {
    onKeyPress: function (cpf, ev, el, op) {
        var masks = ['000.000.000-000', '00.000.000/0000-00'];
        $('.cpf').mask((cpf.length > 14) ? masks[1] : masks[0], op);
    }
}
$('.cpf').length > 11 ? $('.cpf').mask('00.000.000/0000-00', options) : $('.cpf').mask('000.000.000-00#', options);


$( ".float" ).on('input', function() {
  var value=$(this).val().replace(/[^0-9.]*/g, '');
  value=value.replace(/\.{2,}/g, '.');
  value=value.replace(/\.,/g, ',');
  value=value.replace(/\.[0-9]+\./g, '.');
  $(this).val(value);
});

// valida e-mails
function validateEmail(email)
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

//validação cadastro de perfis

$(document).ready(function(){
    $('#btcadperfil').click(function(){
    d = document.cadperfil;

    if (d.perfil.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Perfil</span>.</span>','APUS Digital - Sistema web', function(){
        $("#perfil").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=perfil]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de músculos

$(document).ready(function(){
    $('#btcadmusculo').click(function(){
    d = document.cadmusculo;

    if (d.titulo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Músculo ou grupo muscular</span>.</span>','APUS Digital - Sistema web', function(){
        $("#titulo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=titulo]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de equipes

$(document).ready(function(){
    $('#btcadequipe').click(function(){
    d = document.cadequipe;

    if (d.foto.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Foto</span>.</span>','APUS Digital - Sistema web', function(){
        $("#foto").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=foto]").focus();
      });
      return false;
    }

    if (d.nome.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;"><span style="color: red;">Nome</span>.</span>','APUS Digital - Sistema web', function(){
        $("#nome").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nome]").focus();
      });
      return false;
    }

    if (d.celular.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Telefone celular</span>.</span>','APUS Digital - Sistema web', function(){
        $("#celular").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=celular]").focus();
      });
      return false;
    }

    if (d.email.value == "" || validateEmail(d.email.value) == false){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">E-mail</span> corretamente.</span>','APUS Digital - Sistema web', function(){
        $("#email").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=email]").focus();
      });
      return false;
    }

    if (d.idperfil.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Perfil de acesso</span>.</span>','APUS Digital - Sistema web', function(){
        // /$("#idperfil").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idperfil]").focus();
      });
      return false;
    }

    if (d.senha.value == ""){
      jAlert('<span style="margin-left:15px">Por favor, preencha o campo <span style="color: red;">Senha</span>.</span>','APUS Digital - Sistema web', function(){
        $("#senha").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=senha]").focus();
      });
      return false;
    }

    if (d.senha_conf.value == ""){
      jAlert('<span style="margin-left:15px">Por favor, preencha o campo <span style="color: red;">Confirmação de senha</span>.</span>','APUS Digital - Sistema web', function(){
        $("#senha_conf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=senha_conf]").focus();
      });
      return false;
    }

    if (d.senha.value != d.senha_conf.value){
      jAlert('<span style="margin-left:15px">Por favor, confirme corretamente as <span style="color: red;">senhas</span>.</span>','APUS Digital - Sistema web', function(){
        $("*[name=senha]").val('');
        $("*[name=senha_conf]").val('');
        $("#senha").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=senha]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação edição de equipes

$(document).ready(function(){
    $('#btediequipe').click(function(){
    d = document.ediequipe;

    if (d.nome.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;"><span style="color: red;">Nome</span>.</span>','APUS Digital - Sistema web', function(){
        $("#nome").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nome]").focus();
      });
      return false;
    }

    if (d.celular.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Telefone celular</span>.</span>','APUS Digital - Sistema web', function(){
        $("#celular").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=celular]").focus();
      });
      return false;
    }

    if (d.email.value == "" || validateEmail(d.email.value) == false){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">E-mail</span> corretamente.</span>','APUS Digital - Sistema web', function(){
        $("#email").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=email]").focus();
      });
      return false;
    }

    if (d.idperfil.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Perfil de acesso</span>.</span>','APUS Digital - Sistema web', function(){
        // /$("#idperfil").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idperfil]").focus();
      });
      return false;
    }

    if (d.nova_senha.value != d.nova_senha_conf.value){
      jAlert('<span style="margin-left:15px">Por favor, confirme corretamente as <span style="color: red;">senhas</span>.</span>','APUS Digital - Sistema web', function(){
        $("*[name=nova_senha]").val('');
        $("*[name=nova_senha_conf]").val('');
        $("#nova_senha").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nova_senha]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de assinantes

$(document).ready(function(){
    $('#btcadassinante').click(function(){
    d = document.cadassinante;

    if (d.idplano.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Plano</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idplano").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idplano]").focus();
      });
      return false;
    }

    if (d.nome.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;"><span style="color: red;">Nome</span>.</span>','APUS Digital - Sistema web', function(){
        $("#nome").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nome]").focus();
      });
      return false;
    }

    if (d.nascimento.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;"><span style="color: red;">Nascimento</span>.</span>','APUS Digital - Sistema web', function(){
        $("#nascimento").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nascimento]").focus();
      });
      return false;
    }

    if (d.cpf.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;"><span style="color: red;">CPF/CNPJ</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cpf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cpf]").focus();
      });
      return false;
    }

    if (d.celular.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Telefone celular</span>.</span>','APUS Digital - Sistema web', function(){
        $("#celular").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=celular]").focus();
      });
      return false;
    }

    if (d.cep.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">CEP</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cep").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cep]").focus();
      });
      return false;
    }

    if (d.logradouro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Logradouro</span>.</span>','APUS Digital - Sistema web', function(){
        $("#logradouro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=logradouro]").focus();
      });
      return false;
    }

    if (d.numero.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Número</span>.</span>','APUS Digital - Sistema web', function(){
        $("#numero").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=numero]").focus();
      });
      return false;
    }

    if (d.bairro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Bairro</strong></span>.</span>','APUS Digital - Sistema web', function(){
        $("#bairro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=bairro]").focus();
      });
      return false;
    }

    if (d.cidade.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Cidade</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cidade").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cidade]").focus();
      });
      return false;
    }

    if (d.uf.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">UF</span>.</span>','APUS Digital - Sistema web', function(){
        $("#uf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=uf]").focus();
      });
      return false;
    }

    if (d.pais.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">País</span>.</span>','APUS Digital - Sistema web', function(){
        $("#pais").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=pais]").focus();
      });
      return false;
    }

    if (d.email.value == "" || validateEmail(d.email.value) == false){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">E-mail</span> corretamente.</span>','APUS Digital - Sistema web', function(){
        $("#email").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=email]").focus();
      });
      return false;
    }

    if (d.senha.value == ""){
      jAlert('<span style="margin-left:15px">Por favor, preencha o campo <span style="color: red;">Senha</span>.</span>','APUS Digital - Sistema web', function(){
        $("#senha").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=senha]").focus();
      });
      return false;
    }

    if (d.senha_conf.value == ""){
      jAlert('<span style="margin-left:15px">Por favor, preencha o campo <span style="color: red;">Confirmação de senha</span>.</span>','APUS Digital - Sistema web', function(){
        $("#senha_conf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=senha_conf]").focus();
      });
      return false;
    }

    if (d.senha.value != d.senha_conf.value){
      jAlert('<span style="margin-left:15px">Por favor, confirme corretamente as <span style="color: red;">senhas</span>.</span>','APUS Digital - Sistema web', function(){
        $("*[name=senha]").val('');
        $("*[name=senha_conf]").val('');
        $("#senha").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=senha]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação edição de assinantes

$(document).ready(function(){
    $('#btediassinante').click(function(){
    d = document.ediassinante;

    if (d.nome.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;"><span style="color: red;">Nome</span>.</span>','APUS Digital - Sistema web', function(){
        $("#nome").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nome]").focus();
      });
      return false;
    }

    if (d.nascimento.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;"><span style="color: red;">Nascimento</span>.</span>','APUS Digital - Sistema web', function(){
        $("#nascimento").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nascimento]").focus();
      });
      return false;
    }

    if (d.cpf.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;"><span style="color: red;">CPF/CNPJ</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cpf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cpf]").focus();
      });
      return false;
    }

    if (d.celular.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Telefone celular</span>.</span>','APUS Digital - Sistema web', function(){
        $("#celular").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=celular]").focus();
      });
      return false;
    }

    if (d.cep.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">CEP</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cep").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cep]").focus();
      });
      return false;
    }

    if (d.logradouro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Logradouro</span>.</span>','APUS Digital - Sistema web', function(){
        $("#logradouro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=logradouro]").focus();
      });
      return false;
    }

    if (d.numero.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Número</span>.</span>','APUS Digital - Sistema web', function(){
        $("#numero").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=numero]").focus();
      });
      return false;
    }

    if (d.bairro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Bairro</strong></span>.</span>','APUS Digital - Sistema web', function(){
        $("#bairro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=bairro]").focus();
      });
      return false;
    }

    if (d.cidade.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Cidade</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cidade").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cidade]").focus();
      });
      return false;
    }

    if (d.uf.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">UF</span>.</span>','APUS Digital - Sistema web', function(){
        $("#uf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=uf]").focus();
      });
      return false;
    }

    if (d.pais.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">País</span>.</span>','APUS Digital - Sistema web', function(){
        $("#pais").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=pais]").focus();
      });
      return false;
    }

    if (d.email.value == "" || validateEmail(d.email.value) == false){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">E-mail</span> corretamente.</span>','APUS Digital - Sistema web', function(){
        $("#email").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=email]").focus();
      });
      return false;
    }

    if (d.nova_senha.value != d.nova_senha_conf.value){
      jAlert('<span style="margin-left:15px">Por favor, confirme corretamente as <span style="color: red;">senhas</span>.</span>','APUS Digital - Sistema web', function(){
        $("*[name=nova_senha]").val('');
        $("*[name=nova_senha_conf]").val('');
        $("#nova_senha").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nova_senha]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de enderecos

$(document).ready(function(){
    $('#btcadendereco').click(function(){
    d = document.cadendereco;

    if (d.cep.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">CEP</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cep").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cep]").focus();
      });
      return false;
    }

    if (d.logradouro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Logradouro</span>.</span>','APUS Digital - Sistema web', function(){
        $("#logradouro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=logradouro]").focus();
      });
      return false;
    }

    if (d.numero.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Número</span>.</span>','APUS Digital - Sistema web', function(){
        $("#numero").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=numero]").focus();
      });
      return false;
    }

    if (d.bairro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Bairro</strong></span>.</span>','APUS Digital - Sistema web', function(){
        $("#bairro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=bairro]").focus();
      });
      return false;
    }

    if (d.cidade.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Cidade</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cidade").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cidade]").focus();
      });
      return false;
    }

    if (d.uf.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">UF</span>.</span>','APUS Digital - Sistema web', function(){
        $("#uf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=uf]").focus();
      });
      return false;
    }

    if (d.pais.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">País</span>.</span>','APUS Digital - Sistema web', function(){
        $("#pais").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=pais]").focus();
      });
      return false;
    }

    });
  return true;
});


//validação cadastro de escritórios

$(document).ready(function(){
    $('#btcadunidade').click(function(){
    d = document.cadunidade;

    if (d.unidade.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Nome da unidade</span>.</span>','APUS Digital - Sistema web', function(){
        $("#unidade").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=unidade]").focus();
      });
      return false;
    }

    if (d.logradouro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Logradouro</span>.</span>','APUS Digital - Sistema web', function(){
        $("#logradouro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=logradouro]").focus();
      });
      return false;
    }

    if (d.numero.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Número</span>.</span>','APUS Digital - Sistema web', function(){
        $("#numero").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=numero]").focus();
      });
      return false;
    }

    if (d.bairro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Bairro</strong></span>.</span>','APUS Digital - Sistema web', function(){
        $("#bairro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=bairro]").focus();
      });
      return false;
    }

    if (d.cidade.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Cidade</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cidade").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cidade]").focus();
      });
      return false;
    }

    if (d.uf.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">UF</span>.</span>','APUS Digital - Sistema web', function(){
        $("#uf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=uf]").focus();
      });
      return false;
    }

    if (d.pais.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">País</span>.</span>','APUS Digital - Sistema web', function(){
        $("#pais").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=pais]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de congregações

$(document).ready(function(){
    $('#btcadcongregacao').click(function(){
    d = document.cadcongregacao;

    if (d.congregacao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Nome da congregação</span>.</span>','APUS Digital - Sistema web', function(){
        $("#congregacao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=congregacao]").focus();
      });
      return false;
    }

    if (d.responsavel.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Nome do responsável</span>.</span>','APUS Digital - Sistema web', function(){
        $("#responsavel").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=responsavel]").focus();
      });
      return false;
    }

    if (d.datafundacao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Data de fundação</span>.</span>','APUS Digital - Sistema web', function(){
        $("#datafundacao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=datafundacao]").focus();
      });
      return false;
    }

    if (d.cep.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">CEP</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cep").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cep]").focus();
      });
      return false;
    }

    if (d.logradouro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Logradouro</span>.</span>','APUS Digital - Sistema web', function(){
        $("#logradouro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=logradouro]").focus();
      });
      return false;
    }

    if (d.numero.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Número</span>.</span>','APUS Digital - Sistema web', function(){
        $("#numero").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=numero]").focus();
      });
      return false;
    }

    if (d.bairro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Bairro</strong></span>.</span>','APUS Digital - Sistema web', function(){
        $("#bairro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=bairro]").focus();
      });
      return false;
    }

    if (d.cidade.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Cidade</span>.</span>','APUS Digital - Sistema web', function(){
        $("#cidade").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=cidade]").focus();
      });
      return false;
    }

    if (d.uf.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">UF</span>.</span>','APUS Digital - Sistema web', function(){
        $("#uf").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=uf]").focus();
      });
      return false;
    }

    if (d.pais.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">País</span>.</span>','APUS Digital - Sistema web', function(){
        $("#pais").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=pais]").focus();
      });
      return false;
    }

    });
  return true;
});


//validação cadastro de documento
/*
$(document).ready(function(){
    $('#btcaddocumento').click(function(){
    d = document.caddocumento;

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }

    });
  return true;
});
*/

//validação email

$(document).ready(function(){
    $('#btcadlogin').click(function(){
    d = document.cadlogin;

    if (d.username.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">E-mail</span>.</span>','APUS Digital - Sistema web', function(){
        $("#username").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=username]").focus();
      });
      return false;
    }
    if (d.password.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Senha</span>.</span>','APUS Digital - Sistema web', function(){
        $("#password").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=password]").focus();
      });
      return false;
    }


    });
  return true;
});

//validação esqueci a senha

$(document).ready(function(){
    $('#btcadsenha').click(function(){
    d = document.cadsenha;

    if (d.email.value == "" || validateEmail(d.email.value) == false){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">E-mail</span> corretamente.</span>','APUS Digital - Sistema web', function(){
        $("#email").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=email]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação recuperação de senha

$(document).ready(function(){
    $('#btcadloginsenha').click(function(){
    d = document.cadloginsenha;

    if (d.newpassword.value == ""){
      jAlert('<span style="margin-left:15px">Por favor, preencha o campo <span style="color: red;">Nova senha</span>.</span>','APUS Digital - Sistema web', function(){
        $("#newpassword").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=newpassword]").focus();
      });
      return false;
    }

    if (d.newpasswordconfirmation.value == ""){
      jAlert('<span style="margin-left:15px">Por favor, preencha o campo <span style="color: red;">Confirmação de senha</span>.</span>','APUS Digital - Sistema web', function(){
        $("#newpasswordconfirmation").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=newpasswordconfirmation]").focus();
      });
      return false;
    }

    if (d.newpassword.value != d.newpasswordconfirmation.value){
      jAlert('<span style="margin-left:15px">Por favor, confirme corretamente as <span style="color: red;">senhas</span>.</span>','APUS Digital - Sistema web', function(){
        $("*[name=newpassword]").val('');
        $("*[name=newpasswordconfirmation]").val('');
        $("#newpasswordconfirmation").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=newpassword]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de funcoes

$(document).ready(function(){
    $('#btcadfuncao').click(function(){
    d = document.cadfuncao;

    if (d.funcao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Função</span>.</span>','APUS Digital - Sistema web', function(){
        $("#funcao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=funcao]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de planos

$(document).ready(function(){
    $('#btcadplano').click(function(){
    d = document.cadplano;

    if (d.plano.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Plano</span>.</span>','APUS Digital - Sistema web', function(){
        $("#plano").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=plano]").focus();
      });
      return false;
    }

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }

    if (d.credito.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Créditos</span>.</span>','APUS Digital - Sistema web', function(){
        $("#credito").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=credito]").focus();
      });
      return false;
    }

    if (d.endereco.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Endereços</span>.</span>','APUS Digital - Sistema web', function(){
        $("#endereco").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=endereco]").focus();
      });
      return false;
    }

    if (d.valor.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Valor</span>.</span>','APUS Digital - Sistema web', function(){
        $("#valor").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=valor]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de tipos de serviço

$(document).ready(function(){
    $('#btcadtipo').click(function(){
    d = document.cadtipo;

    if (d.tipo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Tipo de serviço</span>.</span>','APUS Digital - Sistema web', function(){
        $("#tipo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=tipo]").focus();
      });
      return false;
    }

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de serviços

$(document).ready(function(){
    $('#btcadservico').click(function(){
    d = document.cadservico;

    if (d.idtipo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Tipo de serviço</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idtipo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idtipo]").focus();
      });
      return false;
    }

    if (d.idservico.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Serviço</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idservico").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idservico]").focus();
      });
      return false;
    }

    /*

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }
    */

    });
  return true;
});

//validação cadastro de pacotes de serviços

$(document).ready(function(){
    $('#btcadpacote').click(function(){
    d = document.cadpacote;

    if (d.idtipo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Tipo de serviço</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idtipo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idtipo]").focus();
      });
      return false;
    }

    if (d.idservico.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Serviço</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idservico").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idservico]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de solicitações (assinante)

$(document).ready(function(){
    $('#btcadsolicitacao').click(function(){
    d = document.cadsolicitacao;

    if (d.idendereco.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Endereço</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idendereco").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idendereco]").focus();
      });
      return false;
    }

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de andamentos

$(document).ready(function(){
    $('#btcadandamento').click(function(){
    d = document.cadandamento;

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de faturas

$(document).ready(function(){
    $('#btcadfatura').click(function(){
    d = document.cadfatura;

    if (d.idassinante.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Assinante</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idassinante").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idassinante]").focus();
      });
      return false;
    }

    if (d.faturamento_mes.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Faturamento</span>.</span>','APUS Digital - Sistema web', function(){
        $("#faturamento_mes").attr('style', 'border: 1px solid #D43F3A !important; width: 80px');
        $("*[name=#faturamento_mes]").focus();
      });
      return false;
    }

    if (d.faturamento_ano.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Faturamento</span>.</span>','APUS Digital - Sistema web', function(){
        $("#faturamento_ano").attr('style', 'border: 1px solid #D43F3A !important; width: 80px');
        $("*[name=#faturamento_ano]").focus();
      });
      return false;
    }

    if (d.vencimento.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Vencimento</span>.</span>','APUS Digital - Sistema web', function(){
        $("#vencimento").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=vencimento]").focus();
      });
      return false;
    }
    });
  return true;
});

//validação cadastro de faturas

$(document).ready(function(){
    $('#btedifatura').click(function(){
    d = document.edifatura;

    if (d.pagamento.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Data dagamento</span>.</span>','APUS Digital - Sistema web', function(){
        $("#pagamento").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=pagamento]").focus();
      });
      return false;
    }

    if (d.pago.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Valor pago</span>.</span>','APUS Digital - Sistema web', function(){
        $("#pago").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=pago]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de álbuns de fotos

$(document).ready(function(){
    $('#btcadalbum').click(function(){
    d = document.cadalbum;

    if (d.album.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Título do álbum</span>.</span>','APUS Digital - Sistema web', function(){
        $("#album").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=album]").focus();
      });
      return false;
    }

    });
  return true;
});


//validação cadastro de blog

$(document).ready(function(){
    $('#btcadblog').click(function(){
    d = document.cadblog;

    if (d.titulo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Título</span>.</span>','APUS Digital - Sistema web', function(){
        $("#titulo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=titulo]").focus();
      });
      return false;
    }

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }

    if (d.foto.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Foto</span>.</span>','APUS Digital - Sistema web', function(){
        $("#foto").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=foto]").focus();
      });
      return false;
    }

    });
  return true;
});


//validação cadastro de tipos de finança

$(document).ready(function(){
    $('#btcadfinancatipo').click(function(){
    d = document.cadfinancatipo;

    if (d.financatipo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Tipo de finança</span>.</span>','APUS Digital - Sistema web', function(){
        $("#financatipo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=financatipo]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de finanças

$(document).ready(function(){
    $('#btcadfinanca').click(function(){
    d = document.cadfinanca;

    if (d.movimentacao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Movimentação</span>.</span>','APUS Digital - Sistema web', function(){
        $("#movimentacao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=movimentacao]").focus();
      });
      return false;
    }
    if (d.idunidade.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Escritório</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idunidade").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idunidade]").focus();
      });
      return false;
    }
    if (d.idfinancatipo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Tipo</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idfinancatipo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idfinancatipo]").focus();
      });
      return false;
    }
    if (d.idmembro.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Vínculo</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idmembro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idmembro]").focus();
      });
      return false;
    }
    if (d.forma.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Forma de pagamento/recebimento</span>.</span>','APUS Digital - Sistema web', function(){
        $("#forma").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=forma]").focus();
      });
      return false;
    }
    if (d.valor.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Valor</span>.</span>','APUS Digital - Sistema web', function(){
        $("#valor").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=valor]").focus();
      });
      return false;
    }
    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }
    if (d.data.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Data</span>.</span>','APUS Digital - Sistema web', function(){
        $("#data").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=data]").focus();
      });
      return false;
    }

    });
  return true;
});


//validação cadastro de departamentos

$(document).ready(function(){
    $('#btcaddepartamento').click(function(){
    d = document.caddepartamento;

    if (d.departamento.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Departamento</span>.</span>','APUS Digital - Sistema web', function(){
        $("#departamento").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=departamento]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de tarefas

$(document).ready(function(){
    $('#btcadtarefa').click(function(){
    d = document.cadtarefa;

    if (d.tarefa.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Tarefa</span>.</span>','APUS Digital - Sistema web', function(){
        $("#tarefa").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=tarefa]").focus();
      });
      return false;
    }

    if (d.idusuario.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Usuário</span>.</span>','APUS Digital - Sistema web', function(){
        //$("#idusuario").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idusuario]").focus();
      });
      return false;
    }

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }

    if (d.datainicio.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Data de início</span>.</span>','APUS Digital - Sistema web', function(){
        $("#datainicio").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=datainicio]").focus();
      });
      return false;
    }


    });
  return true;
});

//validação cadastro de membro

$(document).ready(function(){
    $('#btcadmembro').click(function(){
    d = document.cadmembro;

    if (d.idfuncao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Congregacional: funcao</span>.</span>','APUS Digital - Sistema web', function(){
        $("#idfuncao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idfuncao]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação cadastro de fornecedor

$(document).ready(function(){
    $('#btcadfornecedor').click(function(){
    d = document.cadfornecedor;

    if (d.nome.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Nome</span>.</span>','APUS Digital - Sistema web', function(){
        $("#nome").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=nome]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação equipamento

$(document).ready(function(){
    $('#btcadequipamento').click(function(){
    d = document.cadequipamento;


    if (d.idunidade.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Unidade</span>.</span>','APUS Digital - Sistema web', function(){
        //$("#idmembro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idunidade]").focus();
      });
      return false;
    }

    if (d.etiqueta.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Etiqueta</span>.</span>','APUS Digital - Sistema web', function(){
        $("#etiqueta").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=etiqueta]").focus();
      });
      return false;
    }

    if (d.descricao.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Descrição</span>.</span>','APUS Digital - Sistema web', function(){
        $("#descricao").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=descricao]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação exercício

$(document).ready(function(){
    $('#btcadexercicio').click(function(){
    d = document.cadexercicio;


    if (d.idmusculo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Músculo ou grupo muscular</span>.</span>','APUS Digital - Sistema web', function(){
        //$("#idmembro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idmusculo]").focus();
      });
      return false;
    }

    if (d.titulo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Título</span>.</span>','APUS Digital - Sistema web', function(){
        $("#titulo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=titulo]").focus();
      });
      return false;
    }

    });
  return true;
});

//validação treino execucao

$(document).ready(function(){
    $('#btcadexecucao').click(function(){
    d = document.cadexecucao;


    if (d.idexercicio.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Exercício</span>.</span>','APUS Digital - Sistema web', function(){
        //$("#idmembro").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=idexercicio]").focus();
      });
      return false;
    }

    });
  return true;
});


//validação treino

$(document).ready(function(){
    $('#btcadtreino').click(function(){
    d = document.cadtreino;

    if (d.titulo.value == ""){
      jAlert('<span>Por favor, preencha o campo <span style="color: red;">Título</span>.</span>','APUS Digital - Sistema web', function(){
        $("#titulo").attr('style', 'border: 1px solid #D43F3A !important;');
        $("*[name=titulo]").focus();
      });
      return false;
    }

    });
  return true;
});



//$(function(){
//       $(".documento").prop('required',true);
//});

/*
$(document).ready( function() {
  $(".exclusao").click( function() {
    jConfirm('Desejas realmente excluir?', 'APUS Digital - Sistema web', function (resp) {
      if (resp) {
        $(".exclusao").submit();
      }
      else{
        return false;
       }
    });
  });
});
*/
