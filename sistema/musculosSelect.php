<?php
	$idmusculo = isset($_GET["idmusculo"]) ? numero($_GET["idmusculo"]) : "";
	$bind = array($idmusculo);
	$sql_musculo = $db->query("SELECT *
														 FROM musculo
														 WHERE idmusculo = ? AND ativo = 1
														 LIMIT 1", $bind);
	//print $sql_musculo;break;
	$linha_musculo = $db->fetchArray($sql_musculo);
?>

		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=musculos">Músculos</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=musculosSelect&idmusculo=<?php print(numero($_GET['idmusculo'])); ?>">
						Ver
					</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["musculos"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-thumbs-up"></i>
							<span class="break"></span>
							Músculos
						</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="" name="cadmusculo">
						  <fieldset>


										<div class="control-group">
											<div class="control-label-bg">
												<label class="control-label text-bold">DADOS GERAIS </label>
											</div>
										</div>


										<div class="control-group">
											<div class="control-label-bg">
												<label class="control-label">Músculo ou grupo muscular </label>
											</div>
											<div class="controls">
												<input disabled class="input-xlarge" id="titulo" type="text" name="titulo" maxlength="200" value="<?php print_db($linha_musculo["titulo"]); ?>" >
											</div>
										</div>

								<div class="form-actions">
									</form>
									<a href="admin.php?action=musculos"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>
								</div>
								</fieldset>


						</div>
					</div><!--/span-->

				</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
