<?php
	$idexercicio = isset($_GET["idexercicio"]) ? numero($_GET["idexercicio"]) : "";
	$bind = array($idexercicio);
	$sql_exercicio = $db->query("SELECT *
														 FROM exercicio
														 WHERE idexercicio = ? AND ativo = 1
														 LIMIT 1", $bind);
	//print $sql_exercicio;break;
	$linha_exercicio = $db->fetchArray($sql_exercicio);
?>

		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=exercicios">Exercícios</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=exerciciosSelect&idexercicio=<?php print(numero($_GET['idexercicio'])); ?>">
						Ver
					</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["exercicios"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-resize-horizontal"></i>
							<span class="break"></span>
							Exercícios
						</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="" name="cadexercicio">
						  <fieldset>

								<ul class="nav nav-tabs">

									<li class="active">
										<a href="#geral" data-toggle="tab">Dados gerais</a>
									</li>
									<li>
										<a href="#masculino" data-toggle="tab">Fotos masculinas</a>
									</li>
									<li>
										<a href="#feminino" data-toggle="tab">Fotos femininas</a>
									</li>
								</ul>

							<div class="tab-content">

							 <div class="tab-pane active" id="geral">

										<div class="control-group">
											<div class="control-label-bg">
												<label class="control-label text-bold">DADOS GERAIS </label>
											</div>
										</div>

										<div class="control-group">
											<div class="control-label-bg">
												<label class="control-label" style="margin-left:0px;">Músculo ou grupo muscular </label>
											</div>
											<div class="controls">
												<select disabled data-placeholder="Músculos cadastradas no sistema" id="selectError" data-rel="chosen" name="idmusculo" style="width: 280px;">
													<optgroup label="">
														<option value=""></option>
														<?php
														$sql_musculo = $db->query("SELECT idmusculo, titulo FROM musculo WHERE ativo = 1 ORDER BY titulo");
														while($linha_musculo = $db->fetchArray($sql_musculo)){
														?>
														<option value="<?php print($linha_musculo["idmusculo"]); ?>" <?php if($linha_musculo["idmusculo"] == $linha_exercicio["idmusculo"]) print('selected'); ?>> <?php print_db($linha_musculo["titulo"]); ?> </option>
														<?php
													} // fim while musculo
														?>
													</optgroup>
												</select>
											</div>
										</div>

										<div class="control-group">
											<div class="control-label-bg">
												<label class="control-label">Título </label>
											</div>
											<div class="controls">
												<input disabled class="input-xlarge" id="titulo" type="text" name="titulo" maxlength="200" value="<?php print_db($linha_exercicio["titulo"]); ?>" >
											</div>
										</div>

									</div> <!-- tab-pane  -->

									<div class="tab-pane" id="masculino">

										<div class="control-group">
											<div class="control-label-bg">
												<label class="control-label text-bold">FOTOS MASCULINAS</label>
											</div>
										</div>

										<div class="control-group">
 		 								<div class="control-label-bg">
 		 									<label class="control-label text-bold">Movimento inicial </label>
 		 									<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
 		 								</div>
 		 							</div>

 									<div class="control-group">
 										<label class="control-label">
 											<span id="gallery" class="gallery">
 												<a href="media/<?php print($linha_exercicio["foto_m1"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
 													<img src="media/<?php print($linha_exercicio["foto_m1"]); ?>" class="foto-medium" data-original="media/<?php print($linha_exercicio["foto_m1"]); ?>" alt="" />
 												</a>
 											</span>
 										</label>
 									</div>

									<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label text-bold">Movimento final </label>
										<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label">
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_exercicio["foto_m2"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/<?php print($linha_exercicio["foto_m2"]); ?>" class="foto-medium" data-original="media/<?php print($linha_exercicio["foto_m2"]); ?>" alt="" />
											</a>
										</span>
									</label>
								</div>

									</div> <!-- tab-pane  -->

									<div class="tab-pane" id="feminino">

										<div class="control-group">
											<div class="control-label-bg">
												<label class="control-label text-bold">FOTOS FEMININAS</label>
											</div>
										</div>

										<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label text-bold">Movimento inicial </label>
											<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">
											<span id="gallery" class="gallery">
												<a href="media/<?php print($linha_exercicio["foto_f1"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
													<img src="media/<?php print($linha_exercicio["foto_f1"]); ?>" class="foto-medium" data-original="media/<?php print($linha_exercicio["foto_f1"]); ?>" alt="" />
												</a>
											</span>
										</label>
									</div>

									<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label text-bold">Movimento final </label>
										<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label">
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_exercicio["foto_f2"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/<?php print($linha_exercicio["foto_f2"]); ?>" class="foto-medium" data-original="media/<?php print($linha_exercicio["foto_f2"]); ?>" alt="" />
											</a>
										</span>
									</label>
								</div>


									</div> <!-- tab-pane  -->

								</div> <!-- tab-content -->

								<div class="form-actions">
									</form>
									<a href="admin.php?action=exercicios"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>
								</div>
								</fieldset>


						</div>
					</div><!--/span-->

				</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
