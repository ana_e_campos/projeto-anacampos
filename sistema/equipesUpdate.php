<?php

//VERIFICA A PERMISSÃO
if($_SESSION["equipe"]["editar"] == 1){
	$where = " AND u.idusuario = ".$_SESSION["dados_evolucao"]["idusuario"]."";
}

	$idusuario = isset($_GET["idusuario"]) ? numero($_GET["idusuario"]) : "";
	$bind = array($idusuario);
	$sql_usuario = $db->query("SELECT u.*, p.*
														 FROM usuario u
														 LEFT JOIN perfil p
														 ON u.idperfil = p.idperfil
														 WHERE u.idusuario = ? AND u.ativo = 1 ".$where."
														 GROUP BY u.idusuario
														 LIMIT 1", $bind);
	//print $sql_usuario;break;
	$linha_usuario = $db->fetchArray($sql_usuario);
?>

		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=equipes">Equipes</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=equipesUpdate&idusuario=<?php print(numero($_GET['idusuario'])); ?>">
						Editar
					</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["equipes"]["editar"] == 1 || $_SESSION["equipe"]["editar"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-user"></i>
							<span class="break"></span>
							Equipe
						</h2>
					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="php/equipeUpdate.php" name="ediequipe">
						  <fieldset>

								<ul class="nav nav-tabs">

									<li class="active">
										<a href="#pessoal" data-toggle="tab">Pessoal</a>
									</li>
									<li>
										<a href="#acesso" data-toggle="tab">Acesso</a>
									</li>
								</ul>

							<div class="tab-content">

							 <div class="tab-pane active" id="pessoal">

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS PESSOAIS </label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">FOTO </label>
									<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
								</div>
							</div>

							<div class="control-group">
									<label class="control-label">
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_usuario["foto"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/<?php print($linha_usuario["foto"]); ?>" class="foto-medium" data-original="media/<?php print($linha_usuario["foto"]); ?>" alt="" />
											</a>
										</span>
									</label>
									<div class="controls">
										<input type="checkbox" name="remover" class="checkbox" /> Remover foto
									</div>
							</div>
							<div class="control-group">
							  <div class="control-label-bg">
									<label class="control-label">
								Substituir foto
								</label>
							</div>
							<div class="controls">
								 <img src="img/mini_sem-imagem.png" class="foto-mini" /><input class="input-file uniform_on" id="" type="file" name="foto">
								<input type="hidden" name="fotoantiga" value="<?php print($linha_usuario["foto"]); ?>">
							  </div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
							    <label class="control-label">Nome </label>
							  </div>
							  <div class="controls">
									<input class="input-xlarge" id="nome" type="text" name="nome" maxlength="200" value="<?php print_db($linha_usuario["nome"]); ?>" >
							  </div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Nascimento </label>
								</div>
								<div class="controls">
									<input type="text" class="input-small datepicker" id="nascimento" name="nascimento" value="<?php print(data_br($linha_usuario["nascimento"])); ?>" >
								</div>
							</div>


							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Telefone celular </label>
								</div>
								<div class="controls">
									<input class="input-medium telefone_celular" id="" type="text" name="celular" maxlength="14" value="<?php print_db($linha_usuario["celular"]); ?>">
								</div>
							</div>

						</div> <!-- tab-pane  -->

						<div class="tab-pane" id="acesso">

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS DE ACESSO</label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
								  <label style="margin-left:0px;" class="control-label vermelho">E-mail <small>(login)</small></label>
							 </div>
							  <div class="controls">
									<input class="input-large text-transform-none" id="email" type="text" name="email" maxlength="100" value="<?php print($linha_usuario["email"]); ?>" >
							  </div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Perfil de acesso</label>
							 </div>
								<div class="controls">
									<select  id="idperfil" name="idperfil" >
									<option value=""></option>
									<?php
										$sql_perfil = $db->query("SELECT idperfil, perfil FROM perfil WHERE ativo = 1 ORDER BY perfil");
										while($linha_perfil = $db->fetchArray($sql_perfil)){
										?>
										<option value="<?php print($linha_perfil["idperfil"]); ?>" <?php if($linha_perfil["idperfil"] == $linha_usuario["idperfil"]) print('selected'); ?>><?php print_db($linha_perfil["perfil"]); ?></option>
										<?php
									} // fim while perfil
										?>
									</select>
								</div>
							</div>

							<?php
								// somente o usuário logado pode alterar a sua senha
								if(numero($_GET["idusuario"]) == $_SESSION["dados_evolucao"]["idusuario"]){
							?>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label">Nova senha </label>
								</div>
								<div class="controls">
									<input class="input-medium text-transform-none" id="" type="password" name="nova_senha" maxlength="100" >
								</div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Confirmação de senha </label>
								</div>
								<div class="controls">
									<input class="input-medium text-transform-none" id="" type="password" name="nova_senha_conf" maxlength="100" >
								</div>
							</div>

							<?php
								} // fim somente o usuário logado pode alterar a sua senha
							 ?>

						</div> <!-- tab-pane  -->

					</div> <!-- tab-content -->

							<div class="form-actions">
							  <input type="submit" id='btediequipe' class="btn btn-large btn-evolucao text-transform-none" value="Salvar">
								<input type="hidden" name="idusuario" value="<?php print($linha_usuario["idusuario"]); ?>" >
								</form>
							  <a href="admin.php?action=equipes"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i>">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
