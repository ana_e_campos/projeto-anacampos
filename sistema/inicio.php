<?php
//$sql_aniversariantes = $db->query("SELECT idusuario FROM usuario WHERE ativo = 1 AND MONTH(nascimento) = MONTH(NOW())");
//$linha_aniversariantes = $db->numRows($sql_aniversariantes);
?>
<!-- start: Content -->
<div id="content" class="span10">

	<ul class="breadcrumb">
		<li>
			<i class="icon-sitemap"></i>
			<a href="admin.php?action=inicio">Início</a>
		</li>
	</ul>

	<?php
	//VERIFICA A PERMISSÃO
	if($_SESSION["inicio"]["ver"] == 1){
	?>

	<?php
	//VERIFICA A PERMISSÃO
	if($_SESSION["unidades"]["ver"] == 1 || $_SESSION["equipamentos"]["ver"] == 1 || $_SESSION["equipes"]["ver"] == 1 || $_SESSION["equipe"]["ver"] == 1){
	?>

	<div class="row-fluid">
		<div class="box blue span12">
			<div class="box-header">
				<h2><i class="icon-sitemap"></i>
					<span class="break"></span>
					Academia
				</h2>
			</div>
			<div class="box-content">

				<?php
				//VERIFICA A PERMISSÃO
				if($_SESSION["unidades"]["ver"] == 1){
				?>

				<a class="quick-button span2" href="admin.php?action=unidades">
					<i class="icon-home"></i>
					<p>Unidades</p>
				</a>

				<?php
				} //FIM VERIFICA A PERMISSÃO
				?>

				<?php
				//VERIFICA A PERMISSÃO
				if($_SESSION["equipamentos"]["ver"] == 1){
				?>

				<a class="quick-button span2" href="admin.php?action=equipamentos">
					<i class="icon-tags"></i>
					<p>Equipamentos</p>
				</a>

				<?php
				} //FIM VERIFICA A PERMISSÃO
				?>


				<?php
				//VERIFICA A PERMISSÃO
				if($_SESSION["equipes"]["ver"] == 1 || $_SESSION["equipe"]["ver"] == 1){
				?>

				<a class="quick-button span2" href="admin.php?action=equipes">
					<i class="icon-user"></i>
					<p>Equipe</p>
				</a>

				<?php
				} //FIM VERIFICA A PERMISSÃO
				?>

				<div class="clearfix"></div>

			</div>

		</div><!--/span-->

	</div><!--/row-->

	<?php
	} //FIM VERIFICA A PERMISSÃO
	?>

	<?php
	//VERIFICA A PERMISSÃO
	if($_SESSION["aniversariantes"]["ver"] == 1){
	?>

	<div class="row-fluid">
		<div class="box blue span12">
			<div class="box-header">
				<h2><i class="icon-sitemap"></i>
					<span class="break"></span>
					Visualização
				</h2>
			</div>
			<div class="box-content">

				<?php
				//VERIFICA A PERMISSÃO
				if($_SESSION["aniversariantes"]["ver"] == 1){
				?>

				<a class="quick-button span2" href="admin.php?action=aniversariantes">
					<i class="icon-gift"></i>
					<p>Aniversariantes</p>
				</a>

				<?php
				} //FIM VERIFICA A PERMISSÃO
				?>


				<div class="clearfix"></div>

			</div>

		</div><!--/span-->

	</div><!--/row-->

	<?php
	} //FIM VERIFICA A PERMISSÃO
	?>

	<?php
	//VERIFICA A PERMISSÃO
	if($_SESSION["treinos"]["ver"] == 1){
	?>

	<div class="row-fluid">
		<div class="box blue span12">
			<div class="box-header">
				<h2><i class="icon-sitemap"></i>
					<span class="break"></span>
					Alunos
				</h2>
			</div>
			<div class="box-content">

				<?php
				//VERIFICA A PERMISSÃO
				if($_SESSION["alunos"]["ver"] == 1){
				?>

				<a class="quick-button span2" href="admin.php?action=alunos">
					<i class="icon-user"></i>
					<p>Alunos</p>
				</a>

				<?php
				} //FIM VERIFICA A PERMISSÃO
				?>

				<div class="clearfix"></div>

			</div>

		</div><!--/span-->

	</div><!--/row-->

	<?php
	} //FIM VERIFICA A PERMISSÃO
	?>

	<?php
	//VERIFICA A PERMISSÃO
	if($_SESSION["perfis"]["ver"] == 1 || $_SESSION["musculos"]["ver"] == 1 || $_SESSION["exercicios"]["ver"] == 1){
	?>

	<div class="row-fluid">
		<div class="box blue span12">
			<div class="box-header">
				<h2><i class="icon-sitemap"></i>
					<span class="break"></span>
					Configuração
				</h2>
			</div>
			<div class="box-content">

				<?php
				//VERIFICA A PERMISSÃO
				if($_SESSION["perfis"]["ver"] == 1){
				?>

				<a class="quick-button span2" href="admin.php?action=perfis">
					<i class="icon-lock"></i>
					<p>Perfis de acesso</p>
				</a>

				<?php
				} //FIM VERIFICA A PERMISSÃO
				?>

				<?php
				//VERIFICA A PERMISSÃO
				if($_SESSION["musculos"]["ver"] == 1){
				?>

				<a class="quick-button span2" href="admin.php?action=musculos">
					<i class="icon-thumbs-up"></i>
					<p>Músculos</p>
				</a>

				<?php
				} //FIM VERIFICA A PERMISSÃO
				?>

				<?php
				//VERIFICA A PERMISSÃO
				if($_SESSION["exercicios"]["ver"] == 1){
				?>

				<a class="quick-button span2" href="admin.php?action=exercicios">
					<i class="icon-resize-horizontal"></i>
					<p>Exercícios</p>
				</a>

				<?php
				} //FIM VERIFICA A PERMISSÃO
				?>


				<div class="clearfix"></div>

			</div>

		</div><!--/span-->

	</div><!--/row-->

	<?php
	} //FIM VERIFICA A PERMISSÃO
	?>


	<?php
	} //FIM VERIFICA A PERMISSÃO
	else{
		include_once('include/permissao.php');
	}
	?>

</div><!--/.fluid-container-->

<!-- end: Content -->
