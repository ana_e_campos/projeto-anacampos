
		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=perfis">Perfis</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=perfisInsert">Cadastrar</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["perfis"]["cadastrar"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-key"></i><span class="break"></span>
							Perfis
						</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="php/perfilInsert.php" name="cadperfil">

						  <fieldset>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS GERAIS </label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
							    <label class="control-label">Perfil </label>
							  </div>
							  <div class="controls">
									<input class="input-xlarge" id="perfil" type="text" name="perfil" maxlength="50" >
							  </div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">NÍVEIS DE ACESSO </label>
								</div>
							</div>

							<div class="control-group">
							  <div class="controls">
									<table class="table table-bordered bootstrap-datatable">
									  <thead>
										  <tr>
												<th width="50">Permissão</th>
												<th width="50">Ver</th>
												<th width="50">Cadastrar</th>
												<th width="50">Editar</th>
												<th width="50">Excluir</th>
										  </tr>
									  </thead>
									  <tbody>

											<?php
												$sql_paginas = $db->query("SELECT idpagina, pagina FROM pagina WHERE ativo = 1 ORDER BY pagina");
												#print $sql_paginas;break;
												while($linha_paginas = $db->fetchArray($sql_paginas)){
											?>

											<tr>
												<td style="vertical-align: middle;"><?php print_db(($linha_paginas["pagina"])); ?></td>
												<input type="hidden" value="<?php print_db($linha_paginas["idpagina"]); ?>" name="idpagina[]" />
												<td style="vertical-align: middle;">
													<input type="checkbox" name="ver[]" class="checkbox" />
												</td>
												<td style="vertical-align: middle;">
													<input type="checkbox" name="cadastrar[]" class="checkbox" />
												</td>
												<td style="vertical-align: middle;">
													<input type="checkbox" name="editar[]" class="checkbox" />
												</td>
												<td style="vertical-align: middle;">
													<input type="checkbox" name="excluir[]" class="checkbox" />
												</td>
											</tr>

											<?php
										} // fecha consulta paginas
											?>

									  </tbody>
								  </table>
							  </div>
							</div>

							<div class="form-actions">
							  <input type="submit" id='btcadperfil' class="btn btn-large btn-evolucao text-transform-none" value="Salvar">
								</form>
							  <a href="admin.php?action=perfis"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
