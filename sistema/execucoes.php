<?php
	$idtreino = isset($_GET["idtreino"]) ? numero($_GET["idtreino"]) : "";
	$treino = isset($_GET["treino"]) ? filtra($_GET["treino"]) : "";
?>

			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=treinos">Treinos</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					Exercícios do treino
				</li>
			</ul>

			<?php include_once('include/status.php'); ?>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["exercicios"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-resize-horizontal"></i>
							<span class="break"></span>
							Exercícios do treino
							<?php
							//VERIFICA A PERMISSÃO
							if($_SESSION["treinos"]["cadastrar"] == 1){
							?>
								<a data-rel="tooltip" data-original-title="Novo cadastro" href="admin.php?action=execucoesInsert&idtreino=<?php print_db($idtreino); ?>&treino=<?php print_db($treino); ?>">
									<i class="halflings-icon white plus"></i>
								</a>
							<?php
								} //FIM VERIFICA A PERMISSÃO
							?>
						</h2>
						<div class="box-icon">
							<!--
							<a href="relatorios.php?tipo=Equipamentos"
							data-rel="tooltip" data-original-title="Gerar PDF"><i class="halflings-icon white white download-alt"></i></a>
						  -->
						</div>

					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th style="width: 50px">Foto</th>
									<th>Exercício</th>
								  <th>Ações</th>
							  </tr>
						  </thead>


							Treino:
							<span class="label label-important" style="margin: 0 10px 20px 0;">
								<?php print($treino); ?>
							</span>

						  <tbody>

								<?php
								$bind = array($idtreino);
								$sql_execucao = $db->query("SELECT te.*, t.titulo AS treino, e.titulo AS exercicio, e.foto_m1, e.foto_m2, e.foto_f1, e.foto_f2, m.titulo AS musculo
																					 				 FROM execucao te
																									 LEFT JOIN treino t
																									 ON te.idtreino = t.idtreino
																									 LEFT JOIN exercicio e
																									 ON te.idexercicio = e.idexercicio
																									 LEFT JOIN musculo m
																									 ON e.idmusculo = m.idmusculo
																					 			 	 WHERE te.idtreino = ? AND te.ativo = 1
																									 GROUP BY te.idexecucao
																									 ORDER BY t.titulo
																					 			 	 ", $bind);
									//print $sql_execucao;
									echo nl2br($db->getDebug());
									while($linha_execucao = $db->fetchArray($sql_execucao)){
								?>

								<tr>
									<td style="vertical-align: middle;">
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_execucao["foto_m1"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/mini_<?php print($linha_execucao["foto_m1"]); ?>" class="foto-mini" data-original="media/<?php print($linha_execucao["foto_m1"]); ?>" alt="" />
											</a>
										</span>
									</td>
									<td style="vertical-align: middle;">
										<?php print_db($linha_execucao["exercicio"]); ?> <br />
										<i class="icon-thumbs-up"></i>
										<strong><?php print_db($linha_execucao["musculo"]); ?></strong>
									</td>
									<td style="vertical-align: middle;">
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["treinos"]["ver"] == 1){
										?>
										<a class="btn btn-warning" data-rel="tooltip" data-original-title="Ver" href="admin.php?action=execucoesSelect&idexecucao=<?php print_db($linha_execucao["idexecucao"]); ?>&idexecucao=<?php print_db($linha_execucao["idexecucao"]); ?>&idexercicio=<?php print_db($linha_execucao["idexercicio"]); ?>&idtreino=<?php print_db($idtreino); ?>&treino=<?php print_db($treino); ?>">
											<i class="halflings-icon white zoom-in"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["treinos"]["editar"] == 1){
										?>
										<a class="btn btn-info" data-rel="tooltip" data-original-title="Editar" href="admin.php?action=execucoesUpdate&idexecucao=<?php print_db($linha_execucao["idexecucao"]); ?>&idexercicio=<?php print_db($linha_execucao["idexercicio"]); ?>&idtreino=<?php print_db($idtreino); ?>&treino=<?php print_db($treino); ?>">
											<i class="halflings-icon white edit"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["treinos"]["excluir"] == 1){
										?>
										<a class="btn btn-danger" data-rel="tooltip" data-original-title="Excluir" onclick="return confirm('Confirma a exclusão?');" href="php/execucaoDelete.php?idexecucao=<?php print_db($linha_execucao["idexecucao"]); ?>&idexecucao=<?php print_db($linha_execucao["idexecucao"]); ?>&idexercicio=<?php print_db($linha_execucao["idexercicio"]); ?>&idtreino=<?php print_db($idtreino); ?>&treino=<?php print_db($treino); ?>">
											<i class="halflings-icon white trash"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
									</td>
								</tr>

								<?php
							} // fecha consulta treino execucoes
								?>

						  </tbody>
					  </table>
					</div>
				</div><!--/span-->

						<a href="admin.php?action=treinos" style="float:right;"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

</div><!--/fluid-row-->
