
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="admin.php?action=musculos">Músculos</a></li>
			</ul>

			<?php include_once('include/status.php'); ?>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["musculos"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-thumbs-up"></i>
							<span class="break"></span>
							Músculos
							<?php
							//VERIFICA A PERMISSÃO
							if($_SESSION["musculos"]["cadastrar"] == 1){
							?>
								<a data-rel="tooltip" data-original-title="Novo cadastro" href="admin.php?action=musculosInsert">
									<i class="halflings-icon white plus"></i>
								</a>
							<?php
								} //FIM VERIFICA A PERMISSÃO
							?>
						</h2>
						<div class="box-icon">
							<!--
							<a href="relatorios.php?tipo=Equipamentos"
							data-rel="tooltip" data-original-title="Gerar PDF"><i class="halflings-icon white white download-alt"></i></a>
						  -->
						</div>

					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th>Título</th>
								  <th>Ações</th>
							  </tr>
						  </thead>
						  <tbody>

								<?php
									$sql_musculos = $db->query("SELECT * FROM musculo WHERE ativo = 1 ORDER BY titulo");
									#print $sql_musculos;break;
									//echo nl2br($db->getDebug());
									while($linha_musculos = $db->fetchArray($sql_musculos)){
								?>

								<tr>

									<td style="vertical-align: middle;">
										<?php print_db($linha_musculos["titulo"]); ?>
									</td>

									<td style="vertical-align: middle;">
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["musculos"]["ver"] == 1){
										?>
										<a class="btn btn-warning" data-rel="tooltip" data-original-title="Ver" href="admin.php?action=musculosSelect&idmusculo=<?php print_db($linha_musculos["idmusculo"]); ?>">
											<i class="halflings-icon white zoom-in"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["musculos"]["editar"] == 1){
										?>
										<a class="btn btn-info" data-rel="tooltip" data-original-title="Editar" href="admin.php?action=musculosUpdate&idmusculo=<?php print_db($linha_musculos["idmusculo"]); ?>">
											<i class="halflings-icon white edit"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["musculos"]["excluir"] == 1){
										?>
										<a class="btn btn-danger" data-rel="tooltip" data-original-title="Excluir" onclick="return confirm('Confirma a exclusão?');" href="php/musculoDelete.php?idmusculo=<?php print_db($linha_musculos["idmusculo"]); ?>">
											<i class="halflings-icon white trash"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
									</td>
								</tr>

								<?php
									} // fecha consulta musculos
								?>

						  </tbody>
					  </table>
					</div>
				</div><!--/span-->

						<a href="admin.php?action=inicio" style="float:right;"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

</div><!--/fluid-row-->
