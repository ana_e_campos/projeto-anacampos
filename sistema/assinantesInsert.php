
		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=assinantes">Assinante</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=assinanteInsert">Cadastrar</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["assinantes"]["cadastrar"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-user"></i>
							<span class="break"></span>
							Assinante
						</h2>
						<div class="box-icon">

						</div>
					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="php/assinanteInsert.php" name="cadassinante">
						  <fieldset>

								<ul class="nav nav-tabs">

									<li class="active">
										<a href="#pessoal" data-toggle="tab">Pessoal</a>
									</li>
									<?php
									/*
									<li>
										<a href="#endereco" data-toggle="tab">Endereço</a>
									</li>
									*/
									?>
									<li>
										<a href="#acesso" data-toggle="tab">Acesso</a>
									</li>
								</ul>

							<div class="tab-content">

							 <div class="tab-pane active" id="pessoal">

								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label text-bold">FOTO </label>
										<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
									</div>
								</div>

								<div class="control-group">
										<label class="control-label"><img src="img/mini_sem-imagem.png" class="foto-mini" /><input class="input-file uniform_on" id="" type="file" name="foto"></label>
									<div class="controls">

									</div>
								</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS PESSOAIS </label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Plano</label>
								</div>
								<div class="controls">
									<select id="idplano" name="idplano">
											<option value=""></option>
											<?php
											$sql_plano = $db->query("SELECT idplano, plano, valor FROM plano WHERE ativo = 1 ORDER BY plano");
											while($linha_plano = $db->fetchArray($sql_plano)){
											?>
											<option value="<?php print($linha_plano["idplano"]); ?>"> <?php print_db($linha_plano["plano"]." - R$ ".valor($linha_plano["valor"])); ?></option>
											<?php
										} // fim while plano
											?>
									</select>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
							    <label class="control-label">Nome </label>
							  </div>
							  <div class="controls">
									<input class="input-xlarge" id="nome" type="text" name="nome" maxlength="200" >
							  </div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Nascimento </label>
								</div>
								<div class="controls">
									<input type="text" class="input-small datepicker" id="nascimento" name="nascimento" >
								</div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">CPF/CNPJ</label>
								</div>
								<div class="controls">
									<input class="input-medium cpf" id="cpf" type="text" name="cpf" maxlength="14">
								</div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Telefone celular </label>
								</div>
								<div class="controls">
									<input class="input-medium telefone_celular" id="" type="text" name="celular" maxlength="14">
								</div>
							</div>

						</div> <!-- tab-pane  -->

						<?php
						/*

						<div class="tab-pane" id="endereco">

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS DO ENDEREÇO </label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label">CEP </label>
								</div>
								<div class="controls">
									<input  class="input-medium cep" id="cep" type="text" name="cep" maxlength="9" >
								</div>
							</div>
								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label" style="margin-left:0px;">Logradouro </label>
									</div>
									<div class="controls">
										<input  class="input-xlarge" id="logradouro" type="text" name="logradouro" maxlength="255" >
									</div>
								</div>
									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">Número </label>
										</div>
										<div class="controls">
											<input  class="input-medium" id="" type="text" name="numero" maxlength="25" >
										</div>
									</div>
									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">Complemento </label>
										</div>
										<div class="controls">
											<input  class="input-xlarge" id="" type="text" name="complemento" maxlength="255" >
										</div>
									</div>

								 <div class="control-group">
									<div class="control-label-bg">
										<label class="control-label">Bairro </label>
									</div>
									<div class="controls">
										<input  class="input-xlarge" id="bairro" type="text" name="bairro" maxlength="100" >
									</div>
								</div>
								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label" style="margin-left:0px;">Cidade </label>
									</div>
									<div class="controls">
										<input  class="input-xlarge" id="cidade" type="text" name="cidade" maxlength="100" >
									</div>
								</div>
								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label" style="margin-left:0px;">UF </label>
										<label class="control-label" style="margin-left:58px;">País </label>
									</div>
									<div class="controls">
										<input  class="input-mini text-transform-upper" id="uf" type="text" name="uf" maxlength="2" >
										<input  class="input-medium" style="width:178px;" id="pais" type="text" name="pais" value="Brasil" maxlength="50" >
									</div>
								</div>

						</div> <!-- tab-pane  -->

						*/
						?>

						<div class="tab-pane" id="acesso">

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS DE ACESSO</label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label vermelho">E-mail (login)</label>
								</div>
								<div class="controls">
									<input class="input-xlarge text-transform-none" id="email" type="text" name="email" maxlength="200">
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label">Senha </label>
								</div>
								<div class="controls">
									<input class="input-medium text-transform-none" id="" type="password" name="senha" maxlength="100" >
								</div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Confirmação de senha </label>
								</div>
								<div class="controls">
									<input class="input-medium text-transform-none" id="" type="password" name="senha_conf" maxlength="100" >
								</div>
							</div>

						</div> <!-- tab-pane  -->

					</div> <!-- tab-content -->

							<div class="form-actions">
							  <input type="submit" id='btcadassinante' class="btn btn-large btn-evolucao text-transform-none" value="Salvar">
								</form>
							  <a href="admin.php?action=assinantes"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i>">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
