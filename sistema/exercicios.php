
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="admin.php?action=exercicios">Exercícios</a></li>
			</ul>

			<?php include_once('include/status.php'); ?>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["exercicios"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-resize-horizontal"></i>
							<span class="break"></span>
							Exercícios
							<?php
							//VERIFICA A PERMISSÃO
							if($_SESSION["exercicios"]["cadastrar"] == 1){
							?>
								<a data-rel="tooltip" data-original-title="Novo cadastro" href="admin.php?action=exerciciosInsert">
									<i class="halflings-icon white plus"></i>
								</a>
							<?php
								} //FIM VERIFICA A PERMISSÃO
							?>
						</h2>
						<div class="box-icon">
							<!--
							<a href="relatorios.php?tipo=Equipamentos"
							data-rel="tooltip" data-original-title="Gerar PDF"><i class="halflings-icon white white download-alt"></i></a>
						  -->
						</div>

					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th style="width: 50px">Foto</th>
									<th>Exercício</th>
								  <th>Ações</th>
							  </tr>
						  </thead>
						  <tbody>

								<?php
									$sql_exercicios = $db->query("SELECT e.*, e.titulo AS exercicio, m.titulo AS musculo
																								FROM exercicio e
																								LEFT JOIN musculo m
																								ON e.idmusculo = m.idmusculo
																								WHERE e.ativo = 1
																								GROUP BY e.idexercicio
																								ORDER BY e.titulo");
									#print $sql_exercicios;break;
									//echo nl2br($db->getDebug());
									while($linha_exercicios = $db->fetchArray($sql_exercicios)){
								?>

								<tr>
									<td style="vertical-align: middle;">
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_exercicios["foto_m1"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/mini_<?php print($linha_exercicios["foto_m1"]); ?>" class="foto-mini" data-original="media/<?php print($linha_exercicios["foto_m1"]); ?>" alt="" />
											</a>
										</span>
									</td>
									<td style="vertical-align: middle;">
										<?php print_db($linha_exercicios["exercicio"]); ?> <br />
										<i class="icon-tags"></i>
										<strong><?php print_db($linha_exercicios["musculo"]); ?></strong>
									</td>
									<td style="vertical-align: middle;">
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["exercicios"]["ver"] == 1){
										?>
										<a class="btn btn-warning" data-rel="tooltip" data-original-title="Ver" href="admin.php?action=exerciciosSelect&idexercicio=<?php print_db($linha_exercicios["idexercicio"]); ?>">
											<i class="halflings-icon white zoom-in"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["exercicios"]["editar"] == 1){
										?>
										<a class="btn btn-info" data-rel="tooltip" data-original-title="Editar" href="admin.php?action=exerciciosUpdate&idexercicio=<?php print_db($linha_exercicios["idexercicio"]); ?>">
											<i class="halflings-icon white edit"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["exercicios"]["excluir"] == 1){
										?>
										<a class="btn btn-danger" data-rel="tooltip" data-original-title="Excluir" onclick="return confirm('Confirma a exclusão?');" href="php/exercicioDelete.php?idexercicio=<?php print_db($linha_exercicios["idexercicio"]); ?>">
											<i class="halflings-icon white trash"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
									</td>
								</tr>

								<?php
									} // fecha consulta exercicios
								?>

						  </tbody>
					  </table>
					</div>
				</div><!--/span-->

						<a href="admin.php?action=inicio" style="float:right;"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

</div><!--/fluid-row-->
