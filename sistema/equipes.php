
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="admin.php?action=equipes">Equipe</a></li>
			</ul>

			<?php include_once('include/status.php'); ?>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["equipes"]["ver"] == 1 || $_SESSION["equipe"]["ver"] == 1){
			?>


			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-user"></i>
							<span class="break"></span>
							Equipe
							<?php
							//VERIFICA A PERMISSÃO
							if($_SESSION["equipes"]["cadastrar"] == 1){
							?>
								<a data-rel="tooltip" data-original-title="Novo cadastro" href="admin.php?action=equipesInsert">
									<i class="halflings-icon white plus"></i>
								</a>
							<?php
								} //FIM VERIFICA A PERMISSÃO
							?>
						</h2>
						<div class="box-icon">
							<!--
							<a href="relatorios.php?tipo=Equipes"
							data-rel="tooltip" data-original-title="Gerar PDF"><i class="halflings-icon white white download-alt"></i></a>
						-->
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th style="width: 50px;">Foto</th>
								  <th>Membro</th>
								  <th>Ações</th>
							  </tr>
						  </thead>
						  <tbody>

								<?php

								//VERIFICA A PERMISSÃO
								if($_SESSION["equipe"]["ver"] == 1){
									$where = " AND u.idusuario = ".$_SESSION["dados_evolucao"]["idusuario"]."";
								}


									$sql_equipes = $db->query("SELECT u.*, p.perfil
																							FROM usuario u
																							LEFT JOIN perfil p
																							ON u.idperfil = p.idperfil
																							WHERE u.ativo = 1 AND p.perfil = 'Professor' OR p.perfil = 'Gerente' ".$where."
																							GROUP BY u.idusuario
																							ORDER BY u.nome");
									#print $sql_equipes;break;
									while($linha_equipes = $db->fetchArray($sql_equipes)){
								?>

								<tr>
									<td>
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_equipes["foto"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/mini_<?php print($linha_equipes["foto"]); ?>" class="foto-mini" data-original="media/<?php print($linha_equipes["foto"]); ?>" alt="" />
											</a>
										</span>
										<br />
									</td>
									<td style="vertical-align: middle;">
										<i class="icon-user"></i>
										<?php print_db($linha_equipes["nome"]); ?> <br />
										<strong>Login: <?php print_db($linha_equipes["email"]); ?></strong> <br />
										<i class="icon-phone-sign"></i>
										<?php print($linha_equipes["celular"]); ?>
										<br />
										Perfil:
										<span class="label label-apus">
											<?php print_db($linha_equipes["perfil"]); ?>
										</span>
										<br />
										<?php
											if($linha_equipes["tentativas"] < 10){
										?>
											Acesso: <strong>Permitido</strong>
										<?php
									   }else{
										?>
												Acesso: <strong class='vermelho'>Senha bloqueada</strong>
												<br />
												<?php
												//VERIFICA A PERMISSÃO
												if($_SESSION["equipes"]["editar"] == 1){
												?>
												<a data-rel="tooltip" data-original-title="Desbloquear acesso" onclick="return confirm('Confirma o desbloqueio?');" href="php/equipeAccess.php?idusuario=<?php print(numero($linha_equipes['idusuario'])); ?>">
													Clique aqui para desbloquear
												</a>
												<?php
													} //FIM VERIFICA A PERMISSÃO
												?>
										<?php
											}
										?>
									</td>

									<td style="vertical-align: middle;">
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["equipes"]["ver"] == 1 || $_SESSION["equipe"]["ver"] == 1){
										?>
										<a class="btn btn-warning" data-rel="tooltip" data-original-title="Ver" href="admin.php?action=equipesSelect&idusuario=<?php print(numero($linha_equipes["idusuario"])); ?>">
											<i class="halflings-icon white zoom-in"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["equipes"]["editar"] == 1 || $_SESSION["equipe"]["editar"] == 1){
										?>
										<a class="btn btn-info" data-rel="tooltip" data-original-title="Editar" href="admin.php?action=equipesUpdate&idusuario=<?php print(numero($linha_equipes["idusuario"])); ?>">
											<i class="halflings-icon white edit"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>

										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["equipes"]["excluir"] == 1 || $_SESSION["equipe"]["excluir"] == 1){
										?>
										<a class="btn btn-danger" data-rel="tooltip" data-original-title="Excluir" onclick="return confirm('Confirma a exclusão?');" href="php/equipeDelete.php?idusuario=<?php print(numero($linha_equipes["idusuario"])); ?>">
											<i class="halflings-icon white trash"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
									</td>
								</tr>

								<?php
									} // fecha consulta equipes
								?>

						  </tbody>
					  </table>
					</div>
				</div><!--/span-->

					<a href="admin.php?action=inicio" style="float:right;"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

</div><!--/fluid-row-->
