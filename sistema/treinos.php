
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="admin.php?action=treinos">Treinos</a></li>
			</ul>

			<?php include_once('include/status.php'); ?>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["treinos"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-calendar"></i>
							<span class="break"></span>
							Treinos
							<?php
							//VERIFICA A PERMISSÃO
							if($_SESSION["treinos"]["cadastrar"] == 1){
							?>
								<a data-rel="tooltip" data-original-title="Novo cadastro" href="admin.php?action=treinosInsert">
									<i class="halflings-icon white plus"></i>
								</a>
							<?php
								} //FIM VERIFICA A PERMISSÃO
							?>
						</h2>
						<div class="box-icon">
							<!--
							<a href="relatorios.php?tipo=Equipamentos"
							data-rel="tooltip" data-original-title="Gerar PDF"><i class="halflings-icon white white download-alt"></i></a>
						  -->
						</div>

					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th>Título</th>
									<th>Observações</th>
									<th>Cadastrado por</th>
								  <th>Ações</th>
							  </tr>
						  </thead>

						  <tbody>

								<?php
									$sql_treinos = $db->query("SELECT t.*, u.nome
																						 FROM treino t
																						 LEFT JOIN usuario u
																						 ON t.idusuario = u.idusuario
																						 WHERE t.ativo = 1
																						 GROUP BY t.idtreino
																						 ORDER BY t.titulo");
									#print $sql_treinos;break;
									//echo nl2br($db->getDebug());
									while($linha_treinos = $db->fetchArray($sql_treinos)){
								?>

								<tr>

									<td style="vertical-align: middle;">
										<i class="icon-calendar"></i>
										<strong><?php print_db($linha_treinos["titulo"]); ?></strong>
									</td>

									<td style="vertical-align: middle;">
										<?php print_db($linha_treinos["observacoes"]); ?>
									</td>

									<td style="vertical-align: middle;">
										<i class="icon-user"></i>
										<?php print_db($linha_treinos["nome"]); ?>
									</td>

									<td style="vertical-align: middle;">
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["treinos"]["ver"] == 1){
										?>
										<a class="btn btn-warning" data-rel="tooltip" data-original-title="Ver" href="admin.php?action=treinosSelect&idtreino=<?php print_db($linha_treinos["idtreino"]); ?>">
											<i class="halflings-icon white zoom-in"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["treinos"]["editar"] == 1){
										?>
										<a class="btn btn-info" data-rel="tooltip" data-original-title="Editar" href="admin.php?action=treinosUpdate&idtreino=<?php print_db($linha_treinos["idtreino"]); ?>">
											<i class="halflings-icon white edit"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["treinos"]["ver"] == 1){
										?>
										<a class="btn btn-success" data-rel="tooltip" data-original-title="Exercícios do treino" href="admin.php?action=treinosexercicios&idtreino=<?php print_db($linha_treinos["idtreino"]); ?>&treino=<?php print_db($linha_treinos["titulo"]); ?>">
											<i class="icon-resize-horizontal"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["treinos"]["excluir"] == 1){
										?>
										<a class="btn btn-danger" data-rel="tooltip" data-original-title="Excluir" onclick="return confirm('Confirma a exclusão?');" href="php/treinoDelete.php?idtreino=<?php print_db($linha_treinos["idtreino"]); ?>">
											<i class="halflings-icon white trash"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
									</td>
								</tr>

								<?php
									} // fecha consulta treinos
								?>

						  </tbody>
					  </table>
					</div>
				</div><!--/span-->

						<a href="admin.php?action=inicio" style="float:right;"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

</div><!--/fluid-row-->
