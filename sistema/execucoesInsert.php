<?php
	$idtreino = isset($_GET["idtreino"]) ? numero($_GET["idtreino"]) : "";
	$treino = isset($_GET["treino"]) ? filtra($_GET["treino"]) : "";
?>

		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=treinos">Treinos</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=execucoes&idtreino=<?php print_db($idtreino); ?>&treino=<?php print_db($treino); ?>">Exercícios do treino</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					Cadastrar
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["exercicios"]["cadastrar"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-resize-horizontal"></i>
							<span class="break"></span>
							Exercícios do treino
						</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="php/execucaoInsert.php" name="cadexecucao">
						  <fieldset>

								Treino:
								<span class="label label-important" style="margin: 0 10px 20px 0;">
									<?php print($treino); ?>
								</span>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label text-bold">DADOS GERAIS </label>
										</div>
									</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">Exercício </label>
										</div>
										<div class="controls">
											<select data-placeholder="Exercícios cadastrados no sistema" id="selectError" data-rel="chosen" name="idexercicio" style="width: 280px;">
												<option value=""> </option>
												<?php
												// somente para não repetir os grupos musculares
												$sql_musculo = $db->query("SELECT idmusculo, titulo FROM musculo WHERE ativo = 1 ORDER BY titulo");
												while($linha_musculo = $db->fetchArray($sql_musculo)){

												?>

												<optgroup label="<?php print($linha_musculo["titulo"]); ?>">

												<?php
													$bind = array($linha_musculo["idmusculo"]);
													$sql_exercicio = $db->query("SELECT e.idexercicio, e.titulo AS exercicio, m.titulo AS musculo
														                           FROM exercicio e
																											 LEFT JOIN musculo m
																											 ON e.idmusculo = m.idmusculo
																											 WHERE e.ativo = 1 AND m.idmusculo = ?
																											 GROUP BY e.idexercicio
																											 ORDER BY e.titulo", $bind);
													while($linha_exercicio = $db->fetchArray($sql_exercicio)){

												?>



													<option value="<?php print($linha_exercicio["idexercicio"]); ?>"> <?php print($linha_exercicio["exercicio"]); ?></option>

												<?php
													} // fim while exercicio
													?>
													</optgroup>
												<?php
													} // fim while musculo
												?>
											</select>
										</div>
									</div>

							<div class="form-actions">
								<input type="hidden" name="idtreino" value="<?php print($idtreino); ?>" />
								<input type="hidden" name="treino" value="<?php print($treino); ?>" />
							  <input type="submit" id='btcadexecucao' class="btn btn-large btn-evolucao text-transform-none" value="Salvar">
								</form>
							  <a href="admin.php?action=execucaos&idtreino=<?php print_db($idtreino); ?>&treino=<?php print_db($treino); ?>"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
