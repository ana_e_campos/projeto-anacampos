<?php

//VERIFICA A PERMISSÃO
if($_SESSION["equipe"]["ver"] == 1){
	$where = " AND u.idusuario = ".$_SESSION["dados_evolucao"]["idusuario"]."";
}

	$idusuario = isset($_GET["idusuario"]) ? numero($_GET["idusuario"]) : "";
	$bind = array($idusuario);
	$sql_usuario = $db->query("SELECT u.*, p.*, f.*
														 FROM usuario u
														 LEFT JOIN perfil p
														 ON u.idperfil = p.idperfil
														 LEFT JOIN funcao f
														 ON u.idfuncao = f.idfuncao
														 WHERE u.idusuario = ? AND u.ativo = 1 AND u.tipo = 'Equipe' ".$where."
														 GROUP BY u.idusuario
														 LIMIT 1", $bind);
	//print $sql_usuario;break;
	$linha_usuario = $db->fetchArray($sql_usuario);
?>

		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=equipes">Equipes</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=equipesSelect&idusuario=<?php print(numero($_GET['idusuario'])); ?>">
						Ver
					</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["equipes"]["ver"] == 1 || $_SESSION["equipe"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-user-md"></i>
							<span class="break"></span>
							Assinante
						</h2>
					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="php/equipeUpdate.php" name="ediequipe">
						  <fieldset>

								<ul class="nav nav-tabs">

									<li class="active">
										<a href="#pessoal" data-toggle="tab">Pessoal</a>
									</li>
									<?php
									/*
									<li>
										<a href="#endereco" data-toggle="tab">Endereço</a>
									</li>
									*/
									?>
									<li>
										<a href="#acesso" data-toggle="tab">Acesso</a>
									</li>
								</ul>

							<div class="tab-content">

							 <div class="tab-pane active" id="pessoal">

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS PESSOAIS </label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">FOTO </label>
									<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
								</div>
							</div>

							<div class="control-group">
									<label class="control-label">
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_usuario["foto"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/<?php print($linha_usuario["foto"]); ?>" class="foto-medium" data-original="media/<?php print($linha_usuario["foto"]); ?>" alt="" />
											</a>
										</span>
									</label>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
							    <label class="control-label">Nome </label>
							  </div>
							  <div class="controls">
									<input disabled class="input-xlarge" id="nome" type="text" name="nome" maxlength="200" value="<?php print_db($linha_usuario["nome"]); ?>" >
							  </div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Nascimento </label>
								</div>
								<div class="controls">
									<input disabled type="text" class="input-small datepicker" id="nascimento" name="nascimento" value="<?php print(data_br($linha_usuario["nascimento"])); ?>" >
								</div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">CPF/CNPJ</label>
								</div>
								<div class="controls">
									<input disabled class="input-medium cpf" id="cpf" type="text" name="cpf" maxlength="14" value="<?php print($linha_usuario["cpf"]); ?>">
								</div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
								  <label class="control-label" style="margin-left:0px;">Função</label>
							 </div>
							  <div class="controls">
									<select disabled id="idfuncao" name="idfuncao" >
									<option value=""></option>
									<?php
										$sql_funcao = $db->query("SELECT idfuncao, funcao FROM funcao WHERE ativo = 1 ORDER BY funcao");
										while($linha_funcao = $db->fetchArray($sql_funcao)){
										?>
										<option value="<?php print($linha_funcao["idfuncao"]); ?>" <?php if($linha_funcao["idfuncao"] == $linha_usuario["idfuncao"]) print('selected'); ?>> <?php print_db($linha_funcao["funcao"]); ?></option>
										<?php
									} // fim while funcao
										?>
									</select>
							  </div>
							</div>
							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Telefone celular </label>
								</div>
								<div class="controls">
									<input disabled class="input-medium telefone_celular" id="" type="text" name="celular" maxlength="14" value="<?php print_db($linha_usuario["celular"]); ?>">
								</div>
							</div>

						</div> <!-- tab-pane  -->

						<?php
						/*

						<div class="tab-pane" id="endereco">

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS DO ENDEREÇO </label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label">CEP </label>
								</div>
								<div class="controls">
									<input disabled  class="input-medium cep" id="cep" type="text" name="cep" maxlength="9" value="<?php print_db($linha_usuario["cep"]); ?>" >
								</div>
							</div>
								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label" style="margin-left:0px;">Logradouro </label>
									</div>
									<div class="controls">
										<input disabled  class="input-xlarge" id="logradouro" type="text" name="logradouro" maxlength="255" value="<?php print_db($linha_usuario["logradouro"]); ?>">
									</div>
								</div>
									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">Número </label>
										</div>
										<div class="controls">
											<input disabled  class="input-medium" id="" type="text" name="numero" maxlength="25" value="<?php print_db($linha_usuario["numero"]); ?>" >
										</div>
									</div>
									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">Complemento </label>
										</div>
										<div class="controls">
											<input disabled  class="input-xlarge" id="" type="text" name="complemento" maxlength="255" value="<?php print_db($linha_usuario["complemento"]); ?>" >
										</div>
									</div>

								 <div class="control-group">
									<div class="control-label-bg">
										<label class="control-label">Bairro </label>
									</div>
									<div class="controls">
										<input disabled  class="input-xlarge" id="bairro" type="text" name="bairro" maxlength="100" value="<?php print_db($linha_usuario["bairro"]); ?>" >
									</div>
								</div>
								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label" style="margin-left:0px;">Cidade </label>
									</div>
									<div class="controls">
										<input disabled  class="input-xlarge" id="cidade" type="text" name="cidade" maxlength="100" value="<?php print_db($linha_usuario["cidade"]); ?>" >
									</div>
								</div>
								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label" style="margin-left:0px;">UF </label>
										<label class="control-label" style="margin-left:58px;">País </label>
									</div>
									<div class="controls">
										<input disabled  class="input-mini text-transform-upper" id="uf" type="text" name="uf" maxlength="2" value="<?php print_db($linha_usuario["uf"]); ?>" >
										<input disabled  class="input-medium" style="width:178px;" id="pais" type="text" name="pais" maxlength="50" value="<?php print_db($linha_usuario["pais"]); ?>">
									</div>
								</div>

						</div> <!-- tab-pane  -->

						*/
						?>

						<div class="tab-pane" id="acesso">

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS DE ACESSO</label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
								  <label style="margin-left:0px;" class="control-label vermelho">E-mail <small>(login)</small></label>
							 </div>
							  <div class="controls">
									<input disabled class="input-medium text-transform-none" id="email" type="text" name="email" maxlength="100" value="<?php print($linha_usuario["email"]); ?>" disabled >
							  </div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label" style="margin-left:0px;">Perfil de acesso</label>
							 </div>
								<div class="controls">
									<select disabled  id="idperfil" name="idperfil" >
									<option value=""></option>
									<?php
										$sql_perfil = $db->query("SELECT idperfil, perfil FROM perfil WHERE ativo = 1 ORDER BY perfil");
										while($linha_perfil = $db->fetchArray($sql_perfil)){
										?>
										<option value="<?php print($linha_perfil["idperfil"]); ?>" <?php if($linha_perfil["idperfil"] == $linha_usuario["idperfil"]) print('selected'); ?>><?php print_db($linha_perfil["perfil"]); ?></option>
										<?php
									} // fim while perfil
										?>
									</select>
								</div>
							</div>

						</div> <!-- tab-pane  -->

					</div> <!-- tab-content -->

							<div class="form-actions">
							  <input type="submit" id='btediequipe' class="btn btn-large btn-evolucao text-transform-none" value="Salvar">
								<input type="hidden" name="idusuario" value="<?php print($linha_usuario["idusuario"]); ?>" >
								</form>
							  <a href="admin.php?action=equipes"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i>">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
