<?php

require_once('php/connection.php');

if(!isset($_SESSION["dados_evolucao"])){
        header("Location: logout.php");
		exit;
}
/*
if($_SESSION["dados_evolucao"]["ip"] != getenv("REMOTE_ADDR")){
	echo "
	<script type='text/javascript'>
	  alert('Seu IP mudou durante a navegacao, por questoes de seguranca entre novamente.');
	</script>
	";
     header("Location: logout.php");
	 exit;
}
*/
$dados = isset($_SESSION["dados_evolucao"]) ? $_SESSION["dados_evolucao"] : "";

//$sql_aniversariantes_dia = $db->query("SELECT idusuario FROM usuario WHERE ativo = 1 AND tipo != 'Fornecedor' AND DAY(nascimento) = DAY(NOW()) AND MONTH(nascimento) = MONTH(NOW())");
//$linha_aniversariantes_dia = $db->numRows($sql_aniversariantes_dia);


?>
<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>APUS Digital - Sistema web</title>
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='font/fonts.css' rel='stylesheet' type='text/css'>
  <link href='css/vanilla.datepicker.css' rel='stylesheet' type='text/css'>
  <link href='css/prettyPhoto.css' rel='stylesheet' type='text/css'>

	<!-- end: CSS -->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->

  <!-- start: Favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
	<link rel="manifest" href="img/favicons/site.webmanifest">
	<link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="theme-color" content="#ffffff">
	<!-- end: Favicon -->

</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="admin.php?action=inicio"><span><img class="img-responsive logo-nav" src="img/logo.png" alt="logo" style="width: 47%;" ></span></a>
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
				  <ul class="nav pull-right">

				<?php
					// barra de notificação
					//include("php/notifications.php")
				?>

						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> <?php print($_SESSION["dados_evolucao"]["email"]); ?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Conta</span>
								</li>

                <?php
                //VERIFICA A PERMISSÃO
                if($_SESSION["assinante"]["editar"] == 1){
                ?>

								<li>
                  <a href="admin.php?action=assinantesUpdate&idusuario=<?php print($_SESSION["dados_evolucao"]["idusuario"]); ?>">
                    <i class="halflings-icon user"></i>
                    Perfil
                  </a>
                </li>

                <?php
                } //FIM VERIFICA A PERMISSÃO
                ?>

                <?php
                //VERIFICA A PERMISSÃO
                if($_SESSION["equipes"]["editar"] == 1 || $_SESSION["equipe"]["editar"] == 1){
                ?>

								<li>
                  <a href="admin.php?action=equipesUpdate&idusuario=<?php print($_SESSION["dados_evolucao"]["idusuario"]); ?>">
                    <i class="halflings-icon user"></i>
                    Perfil
                  </a>
                </li>

                <?php
                } //FIM VERIFICA A PERMISSÃO
                ?>

								<li>
                  <a href="logout.php">
                    <i class="halflings-icon off"></i>
                    Sair
                  </a>
                </li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

		<div class="container-fluid-full">
		<div class="row-fluid">

			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">

            <?php
            //VERIFICA A PERMISSÃO
            if($_SESSION["inicio"]["ver"] == 1){
            ?>

						<li <?php if($_GET['action'] == "inicio") echo 'class="active";' ?>>
              <a href="admin.php?action=inicio">
                <i class="icon-sitemap"></i>
                <span class="hidden-tablet">Início</span>
              </a>
            </li>

            <?php
            } //FIM VERIFICA A PERMISSÃO
            ?>

            <?php
            //VERIFICA A PERMISSÃO
            if($_SESSION["unidades"]["ver"] == 1 || $_SESSION["equipamentos"]["ver"] == 1 || $_SESSION["equipes"]["ver"] == 1){
            ?>

            <li>
              <a class="dropmenu" href="#">
                <i class="icon-home"></i>
                <span class="hidden-tablet">Academia</span>
              </a>
              <ul>

                <?php
                //VERIFICA A PERMISSÃO
                if($_SESSION["unidades"]["ver"] == 1){
                ?>

                <li>
                  <a class="submenu" href="admin.php?action=unidades">
                    <i class="icon-home" style="margin-left:10px;"></i>
                    <span class="hidden-tablet"> Unidades</span></a>
                </li>

                <?php
                } //FIM VERIFICA A PERMISSÃO
                ?>

                <?php
                //VERIFICA A PERMISSÃO
                if($_SESSION["equipamentos"]["ver"] == 1){
                ?>

                <li>
                  <a class="submenu" href="admin.php?action=equipamentos">
                    <i class="icon-tags" style="margin-left:10px;"></i>
                    <span class="hidden-tablet"> Equipamentos</span></a>
                </li>

                <?php
                } //FIM VERIFICA A PERMISSÃO
                ?>

                <?php
                //VERIFICA A PERMISSÃO
                if($_SESSION["equipes"]["ver"] == 1 || $_SESSION["equipe"]["ver"] == 1){
                ?>

                <li>
                  <a class="submenu" href="admin.php?action=equipes">
                    <i class="icon-user" style="margin-left:10px;"></i>
                    <span class="hidden-tablet"> Equipe</span>
                  </a>
                </li>

                <?php
                } //FIM VERIFICA A PERMISSÃO
                ?>

              </ul>
            </li>

            <?php
            } //FIM VERIFICA A PERMISSÃO
            ?>

            <?php
            //VERIFICA A PERMISSÃO
            if($_SESSION["aniversariantes"]["ver"] == 1){
            ?>

            <li <?php if($_GET['action'] == "aniversariantes") echo 'class="active";' ?>>
              <a href="admin.php?action=aniversariantes">
                <i class="icon-gift"></i>
                <span class="hidden-tablet">Aniversariantes</span>
                <?php if($linha_aniversariantes_dia > 0){ ?>
                <span class="label label-important"> <?php print($linha_aniversariantes_dia); ?> </span>
                <?php } ?>
              </a>
            </li>

            <?php
            } //FIM VERIFICA A PERMISSÃO
            ?>


            <?php
            //VERIFICA A PERMISSÃO
            if($_SESSION["alunos"]["ver"] == 1){
            ?>

            <li <?php if($_GET['action'] == "alunos") echo 'class="active";' ?>>
              <a href="admin.php?action=alunos">
                <i class="icon-user"></i>
                <span class="hidden-tablet">Alunos</span>
              </a>
            </li>

            <?php
            } //FIM VERIFICA A PERMISSÃO
            ?>

            <?php
            //VERIFICA A PERMISSÃO
            if($_SESSION["musculos"]["ver"] == 1 || $_SESSION["exercicios"]["ver"] == 1 || $_SESSION["perfis"]["ver"] == 1){
            ?>

            <li>
              <a class="dropmenu" href="#">
                <i class="icon-cog"></i>
                <span class="hidden-tablet">Configuração</span>
              </a>
              <ul>

                <?php
                //VERIFICA A PERMISSÃO
                if($_SESSION["perfis"]["ver"] == 1){
                ?>

                <li>
                  <a class="submenu" href="admin.php?action=perfis">
                    <i class="icon-key" style="margin-left:10px;"></i>
                    <span class="hidden-tablet"> Perfis de acesso</span></a>
                </li>

                <?php
                } //FIM VERIFICA A PERMISSÃO
                ?>

                <?php
                //VERIFICA A PERMISSÃO
                if($_SESSION["musculos"]["ver"] == 1){
                ?>

                <li>
                  <a class="submenu" href="admin.php?action=musculos">
                    <i class="icon-thumbs-up" style="margin-left:10px;"></i>
                    <span class="hidden-tablet"> Músculos</span></a>
                </li>

                <?php
                } //FIM VERIFICA A PERMISSÃO
                ?>

                <?php
                //VERIFICA A PERMISSÃO
                if($_SESSION["exercicios"]["ver"] == 1){
                ?>

                <li>
                  <a class="submenu" href="admin.php?action=exercicios">
                    <i class="icon-resize-horizontal" style="margin-left:10px;"></i>
                    <span class="hidden-tablet"> Exercícios</span></a>
                </li>

                <?php
                } //FIM VERIFICA A PERMISSÃO
                ?>

              </ul>
            </li>

            <?php
            } //FIM VERIFICA A PERMISSÃO
            ?>




            <?php
            /*
            <footer class="direita" style="margin-top:10px;">
              <div class="row">
                    <div class="col-sm-6">
                      <p style="color: #fff; font-size:12px;">
                        <strong>&copy; <?php print(date('Y')); ?> Evolução Assessoria Esportiva</strong> <br />
                        Uma plataforma PSJP - CNPJ: 22.445.158/0001-08 <br />
                        <a href="#" style="color:#EE3137">Termos de uso</a>
                      </p>
                    </div>
              </div>
            </footer>

            */
            ?>

					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Aviso!</h4>
					<p>Você precisa ter o <a href="https://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> abilitado para utilizar este sistema.</p>
				</div>
			</noscript>

<!--  Inicio corpo  -->

<?php
	$pathpage = preg_replace("/[^0-9a-z]/i", '', $_GET['action']) . '.php';
	if(is_file($pathpage)){

		include($pathpage);

	}
	else{
?>
	<meta http-equiv='refresh' content='0; url=logout.php'>
<?php
	}
?>

<!--  Fim corpo  -->

		</div><!--/#content.span10-->
	</div><!--/fluid-row-->


	<div class="clearfix"></div>



	<!-- start: JavaScript-->

	<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	<script src="js/jquery.ui.touch-punch.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src='js/fullcalendar.min.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	<script src="js/jquery.chosen.min.js"></script>
	<!-- <script src="js/jquery.uniform.min.js"></script> -->
	<script src="js/jquery.cleditor.min.js"></script>
	<script src="js/jquery.noty.js"></script>
	<script src="js/jquery.elfinder.min.js"></script>
	<script src="js/jquery.raty.min.js"></script>
	<script src="js/jquery.iphone.toggle.js"></script>
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<script src="js/jquery.gritter.min.js"></script>
	<script src="js/jquery.imagesloaded.js"></script>
	<script src="js/jquery.masonry.min.js"></script>
	<script src="js/jquery.knob.modified.js"></script>
	<script src="js/jquery.sparkline.min.js"></script>
	<script src="js/counter.js"></script>
	<script src="js/retina.js"></script>
	<script src="js/jquery.mask.js"></script>
	<script src="js/jquery.alerts.js"></script>
  <script src="js/vanilla.datepicker.js"></script>
  <!--Validação e customização-->
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/jquery.autocomplete.js"></script>
  <script src="js/validation.js"></script>
  <script src="js/custom.js"></script>

	<!-- end: JavaScript-->

</body>
</html>
