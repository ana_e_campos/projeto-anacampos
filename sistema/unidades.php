
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="admin.php?action=unidades">Unidades</a></li>
			</ul>

			<?php include_once('include/status.php'); ?>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["unidades"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="halflings-icon white home"></i>
							<span class="break"></span>
							Unidades
							<?php
							//VERIFICA A PERMISSÃO
							if($_SESSION["unidades"]["cadastrar"] == 1){
							?>
								<a data-rel="tooltip" data-original-title="Novo cadastro" href="admin.php?action=unidadesInsert">
									<i class="halflings-icon white plus"></i>
								</a>
							<?php
								} //FIM VERIFICA A PERMISSÃO
							?>
						</h2>
						<div class="box-icon">
							<!--
							<a href="relatorios.php?tipo=Unidades"
							data-rel="tooltip" data-original-title="Gerar PDF"><i class="halflings-icon white white download-alt"></i></a>
						-->
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th style="width: 50px;">Foto</th>
									<th>Unidade</th>
								  <th>Ações</th>
							  </tr>
						  </thead>
						  <tbody>

								<?php
									$sql_unidades = $db->query("SELECT * FROM unidade WHERE ativo = 1 ORDER BY unidade");
									#print $sql_unidades;break;
									while($linha_unidades = $db->fetchArray($sql_unidades)){
								?>

								<tr>
									<td>
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_unidades["foto"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/mini_<?php print($linha_unidades["foto"]); ?>" class="foto-mini" data-original="media/<?php print($linha_unidades["foto"]); ?>" alt="" />
											</a>
										</span>
									</td>
									<td style="vertical-align: middle;">
										<strong><?php print_db($linha_unidades["unidade"]); ?></strong>
										<br />
										<?php print_db($linha_unidades["logradouro"].", ".$linha_unidades["numero"]); ?>
										<br />
										<?php print_db($linha_unidades["bairro"]); ?>
										<br />
										<?php print_db($linha_unidades["cidade"]."/".$linha_unidades["uf"]); ?>
									</td>
									<td style="vertical-align: middle;">
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["unidades"]["ver"] == 1){
										?>
										<a class="btn btn-warning" data-rel="tooltip" data-original-title="Ver" href="admin.php?action=unidadesSelect&idunidade=<?php print_db($linha_unidades["idunidade"]); ?>">
											<i class="halflings-icon white zoom-in"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["unidades"]["editar"] == 1){
										?>
										<a class="btn btn-info" data-rel="tooltip" data-original-title="Editar" href="admin.php?action=unidadesUpdate&idunidade=<?php print_db($linha_unidades["idunidade"]); ?>">
											<i class="halflings-icon white edit"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										/*
										//VERIFICA A PERMISSÃO
										if($_SESSION["unidades"]["ver"] == 1){
										?>
										<a class="btn btn-reverse" data-rel="tooltip" data-original-title="Congregações" href="admin.php?action=congregacoes&idunidade=<?php print($linha_unidades["idunidade"]); ?>">
											<i class="halflings-icon white home"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO


										//VERIFICA A PERMISSÃO
										if($_SESSION["unidades"]["ver"] == 1){
										?>
										<a class="btn btn-evolucao" data-rel="tooltip" data-original-title="Documentos" href="admin.php?action=documentos&tipo=Unidades&idunidade=<?php print($linha_unidades["idunidade"]); ?>">
											<i class="halflings-icon white folder-open"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
											*/
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["unidades"]["excluir"] == 1){
										?>
										<a class="btn btn-danger" data-rel="tooltip" data-original-title="Excluir" onclick="return confirm('Confirma a exclusão?');" href="php/unidadeDelete.php?idunidade=<?php print_db($linha_unidades["idunidade"]); ?>">
											<i class="halflings-icon white trash"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
									</td>
								</tr>

								<?php
									} // fecha consulta unidades
								?>

						  </tbody>
					  </table>
					</div>
				</div><!--/span-->

						<a href="admin.php?action=inicio" style="float:right;"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

</div><!--/fluid-row-->
