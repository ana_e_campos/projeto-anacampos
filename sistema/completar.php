<?php
session_start();
header("Content-Type: text/html; charset=UTF-8",true);

require_once('php/connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: logout.php");
	exit;
}

//pesquisa reuniões com o autocomplete
if ($_GET['tipo'] == 'pesquisareuniao') {
$q = filtra($_GET['q']);
$q = strtoupper($q);
$bind = array('%'.$q.'%');
$sql = $db->query("SELECT reuniao FROM escalalista WHERE ativo = 1 AND reuniao LIKE ? GROUP BY reuniao ORDER BY reuniao LIMIT 0,10", $bind);
while($row = $db->fetchArray($sql)){
	print_db(utf8_encode($row["reuniao"])."\n");
}
}

//pesquisa pregador externo com o autocomplete
if ($_GET['tipo'] == 'pesquisapregadorexterno') {
$q = filtra($_GET['q']);
$q = strtoupper($q);
$bind = array('%'.$q.'%');
$sql = $db->query("SELECT pregadorexterno FROM escalalista WHERE ativo = 1 AND pregadorexterno LIKE ? GROUP BY pregadorexterno ORDER BY reuniao LIMIT 0,10", $bind);
while($row = $db->fetchArray($sql)){
	print_db(utf8_encode($row["pregadorexterno"])."\n");
}
}

//pesquisa hora com o autocomplete
if ($_GET['tipo'] == 'pesquisahora') {
$q = filtra($_GET['q']);
$q = strtoupper($q);
$bind = array('%'.$q.'%');
$sql = $db->query("SELECT hora FROM escalalista WHERE ativo = 1 AND hora LIKE ? GROUP BY hora ORDER BY hora LIMIT 0,10", $bind);
while($row = $db->fetchArray($sql)){
	print_db(utf8_encode($row["hora"])."\n");
}
}

//pesquisa os servicos com o combo box dinâmico em ajax
if ($_POST['idtipo']) {
$q = numero($_POST['idtipo']);
$bind = array($q);
$sql = $db->query("SELECT idservico, servico, descricao
				   FROM servico
				   WHERE idtipo = ? AND ativo = 1
				   ORDER BY servico", $bind);

if($db->numRows($sql) == 0){
   print('<option value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>');
}else{
   	  print '<option value="">
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   	       </option>';
      while($row = $db->fetchArray($sql)){
      print '<option value="'.$row['idservico'].'">'.$row['servico'].'</option>';

   }
}
}


?>
