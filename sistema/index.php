<?php
// zera a sessão principal (caso o usuário anterior não tenha clicado em Sair)
session_start();
$_SESSION["dados_evolucao"] = null;
session_destroy();
require_once('php/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
<title>APUS Digital - Sistema web</title>
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='font/fonts.css' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
	<link rel="manifest" href="img/favicons/site.webmanifest">
	<link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="theme-color" content="#ffffff">
	<!-- end: Favicon -->

	<style type="text/css">
		body {background: url(img/bg-login-evolucao.jpg) !important;}
	</style>

</head>

<body>
	<div class="container-fluid-full">
		<div class="row-fluid">

			<div class="row-fluid">
				<div class="login-box">
					<div class="icons">
						<a href="../site/index.php"><i class="halflings-icon home"></i></a>
					</div>
					<h2 class="center-login">
						<img class="img-responsive" src="img/evolucao_branco.png" alt="logo" style="width: 82%;" >
					</h2>

					<?php include_once('include/status.php'); ?>

					<?php
					// se houve um link de recuperação de senha
					if(isset($_GET["email"]) && isset($_GET["token"])){
					?>

					<form class="form-horizontal" onsubmit="ShowLoading();" action="php/senhaUpdate.php" method="post" name="cadloginsenha">
						<fieldset>

							<div class="alert alert-info" style="font-size:15px; margin:5px;">
								<strong>Recuperação de senha:</strong> atualize-a abaixo.
							</div>

							<div class="input-prepend" style="margin-top:0px;" title="Nova senha">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large  text-transform-none" name="newpassword" id="newpassword" type="password" placeholder="Digite a nova senha" />
							</div>

							<div class="clearfix"></div>

							<div class="input-prepend" style="margin-top:-20px;" title="Confirmação de senha">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large  text-transform-none" name="newpasswordconfirmation" id="newpasswordconfirmation" type="password" placeholder="Confirme a nova senha" />
							</div>
							<div class="clearfix"></div>

							<div class="button-login" style="margin:-1px 0 0px 23px; float:left;">
								<button type="submit" id="btcadloginsenha" class="btn btn-primary">Salvar cadastro</button>
								<input type="hidden" name="email" value="<?php print($_GET["email"]); ?>"  />
								<input type="hidden" name="token" value="<?php print($_GET["token"]); ?>"  />
							  </form>
								<a href="index.php"><span class="btn btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Cancelar</span></a>
							</div>
							<div class="clearfix"></div>

					<?php
					 // fim se houve um link de recuperação de senha
				 	}else{

					?>

					<form class="form-horizontal" onsubmit="ShowLoading();" action="php/acesso.php" method="post" name="cadlogin">
						<fieldset>

							<div class="input-prepend" title="Usuário">
								<span class="add-on"><i class="halflings-icon envelope"></i></span>
								<input class="input-large  text-transform-none" name="username" value="<?php if(isset($_COOKIE["username"])) print(decryptCookie($_COOKIE["username"])); ?>" id="username" type="text" placeholder="Seu e-mail" autofocus >
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" style="margin-top:-20px;" title="Senha">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large  text-transform-none" name="password" value="<?php if(isset($_COOKIE["password"])) print(decryptCookie($_COOKIE["password"])); ?>" id="password" type="password" placeholder="Sua senha" />
							</div>
							<div class="clearfix"></div>

							<div class="button-login" style="margin-top:-5px;">
								<button type="submit" id="btcadlogin" class="btn btn-evolucao" style="width: 220px; margin-right: 17px">Entrar</button>
							</div>
							<div class="clearfix"></div>

							<?php
							// HABILITAR NOVAMENTE QUANDO OS COOKIES INTERAGIREM COM A SESSÃO
							/*
							<label class="remember cinza" for="remember" style="margin-top:-5px;">
								<input type="checkbox" id="remember" name="remember" class="checkbox" <?php if(isset($_COOKIE["password"])) print(($_COOKIE["remember"])); ?>  />
								Lembrar-me</label>
								*/
							?>
					</form>
					<p>
					<a href="#" data-toggle="modal" data-target="#passwordModal" class="vermelho forgot">Esqueceu a sua senha?</a>
					</p>
         <br />

					<?php
					 // fim else se houve um link de recuperação de senha
				 	}
					?>


				</div><!--/span-->
			</div><!--/row-->
		</div><!--/.fluid-container-->


		<p style="color: #fff; text-align: center; margin-top: -50px;">
			<strong>&copy; <?php print(date('Y')); ?> Evolução Assessoria Esportiva</strong> <br />
			Todos os direitos reservados. <br />
			<a href="https://apus.digital" class="azul">APUS Digital</a>
		</p>


		</div><!--/fluid-row-->

	<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	 <form onsubmit="ShowLoading();" action="php/senhaSelect.php" method="post" name="cadsenha">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Recuperação de senha</h4>
			  </div>
			  <div class="modal-body">
				<label>E-mail (cadastrado no sistema):</label>
				<input class="input-xlarge" id="email" type="text" name="email" maxlength="255" style="text-transform: lowercase" > <br />
				<small class="vermelho">*será enviado um link de recuperação</small>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
				<button type="submit" class="btn btn-evolucao" id="btcadsenha">Enviar</button>
			  </div>
			</div>
		  </div>
		</form>
	</div>

	<!-- start: JavaScript-->

	<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	<script src="js/jquery.ui.touch-punch.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src='js/fullcalendar.min.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	<script src="js/jquery.chosen.min.js"></script>
	<script src="js/jquery.uniform.min.js"></script>
	<script src="js/jquery.cleditor.min.js"></script>
	<script src="js/jquery.noty.js"></script>
	<script src="js/jquery.elfinder.min.js"></script>
	<script src="js/jquery.raty.min.js"></script>
	<script src="js/jquery.iphone.toggle.js"></script>
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<script src="js/jquery.gritter.min.js"></script>
	<script src="js/jquery.imagesloaded.js"></script>
	<script src="js/jquery.masonry.min.js"></script>
	<script src="js/jquery.knob.modified.js"></script>
	<script src="js/jquery.sparkline.min.js"></script>
	<script src="js/counter.js"></script>
	<script src="js/retina.js"></script>
	<script src="js/jquery.alerts.js"></script>
	<script src="js/validation.js"></script>
	<script src="js/custom.js"></script>

	<!-- end: JavaScript-->

</body>
</html>
