<?php
	$bind = array(numero($_GET["idunidade"]));
	$sql_unidade = $db->query("SELECT *
														 FROM unidade
														 WHERE idunidade = ? AND ativo = 1
														 LIMIT 1", $bind);
	//print $sql_unidade;break;
	$linha_unidade = $db->fetchArray($sql_unidade);
?>
		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=unidades">Unidades</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=unidadesSelect&idunidade=<?php print(numero($_GET['idunidade'])); ?>">Ver</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["unidades"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white home"></i><span class="break"></span>Unidades</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="" name="cadunidade">
						  <fieldset>

								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label text-bold">DADOS GERAIS </label>
									</div>
								</div>

								<div class="control-group">
									<div class="control-label-bg">
								    <label class="control-label">Nome da unidade </label>
								  </div>
								  <div class="controls">
										<input disabled class="input-xlarge" id="unidade" type="text" name="unidade" maxlength="200" value="<?php print_db($linha_unidade["unidade"]); ?>" >
								  </div>
								</div>
								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label">Telefone </label>
									</div>
									<div class="controls">
										<input disabled class="input-medium" id="" type="text" name="telefone" maxlength="15" value="<?php print_db($linha_unidade["telefone"]); ?>">
									</div>
								</div>

								<div class="control-group">
									<div class="control-label-bg">
										<label class="control-label text-bold">ENDEREÇO</label>
									</div>
								</div>

								<div class="control-group">
										<div class="control-label-bg">
										  <label class="control-label">CEP </label>
									  </div>
										<div class="controls">
											<input disabled class="input-medium cep" id="cep" type="text" name="cep" maxlength="9" value="<?php print_db($linha_unidade["cep"]); ?>">
										</div>
									</div>
									<div class="control-group">
											<div class="control-label-bg">
											  <label class="control-label" style="margin-left:0px;">Logradouro </label>
										  </div>
											<div class="controls">
												<input disabled class="input-xlarge" id="logradouro" type="text" name="logradouro" maxlength="255" value="<?php print_db($linha_unidade["logradouro"]); ?>">
											</div>
										</div>
										<div class="control-group">
												<div class="control-label-bg">
												  <label class="control-label" style="margin-left:0px;">Número </label>
											  </div>
												<div class="controls">
													<input disabled class="input-medium" id="" type="text" name="numero" maxlength="25" value="<?php print_db($linha_unidade["numero"]); ?>">
												</div>
										</div>
										<div class="control-group">
												<div class="control-label-bg">
													<label class="control-label" style="margin-left:0px;">Complemento </label>
												</div>
												<div class="controls">
													<input disabled class="input-xlarge" id="" type="text" name="complemento" maxlength="255" value="<?php print_db($linha_unidade["complemento"]); ?>">
												</div>
											</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label">Bairro </label>
										</div>
										<div class="controls">
											<input disabled class="input-xlarge" id="bairro" type="text" name="bairro" maxlength="100" value="<?php print_db($linha_unidade["bairro"]); ?>">
										</div>
									</div>
									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">Cidade </label>
										</div>
										<div class="controls">
											<input disabled class="input-xlarge" id="cidade" type="text" name="cidade" maxlength="100" value="<?php print_db($linha_unidade["cidade"]); ?>">
										</div>
									</div>
									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">UF </label>
										</div>
										<div class="controls">
											<input disabled class="input-mini text-transform-upper" id="uf" type="text" name="uf" maxlength="2" value="<?php print_db($linha_unidade["uf"]); ?>">
										</div>
									</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label text-bold">FOTO </label>
											<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
										</div>
									</div>

									<div class="control-group">
											<label class="control-label">
												<span id="gallery" class="gallery">
													<a href="media/<?php print($linha_unidade["foto"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
														<img src="media/<?php print($linha_unidade["foto"]); ?>" class="foto-medium" data-original="media/<?php print($linha_unidade["foto"]); ?>" alt="" />
													</a>
												</span>
											</label>
									</div>

								<div class="form-actions">
									</form>
								  <a href="admin.php?action=unidades"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i>">Voltar</span></a>
								</div>
							  </fieldset>

						</div>
					</div><!--/span-->

				</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
