<?php
session_start();
$_SESSION["dados_evolucao"] = null;
session_destroy();
// expira os cookies
setcookie("username","",time()-3600,"/");
setcookie("password","",time()-3600,"/");
setcookie("remember","",time()-3600,"/");
header("Location: index.php");
?>
