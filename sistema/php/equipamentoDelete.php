<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["equipamentos"]["excluir"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  $idequipamento = isset($_GET["idequipamento"]) ? numero($_GET["idequipamento"]) : 0;

	// inativa a equipamento
	$bind = array($idequipamento);
	$sql_equipamento = $db->query("UPDATE equipamento SET ativo = 0 WHERE idequipamento = ? LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE equipamento SET ativo = 0 WHERE idequipamento = ? LIMIT 1", $bind));

  if($sql_equipamento){
    // inserido com sucesso
    header("Location: ../admin.php?action=equipamentos&status=6");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=equipamentos&status=7");
  }

}

} //FIM VERIFICA A PERMISSÃO
?>
