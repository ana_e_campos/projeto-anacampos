<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["exercicios"]["excluir"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  $idexecucao       = isset($_GET["idexecucao"]) ? numero($_GET["idexecucao"]) : "";
  $idexercicio    = isset($_GET["idexercicio"]) ? numero($_GET["idexercicio"]) : "";
  $idtreino       = isset($_GET["idtreino"]) ? numero($_GET["idtreino"]) : "";
  $treino       = isset($_GET["treino"]) ? filtra($_GET["treino"]) : "";

	// inativa a execucao
	$bind = array($idexecucao);
	$sql_execucao = $db->query("UPDATE execucao SET ativo = 0 WHERE idexecucao = ? LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE execucao SET ativo = 0 WHERE idexecucao = ? LIMIT 1", $bind));

  if($sql_execucao){
    // inserido com sucesso
    header("Location: ../admin.php?action=execucoes&status=6&idtreino=".$idtreino."&treino=".$treino."");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=execucoes&status=7&idtreino=".$idtreino."&treino=".$treino."");
  }

}

} //FIM VERIFICA A PERMISSÃO
?>
