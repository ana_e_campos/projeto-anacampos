<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["exercicios"]["editar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $idexecucao       = isset($_POST["idexecucao"]) ? numero($_POST["idexecucao"]) : "";
  $idexercicio    = isset($_POST["idexercicio"]) ? numero($_POST["idexercicio"]) : "";
  $idtreino       = isset($_POST["idtreino"]) ? numero($_POST["idtreino"]) : "";
  $treino       = isset($_POST["treino"]) ? filtra($_POST["treino"]) : "";

  // atualiza no banco
  $bind = array($idtreino, $idexercicio, $idexecucao);
	$sql_execucao = $db->query("UPDATE execucao SET idtreino=?, idexercicio=? WHERE idexecucao = ? AND ativo = 1 LIMIT 1", $bind);
  //print $sql_execucao; break;
  //echo nl2br($db->getDebug());break;

  // insere no log
  salvaLog($db->mostraquery("UPDATE execucao SET idtreino=?, idexercicio=? WHERE idexecucao = ? AND ativo = 1 LIMIT 1", $bind));

  if($sql_execucao){
    // editado com sucesso
    header("Location: ../admin.php?action=execucoes&status=8&idtreino=".$idtreino."&treino=".$treino."");
  }
  else{
    // erro ao editar
    header("Location: ../admin.php?action=execucoes&status=3&idtreino=".$idtreino."&treino=".$treino."");
  }

}

} //FIM VERIFICA A PERMISSÃO

?>
