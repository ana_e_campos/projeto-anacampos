<?php
ob_start();
require_once('connection.php');

//session_start();
$_SESSION["dados_evolucao"] = null;
//session_destroy();

if(getenv("REQUEST_METHOD") == "GET"){

    colocar pemrissão somente adm apus

  $idusuario  = isset($_GET["idusuario"]) ? numero($_GET["idusuario"]) : 0;

	//pega o ip
	$ip=getenv("REMOTE_ADDR");

	// consulta o usuário no banco
	$bind = array($idusuario);
	$sql = $db->query("SELECT u.*, p.perfil, pl.plano, pl.desconto, pl.credito, pl.valor, pl.endereco
                    FROM usuario u
                    LEFT JOIN perfil p
                    ON u.idperfil = p.idperfil
                    LEFT JOIN plano pl
                    ON u.idplano = pl.idplano
                    WHERE u.idusuario = ? AND u.tipo = 'Assinante' AND u.ativo = 1
                    GROUP BY u.idusuario
                    LIMIT 1", $bind);
  //echo nl2br($db->getDebug());
  //print $sql;
  //break;

  // insere no log
  salvaLog($db->mostraquery("SELECT u.*, p.perfil, pl.plano, pl.desconto, pl.credito, pl.valor, pl.endereco
                    FROM usuario u
                    LEFT JOIN perfil p
                    ON u.idperfil = p.idperfil
                    LEFT JOIN plano pl
                    ON u.idplano = pl.idplano
                    WHERE u.idusuario = ? AND u.tipo = 'Assinante' AND u.ativo = 1
                    GROUP BY u.idusuario
                    LIMIT 1", $bind));

  //break;

	if($db->numRows($sql)){

		$linha = $db->fetchArray($sql);

		//cria um array com os valores da session do usuário
		$dados                	= array();
    $dados["idusuario"] 	  = $linha['idusuario'];
    $dados["idperfil"]  		= $linha['idperfil'];
    $dados["tipo"]  		    = $linha['tipo'];
		$dados["nome"]          = $linha['nome'];
		$dados["email"]   	 	  = $linha['email'];
		$dados["foto"]   	 	    = $linha['foto'];
    $dados["perfil"]  		  = $linha['perfil'];
    $dados["idplano"]  		  = $linha['idplano'];
    $dados["plano"]  		    = $linha['plano'];
    $dados["desconto"]  		= $linha['desconto'];
    $dados["credito"]  		  = $linha['credito'];
    $dados["valor"]  		    = $linha['valor'];
    $dados["endereco"]      = $linha['endereco'];
		$dados["ip"]    	 	    = getenv("REMOTE_ADDR");
    $_SESSION["dados_evolucao"]	= $dados;

    //print $_SESSION["dados_evolucao"]["nome"];break;

    // consulta as permissões do perfil do usuário
    $bind = array($linha['idperfil']);
    $sql_perfil = $db->query("SELECT pa.pagina, p.ver, p.cadastrar, p.editar, p.excluir
                              FROM permissao p
                              LEFT JOIN pagina pa
                              ON p.idpagina = pa.idpagina
                              WHERE p.idperfil = ? AND p.ativo = 1
                              GROUP BY p.idpermissao", $bind);
    //print $sql_perfil;break;

    while($linha_perfil = $db->fetchArray($sql_perfil)){

      //cria sessions com os valores dos arrays do perfil
      $perfil                	= array();
      $perfil['pagina']       = $linha_perfil['pagina'];
      $perfil['ver']          = $linha_perfil['ver'];
      $perfil['cadastrar']    = $linha_perfil['cadastrar'];
      $perfil['editar']       = $linha_perfil['editar'];
      $perfil['excluir']      = $linha_perfil['excluir'];
      $_SESSION[$linha_perfil['pagina']] = $perfil;

      //exemplo de saída: Array ( [pagina] => inicio [ver] => 1 [cadastrar] => 1 [editar] => 1 [excluir] => 0 )
      /*
      print '<br />';
      print_r($_SESSION[$linha_perfil['pagina']]);
      print '<br />';
      //exemplo de saída: 0
      print $_SESSION["inicio"]["excluir"].'<br /><br />';
      break;
      */

    } // fim while perfil

    // redireciona para a página logada (com a nova identidade)
    header("Location: ../admin.php?action=inicio");
  }
  else{
		  header("Location: ../index.php?status=3"); // email ou senha errados
		  exit;
	}
}
?>
