<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["treinos"]["cadastrar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $titulo        = isset($_POST["titulo"]) ? filtra($_POST["titulo"]) : "";
  $observacoes   = isset($_POST["observacoes"]) ? filtra($_POST["observacoes"]) : "";

  // insere no banco o treino
  $bind = array($titulo, $observacoes, $_SESSION["dados_evolucao"]["idusuario"]);
	$sql_treino = $db->query("INSERT INTO treino (titulo, observacoes, idusuario) VALUES (?, ?, ?)", $bind);
  //print $sql_treino; break;

  // insere no log o treino
  salvaLog($db->mostraquery("INSERT INTO treino (titulo, observacoes, idusuario) VALUES (?, ?, ?)", $bind));

  if($sql_treino){
    // inserido com sucesso
    header("Location: ../admin.php?action=treinos&status=1");
  }
  else{
    // erro ao inserir
    header("Location: ../admin.php?action=treinos&status=3");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
