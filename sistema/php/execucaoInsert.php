<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["execucaos"]["cadastrar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $idexercicio    = isset($_POST["idexercicio"]) ? numero($_POST["idexercicio"]) : "";
  $idtreino       = isset($_POST["idtreino"]) ? numero($_POST["idtreino"]) : "";
  $treino       = isset($_POST["treino"]) ? filtra($_POST["treino"]) : "";

  // insere no banco o execucao
  $bind = array($idtreino, $idexercicio);
	$sql_execucao = $db->query("INSERT INTO execucao (idtreino, idexercicio) VALUES (?,?)", $bind);
  //print $sql_execucao;
  //echo nl2br($db->getDebug()); break;

  // insere no log a execucao
  salvaLog($db->mostraquery("INSERT INTO execucao (idtreino, idexercicio) VALUES (?,?)", $bind));

  if($sql_execucao){
    // inserido com sucesso
    header("Location: ../admin.php?action=execucoes&status=1&idtreino=".$idtreino."&treino=".$treino."");
  }
  else{
    // erro ao inserir
    header("Location: ../admin.php?action=execucoes&status=3&idtreino=".$idtreino."&treino=".$treino."");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
