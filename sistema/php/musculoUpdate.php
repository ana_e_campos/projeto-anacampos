<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["musculos"]["editar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $idmusculo     = isset($_POST["idmusculo"]) ? numero($_POST["idmusculo"]) : "";
  $titulo          = isset($_POST["titulo"]) ? filtra($_POST["titulo"]) : "";

  // atualiza no banco
  $bind = array($titulo, $idmusculo);
	$sql_musculo = $db->query("UPDATE musculo SET titulo=? WHERE idmusculo = ? AND ativo = 1 LIMIT 1", $bind);
  //print $sql_musculo; break;
  //echo nl2br($db->getDebug());break;

  // insere no log
  salvaLog($db->mostraquery("UPDATE musculo SET titulo=? WHERE idmusculo = ? AND ativo = 1 LIMIT 1", $bind));

  if($sql_musculo){
    // editado com sucesso
    header("Location: ../admin.php?action=musculos&status=8");
  }
  else{
    // erro ao editar
    header("Location: ../admin.php?action=musculos&status=3");
  }

}

} //FIM VERIFICA A PERMISSÃO

?>
