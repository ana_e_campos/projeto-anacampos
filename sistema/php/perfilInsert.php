<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}


//VERIFICA A PERMISSÃO
if($_SESSION["perfis"]["cadastrar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $perfil = isset($_POST["perfil"]) ? filtra($_POST["perfil"]) : "";

	// consulta o perfil no banco
	$bind = array($perfil);
	$sql_nome = $db->query("SELECT perfil FROM perfil WHERE perfil = ? LIMIT 1", $bind);

	if($db->numRows($sql_nome) == 0){

    // insere no banco o perfil
    $bind = array($perfil);
		$sql_perfil = $db->query("INSERT INTO perfil (perfil) VALUES (?)", $bind);
    #print $sql_perfil; break;
    $idperfil = $db->lastInsertId();

    // insere no log o perfil
    salvaLog($db->mostraquery("INSERT INTO perfil (perfil) VALUES (?)", $bind));

    //print_r($_POST["idpagina"]);
    //print '<br />';
    //print_r($_POST["ver"]);
    //print_r($_POST["editar"]);

    //break;

    // percorre as páginas listadas
    for($i=0; $i < count(array_filter($_POST['idpagina'])); $i++){

      $idpagina = isset($_POST["idpagina"][$i]) ? numero($_POST["idpagina"][$i]) : NULL;
      $ver = $_POST["ver"][$i] ? '1' : '0';
      $cadastrar = $_POST["cadastrar"][$i] ? '1' : '0';
      $editar = $_POST["editar"][$i] ? '1' : '0';
      $excluir = $_POST["excluir"][$i] ? '1' : '0';

      //insere as permissões
      $bind = array($idperfil, $idpagina, $ver, $cadastrar, $editar, $excluir);
      $sql_permissao = $db->query("INSERT INTO permissao (idperfil, idpagina, ver, cadastrar, editar, excluir) VALUES (?, ?, ?, ?, ?, ?)", $bind);
      //print $sql_permissao; break;

      // insere no log as permissões
      salvaLog($db->mostraquery("INSERT INTO permissao (idperfil, idpagina, ver, cadastrar, editar, excluir) VALUES (?, ?, ?, ?, ?, ?)", $bind));

    }

    if($sql_perfil){
      // inserido com sucesso
      header("Location: ../admin.php?action=perfis&status=1");
    }
    else{
      // erro ao inserir
      header("Location: ../admin.php?action=perfis&status=3");
    }
  }
  else{
    // erro de nome
    header("Location: ../admin.php?action=perfis&status=2");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
