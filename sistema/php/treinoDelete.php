<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["treinos"]["excluir"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  $idtreino = isset($_GET["idtreino"]) ? numero($_GET["idtreino"]) : 0;

	// inativa a treino
	$bind = array($idtreino);
	$sql_treino = $db->query("UPDATE treino SET ativo = 0 WHERE idtreino = ? LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE treino SET ativo = 0 WHERE idtreino = ? LIMIT 1", $bind));

  if($sql_treino){
    // inserido com sucesso
    header("Location: ../admin.php?action=treinos&status=6");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=treinos&status=7");
  }

}

} //FIM VERIFICA A PERMISSÃO
?>
