<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["exercicios"]["cadastrar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $titulo           = isset($_POST["titulo"]) ? filtra($_POST["titulo"]) : "";
  $idmusculo        = isset($_POST["idmusculo"]) ? numero($_POST["idmusculo"]) : "";

  require('wideimage/WideImage.php');
  $dir = "../media/";


  // FOTO MASCULINA 1

  $fotos_m1 = isset($_FILES["foto_m1"]) ? $_FILES["foto_m1"] : false;
  //$marca = $_POST["marca"] ? '0' : '0'; // nunca incluir a marca
  //$fotoantiga	= $_POST['fotoantiga'];

  if(!empty($fotos_m1['name'])){

    // renomeia o arquivo
    $foto_nome = $fotos_m1['name'];
    $foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    $foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
    $foto_m1 = md5($foto_nome).strtolower($foto_extensao);
    //print $foto;break;

      // novo formato de verificação do PHP (TYPE em desuso)
      $path_info = pathinfo($fotos_m1['name']);
      if(!preg_match( '/^(jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos_m1["size"] > 5000000){
        // formato de imagem não suportado
        header("Location: ../admin.php?action=exercicios&status=4");
      exit;
      }
      else{
        // renomeia o arquivo, se necessário
        if(file_exists($dir.$foto_m1)){
          $a = 1;
          while(file_exists($dir."[{$a}]{$foto_m1}"))
            $a++;
          $foto_m1 = "[{$a}]{$foto_m1}";
        }

        if(!@move_uploaded_file($fotos_m1['tmp_name'], $dir.$foto_m1)){

          // erro ao enviar a foto
          header("Location: ../admin.php?action=exercicios&status=5");
          exit;
        }
        else{
          $img = WideImage::load($dir.$foto_m1);
          $img = $img->resize(900, 900, 'inside');
          //$img = $img->resize('50%');

          if($marca == '1'){
            // insere a marca d'agua
            $marca = WideImage::load("../img/marca.png");
            $img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto_m1);
          }

          $img = WideImage::load($dir.$foto_m1);
          $img = $img->resize(320, 320, 'inside')->crop('center', 'center', 300, 300);
          $img->saveToFile($dir.'mini_'.$foto_m1);
        }
      }
  }
  // se não selecionou uma foto para o envio
  else{
    $foto_m1 = "sem-imagem.png";
  }

  // FOTO MASCULINA 2

  $fotos_m2 = isset($_FILES["foto_m2"]) ? $_FILES["foto_m2"] : false;
  //$marca = $_POST["marca"] ? '0' : '0'; // nunca incluir a marca
  //$fotoantiga	= $_POST['fotoantiga'];

  if(!empty($fotos_m2['name'])){

    // renomeia o arquivo
    $foto_nome = $fotos_m2['name'];
    $foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    $foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
    $foto_m2 = md5($foto_nome).strtolower($foto_extensao);
    //print $foto;break;

      // novo formato de verificação do PHP (TYPE em desuso)
      $path_info = pathinfo($fotos_m2['name']);
      if(!preg_match( '/^(jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos_m2["size"] > 5000000){
        // formato de imagem não suportado
        header("Location: ../admin.php?action=exercicios&status=4");
      exit;
      }
      else{
        // renomeia o arquivo, se necessário
        if(file_exists($dir.$foto_m2)){
          $a = 1;
          while(file_exists($dir."[{$a}]{$foto_m2}"))
            $a++;
          $foto_m2 = "[{$a}]{$foto_m2}";
        }

        if(!@move_uploaded_file($fotos_m2['tmp_name'], $dir.$foto_m2)){

          // erro ao enviar a foto
          header("Location: ../admin.php?action=exercicios&status=5");
          exit;
        }
        else{
          $img = WideImage::load($dir.$foto_m2);
          $img = $img->resize(900, 900, 'inside');
          //$img = $img->resize('50%');

          if($marca == '1'){
            // insere a marca d'agua
            $marca = WideImage::load("../img/marca.png");
            $img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto_m2);
          }

          $img = WideImage::load($dir.$foto_m2);
          $img = $img->resize(320, 320, 'inside')->crop('center', 'center', 300, 300);
          $img->saveToFile($dir.'mini_'.$foto_m2);
        }
      }
  }
  // se não selecionou uma foto para o envio
  else{
    $foto_m2 = "sem-imagem.png";
  }

  // FOTO FEMININA 1

  $fotos_f1 = isset($_FILES["foto_f1"]) ? $_FILES["foto_f1"] : false;
  //$marca = $_POST["marca"] ? '0' : '0'; // nunca incluir a marca
  //$fotoantiga	= $_POST['fotoantiga'];

  if(!empty($fotos_f1['name'])){

    // renomeia o arquivo
    $foto_nome = $fotos_f1['name'];
    $foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    $foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
    $foto_f1 = md5($foto_nome).strtolower($foto_extensao);
    //print $foto;break;

      // novo formato de verificação do PHP (TYPE em desuso)
      $path_info = pathinfo($fotos_f1['name']);
      if(!preg_match( '/^(jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos_f1["size"] > 5000000){
        // formato de imagem não suportado
        header("Location: ../admin.php?action=exercicios&status=4");
      exit;
      }
      else{
        // renomeia o arquivo, se necessário
        if(file_exists($dir.$foto_f1)){
          $a = 1;
          while(file_exists($dir."[{$a}]{$foto_f1}"))
            $a++;
          $foto_f1 = "[{$a}]{$foto_f1}";
        }

        if(!@move_uploaded_file($fotos_f1['tmp_name'], $dir.$foto_f1)){

          // erro ao enviar a foto
          header("Location: ../admin.php?action=exercicios&status=5");
          exit;
        }
        else{
          $img = WideImage::load($dir.$foto_f1);
          $img = $img->resize(900, 900, 'inside');
          //$img = $img->resize('50%');

          if($marca == '1'){
            // insere a marca d'agua
            $marca = WideImage::load("../img/marca.png");
            $img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto_f1);
          }

          $img = WideImage::load($dir.$foto_f1);
          $img = $img->resize(320, 320, 'inside')->crop('center', 'center', 300, 300);
          $img->saveToFile($dir.'mini_'.$foto_f1);
        }
      }
  }
  // se não selecionou uma foto para o envio
  else{
    $foto_f1 = "sem-imagem.png";
  }

  // FOTO FEMININA 2

  $fotos_f2 = isset($_FILES["foto_f2"]) ? $_FILES["foto_f2"] : false;
  //$marca = $_POST["marca"] ? '0' : '0'; // nunca incluir a marca
  //$fotoantiga	= $_POST['fotoantiga'];

  if(!empty($fotos_f2['name'])){

    // renomeia o arquivo
    $foto_nome = $fotos_f2['name'];
    $foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    $foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
    $foto_f2 = md5($foto_nome).strtolower($foto_extensao);
    //print $foto;break;

      // novo formato de verificação do PHP (TYPE em desuso)
      $path_info = pathinfo($fotos_f2['name']);
      if(!preg_match( '/^(jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos_f2["size"] > 5000000){
        // formato de imagem não suportado
        header("Location: ../admin.php?action=exercicios&status=4");
      exit;
      }
      else{
        // renomeia o arquivo, se necessário
        if(file_exists($dir.$foto_f2)){
          $a = 1;
          while(file_exists($dir."[{$a}]{$foto_f2}"))
            $a++;
          $foto_f2 = "[{$a}]{$foto_f2}";
        }

        if(!@move_uploaded_file($fotos_f2['tmp_name'], $dir.$foto_f2)){

          // erro ao enviar a foto
          header("Location: ../admin.php?action=exercicios&status=5");
          exit;
        }
        else{
          $img = WideImage::load($dir.$foto_f2);
          $img = $img->resize(900, 900, 'inside');
          //$img = $img->resize('50%');

          if($marca == '1'){
            // insere a marca d'agua
            $marca = WideImage::load("../img/marca.png");
            $img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto_f2);
          }

          $img = WideImage::load($dir.$foto_f2);
          $img = $img->resize(320, 320, 'inside')->crop('center', 'center', 300, 300);
          $img->saveToFile($dir.'mini_'.$foto_f2);
        }
      }
  }
  // se não selecionou uma foto para o envio
  else{
    $foto_f2 = "sem-imagem.png";
  }

  // insere no banco o exercicio
  $bind = array($idmusculo, $titulo, $foto_m1, $foto_m2, $foto_f1, $foto_f2);
	$sql_exercicio = $db->query("INSERT INTO exercicio (idmusculo, titulo, foto_m1, foto_m2, foto_f1, foto_f2) VALUES (?,?,?,?,?,?)", $bind);
  //print $sql_exercicio;
  //echo nl2br($db->getDebug()); break;

  // insere no log o exercicio
  salvaLog($db->mostraquery("INSERT INTO exercicio (idmusculo, titulo, foto_m1, foto_m2, foto_f1, foto_f2) VALUES (?,?,?,?,?,?)", $bind));

  if($sql_exercicio){
    // inserido com sucesso
    header("Location: ../admin.php?action=exercicios&status=1");
  }
  else{
    // erro ao inserir
    header("Location: ../admin.php?action=exercicios&status=3");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
