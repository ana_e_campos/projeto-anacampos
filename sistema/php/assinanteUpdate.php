<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["assinantes"]["editar"] == 1 || $_SESSION["assinante"]["editar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  //VERIFICA A PERMISSÃO
  if($_SESSION["assinante"]["editar"] == 1){
  	$where = " AND idusuario = ".$_SESSION["dados_evolucao"]["idusuario"]."";
  }

  $idusuario   = isset($_POST["idusuario"]) ? numero($_POST["idusuario"]) : "";
  $nome        = isset($_POST["nome"]) ? filtra($_POST["nome"]) : "";
  $nascimento  = isset($_POST["nascimento"]) ? data_en($_POST["nascimento"]) : NULL;
  $cpf         = isset($_POST["cpf"]) ? filtra($_POST["cpf"]) : "";
  $celular    = isset($_POST["celular"]) ? filtra($_POST["celular"]) : "";

  /*

  $cep                  = isset($_POST["cep"]) ? filtra($_POST["cep"]) : "";
  $logradouro           = isset($_POST["logradouro"]) ? filtra($_POST["logradouro"]) : "";
  $numero               = isset($_POST["numero"]) ? filtra($_POST["numero"]) : "";
  $complemento          = isset($_POST["complemento"]) ? filtra($_POST["complemento"]) : "";
  $bairro               = isset($_POST["bairro"]) ? filtra($_POST["bairro"]) : "";
  $cidade               = isset($_POST["cidade"]) ? filtra($_POST["cidade"]) : "";
  $uf                   = isset($_POST["uf"]) ? filtra($_POST["uf"]) : "";
  $pais                 = isset($_POST["pais"]) ? filtra($_POST["pais"]) : "";

  */

  //$email       = isset($_POST["email"]) ? filtra(strtolower($_POST["email"])) : "";
  $senha       = isset($_POST["nova_senha"]) ? filtraLogin($_POST["nova_senha"]) : "";

  $sql_perfil   = $db->query("SELECT idperfil FROM perfil WHERE ativo = 1 AND perfil = 'Assinante' LIMIT 1");
  $linha_perfil = $db->fetchArray($sql_perfil);
  $idperfil     = isset($linha_perfil["idperfil"]) ? numero($linha_perfil["idperfil"]) : "";


    require('wideimage/WideImage.php');
    $dir = "../media/";
    $fotos = isset($_FILES["foto"]) ? $_FILES["foto"] : false;
    $remover = $_POST["remover"] ? '1' : '0';
    $fotoantiga	= $_POST['fotoantiga'];

    if(!empty($fotos['name'])){

      // renomeia o arquivo
      $foto_nome = $fotos['name'];
    	$foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    	$foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
      $foto = md5($foto_nome).strtolower($foto_extensao);
      //print $foto;break;

        // novo formato de verificação do PHP (TYPE em desuso)
        $path_info = pathinfo($fotos['name']);
        if(!preg_match( '/^(jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos["size"] > 5000000){
          // formato de imagem não suportado
          header("Location: ../admin.php?action=assinantes&status=4");
        exit;
        }
        else{
          // renomeia o arquivo, se necessário
          if(file_exists($dir.$foto)){
            $a = 1;
            while(file_exists($dir."[{$a}]{$foto}"))
              $a++;
            $foto = "[{$a}]{$foto}";
          }

          if(!@move_uploaded_file($fotos['tmp_name'], $dir.$foto)){
            $erros[] = $fotos['name'];
            // erro ao enviar a foto
            header("Location: ../admin.php?action=assinantes&status=5");
            exit;
          }
          else{
            $img = WideImage::load($dir.$foto);
            $img = $img->resize(900, 900, 'inside');
            //$img = $img->resize('50%');
            //$marca = WideImage::load("imgs/marca.png");
            //$img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto);

            $img = WideImage::load($dir.$foto);
            $img = $img->resize(170, 170, 'inside')->crop('center', 'center', 150, 150);
            $img->saveToFile($dir.'mini_'.$foto);

            // exclui a foto antiga
            //if($fotoantiga != "sem-imagem.png"){
              //unlink($dir.$fotoantiga);
            //}

          }
        }
    }
    // se não selecionou uma foto para o envio
    else{
      if($remover == '1'){
        $foto = "sem-imagem.png";
      }
      else{
        $foto = $fotoantiga;
      }
    }

    // caso seja alterada a senha
    if($senha != "" ){
      // atualiza no banco
      $bind = array($nome,$nascimento,$cpf,$celular,$idperfil, $senha, $foto, $idusuario);
  		$sql_usuario = $db->query("UPDATE usuario SET nome=?,nascimento=?,cpf=?,celular=?,idperfil=?, senha=PASSWORD(?), foto=? WHERE idusuario = ? ".$where." LIMIT 1", $bind);
      #print $sql_usuario; break;

      // insere no log
      $bind = array($nome,$nascimento,$cpf,$celular,$idperfil, $foto, $idusuario);
      salvaLog($db->mostraquery("UPDATE usuario SET nome=?,nascimento=?,cpf=?,celular=?,idperfil=?, senha='XXXXX', foto=? WHERE idusuario = ? ".$where." LIMIT 1", $bind));
    }
    else{
      // atualiza no banco
      $bind = array($nome,$nascimento,$cpf,$celular,$idperfil, $foto, $idusuario);
  		$sql_usuario = $db->query("UPDATE usuario SET nome=?,nascimento=?,cpf=?,celular=?,idperfil=?, foto=? WHERE idusuario = ? ".$where." LIMIT 1", $bind);
      //print $sql_usuario;break;

      // insere no log
      salvaLog($db->mostraquery("UPDATE usuario SET nome=?,nascimento=?,cpf=?,celular=?,idperfil=?, foto=? WHERE idusuario = ? ".$where." LIMIT 1", $bind));
    }

    if($sql_usuario){
      // editado com sucesso
      header("Location: ../admin.php?action=assinantes&status=8");
    }
    else{
      // erro ao editar
      header("Location: ../admin.php?action=assinantes&status=3");
    }
}

} //FIM VERIFICA A PERMISSÃO

?>
