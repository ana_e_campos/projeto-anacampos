<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["equipes"]["editar"] == 1 || $_SESSION["equipe"]["editar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  //VERIFICA A PERMISSÃO
  if($_SESSION["equipe"]["editar"] == 1){
  	$where = " AND idusuario = ".$_SESSION["dados_evolucao"]["idusuario"]."";
  }

  $idusuario   = isset($_POST["idusuario"]) ? numero($_POST["idusuario"]) : "";
  $nome        = isset($_POST["nome"]) ? filtra($_POST["nome"]) : "";
  $nascimento  = isset($_POST["nascimento"]) ? data_en($_POST["nascimento"]) : NULL;
  $celular    = isset($_POST["celular"]) ? filtra($_POST["celular"]) : "";

  $email       = isset($_POST["email"]) ? filtra(strtolower($_POST["email"])) : "";
  $idperfil    = isset($_POST["idperfil"]) ? numero($_POST["idperfil"]) : "";
  $senha       = isset($_POST["nova_senha"]) ? filtraLogin($_POST["nova_senha"]) : "";

	// consulta o email no banco
	$bind = array($email, $idusuario);
	$sql_email = $db->query("SELECT email FROM usuario WHERE email = ? AND idusuario != ?  LIMIT 1", $bind);
  //print $sql_email;break;

	if($db->numRows($sql_email) == 0){

    require('wideimage/WideImage.php');
    $dir = "../media/";
    $fotos = isset($_FILES["foto"]) ? $_FILES["foto"] : false;
    $remover = $_POST["remover"] ? '1' : '0';
    $fotoantiga	= $_POST['fotoantiga'];

    if(!empty($fotos['name'])){

      // renomeia o arquivo
      $foto_nome = $fotos['name'];
    	$foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    	$foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
      $foto = md5($foto_nome).strtolower($foto_extensao);
      //print $foto;break;

        // novo formato de verificação do PHP (TYPE em desuso)
        $path_info = pathinfo($fotos['name']);
        if(!preg_match( '/^(jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos["size"] > 5000000){
          // formato de imagem não suportado
          header("Location: ../admin.php?action=equipes&status=4");
        exit;
        }
        else{
          // renomeia o arquivo, se necessário
          if(file_exists($dir.$foto)){
            $a = 1;
            while(file_exists($dir."[{$a}]{$foto}"))
              $a++;
            $foto = "[{$a}]{$foto}";
          }

          if(!@move_uploaded_file($fotos['tmp_name'], $dir.$foto)){
            $erros[] = $fotos['name'];
            // erro ao enviar a foto
            header("Location: ../admin.php?action=equipes&status=5");
            exit;
          }
          else{
            $img = WideImage::load($dir.$foto);
            $img = $img->resize(900, 900, 'inside');
            //$img = $img->resize('50%');
            //$marca = WideImage::load("imgs/marca.png");
            //$img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto);

            $img = WideImage::load($dir.$foto);
            $img = $img->resize(170, 170, 'inside')->crop('center', 'center', 150, 150);
            $img->saveToFile($dir.'mini_'.$foto);

            // exclui a foto antiga
            //if($fotoantiga != "sem-imagem.png"){
              //unlink($dir.$fotoantiga);
            //}

          }
        }
    }
    // se não selecionou uma foto para o envio
    else{
      if($remover == '1'){
        $foto = "sem-imagem.png";
      }
      else{
        $foto = $fotoantiga;
      }
    }

    // caso seja alterada a senha
    if($senha != "" ){
      // atualiza no banco
      $bind = array($nome,$nascimento,$celular,$email,$idperfil, $senha, $foto, $idusuario);
  		$sql_usuario = $db->query("UPDATE usuario SET nome=?,nascimento=?,cpf=?,celular=?,email=?,idperfil=?, senha=PASSWORD(?), foto=? WHERE idusuario = ? ".$where." LIMIT 1", $bind);
      #print $sql_usuario; break;

      // insere no log
      $bind = array($nome,$nascimento,$celular,$email,$idperfil, $foto, $idusuario);
      salvaLog($db->mostraquery("UPDATE usuario SET nome=?,nascimento=?,celular=?,email=?,idperfil=?, senha='XXXXX', foto=? WHERE idusuario = ? ".$where." LIMIT 1", $bind));
    }
    else{
      // atualiza no banco
      $bind = array($nome,$nascimento,$celular,$email,$idperfil, $foto, $idusuario);
  		$sql_usuario = $db->query("UPDATE usuario SET nome=?,nascimento=?,celular=?,email=?,idperfil=?, foto=? WHERE idusuario = ? ".$where." LIMIT 1", $bind);
      //print $sql_usuario;break;

      // insere no log
      salvaLog($db->mostraquery("UPDATE usuario SET nome=?,nascimento=?,celular=?,email=?,idperfil=?, foto=? WHERE idusuario = ? ".$where." LIMIT 1", $bind));
    }

    if($sql_usuario){
      // editado com sucesso
      header("Location: ../admin.php?action=equipes&status=8");
    }
    else{
      // erro ao editar
      header("Location: ../admin.php?action=equipes&status=3");
    }
  }
  else{
    // erro de email
    header("Location: ../admin.php?action=equipes&status=2");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
