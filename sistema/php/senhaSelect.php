<?php
ob_start();
require_once('connection.php');

if(getenv("REQUEST_METHOD") == "POST"){

  $email  = isset($_POST["email"]) ? filtraLogin($_POST["email"]) : "";

	// consulta o email no banco
	$bind = array($email);
	$sql = $db->query("SELECT * FROM usuario WHERE email = ? AND ativo = 1 LIMIT 1", $bind);
  // insere no log
  salvaLog($db->mostraquery("SELECT * FROM usuario WHERE email = ? AND ativo = 1 LIMIT 1", $bind));

	if($db->numRows($sql)){

		$linha = $db->fetchArray($sql);
    $email = $linha['email'];
    $nome  = $linha['nome'];

    // cria o token
    $token = rand(999,99999);
    $token = md5($token);
    //print $token;break;

    // atualiza o token
    $bind = array($token, $email);
	  $sql = $db->query("UPDATE usuario SET token = ? WHERE email = ? LIMIT 1", $bind);
    // insere no log
    salvaLog($db->mostraquery("UPDATE usuario SET token = ? WHERE email = ? LIMIT 1", $bind));

    // envia uma notificação por e-mail
    require_once('class.phpmailer.php');

    // DADOS DO REMETENTE
    $nome_from1 = utf8_decode("Evolução Assessoria Esportiva");
    $email_from1 = "contato@servicodireto.net";
    $assunto1 = utf8_decode("Recuperação de senha - Evolução Assessoria Esportiva");
    $msg1 = utf8_decode("Oiii, <b>".$nome."</b>. <br /><br />
    A Evolução Assessoria Esportiva informa que recebeu a sua solicitação de redefinição de senha :) <br />
    Para criar a sua nova senha, clique no link abaixo: <br /><br />
    https://www.servicodireto.net/sistema/index.php?email=".$email."&token=".$token." <br /><br />
    Caso você não tenha feito esta solicitação, pedimos desculpas :( Basta ignorar esta mensagem.<br />
    ");

    //print $msg1;break;

    // DADOS DO REMETENTE
    // chamada da classe
    $Email1 = new PHPMailer();
    // opção de idioma
    $Email1->SetLanguage("br");
    // envio feito através da função mail do php
    $Email1->IsMail();

    // DADOS DO REMETENTE
    // ativa o envio de e-mails em HTML, se false, desativa.
    $Email1->IsHTML(true);
    // email do remetente da mensagem
    $Email1->From = $email_from1;
    // nome do remetente do email
    $Email1->FromName = $nome_from1;
    // endereço de destino do e-mail
    $Email1->AddAddress($email);
    // assunto da mensagem
    $Email1->Subject = $assunto1;
    // texto da mensagem (aceita HTML)
    $Email1->Body .= $msg1;
    $Email1->Body .= utf8_decode("<br /> Se precisar, basta nos ligar.. sem problemas!<br /><br />

      Este é um e-mail automático. Não é necessário respondê-lo, ok?!
    ");

    if(!$Email1->send()) {
      echo 'Mensagem de e-mail não enviada :( Entre em contato com a Evolução Assessoria Esportiva, ok?!';
    	//echo 'Erro: ' . $Email1->ErrorInfo;
    	exit;
    }
    else{
      // mensagem enviada com sucesso
      header("Location: ../index.php?status=11");
	  }
  }
  else{
    // email não encontrado
    header("Location: ../index.php?status=12");
  }
}
?>
