<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["equipes"]["editar"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  $idusuario = isset($_GET["idusuario"]) ? numero($_GET["idusuario"]) : 0;

	// reativa o acesso
	$bind = array($idusuario);
	$sql_usuario = $db->query("UPDATE usuario SET tentativas = 0 WHERE idusuario = ? LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE usuario SET tentativas = 0 WHERE idusuario = ? LIMIT 1", $bind));

  if($sql_usuario){
    // inserido com sucesso
    header("Location: ../admin.php?action=equipes&status=8");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=equipes&status=3");
  }

}

} //FIM VERIFICA A PERMISSÃO
?>
