<?php

/**
 * Sistema de cache
 * 
 * @author Thiago Belem <contato@thiagobelem.net>
 * @link https://blog.thiagobelem.net/
 */
class Cache {
	
	private static $time = '5 minutes'; // tempo padrão de cache
	private $folder; // Local onde o cache será salvo

	public function __construct($folder = null) {
		$this->setFolder(!is_null($folder) ? $folder : sys_get_temp_dir());
	}

	protected function setFolder($folder) {
		// Se a pasta existir, for uma pasta e puder ser escrita
		if (file_exists($folder) && is_dir($folder) && is_writable($folder)) {
			$this->folder = $folder;
		} else {
			mkdir('/'.$folder.'/', 0755);
		}
	}

	protected function generateFileLocation($key) {
		return $this->folder . DIRECTORY_SEPARATOR . sha1($key) . '.tmp';
	}

	protected function createCacheFile($key, $content) {
		// Gera o nome do arquivo
		$filename = $this->generateFileLocation($key);
		
		// Cria o arquivo com o conteúdo
		return file_put_contents($filename, $content)
			OR trigger_error('Não foi possível criar o arquivo de cache', E_USER_ERROR);
	}

	public function save($key, $content, $time = null) {
		$time = strtotime(!is_null($time) ? $time : self::$time);
			
		$content = serialize(array(
			'expires' => $time,
			'content' => $content));
		
		return $this->createCacheFile($key, $content);
	}

	public function read($key) {
		$filename = $this->generateFileLocation($key);
		if (file_exists($filename) && is_readable($filename)) {
			$cache = unserialize(file_get_contents($filename));
			if ($cache['expires'] > time()) {
				return $cache['content'];
			} else {
				unlink($filename);
			}
		}
		return null;
	}
}


###########################################################################################################
#DataBase.class.php
#Extensão da Classe PDO.
#Com médotos para auxilio de execução de queries.
#Página Orientada a Objetos
#Desenvolvido pela Head Trust
#Criado em: 09/05/2008
#Modificado em: (26/08/2008) - (13/10/2008) - (19/10/2008) - (20/10/2008) - (30/10/2008)- (14/12/2008)
# - (25/12/2008) - (27/02/2009) - (16/03/2009) - (18/03/2009) - (06/05/2009) - (03/07/2009) - (13/05/2010)
# - (07/02/2011) - (31/10/2012)
###########################################################################################################

class DataBase extends PDO {
	
	private $dbTables; //Atributos de conexão
	private $debug, $debugCode; //Atributos de Debug
	private $showQuery = 0;//Status do ShowQuery
	private $affected = array(), $countedRows = array(), $fetchedResult = array(), $fetchList = array(), $numberResult = 0; //Atributos de resultados de operações
	static $_SHOW_OFF = 0, $_SHOW_ON = 1;

	/*
	 * Métodos get e set dos atributos para acesso externo
	 */
	
	public function getDbServer(){
		return $this->dbServer;
	}
	
	public function setDbServer($server){
		$this->dbServer = $server;	
	}
	
	public function getDbLog(){
		return $this->dbLog;
	}
	
	public function setDbLog($log){
		$this->dbLog = $log;
	}
	
	public function getDbPass(){
		return $this->dbPass;
	}
	
	public function setDbPass($pass){
		$this->dbPass = $pass;
	}

	public function getDbTables(){
		return $this->dbTables;
	}

	public function setDbTables($dbTables){
		$this->dbTables = $dbTables;
	}
	
	public function getDebug(){
		return $this->debug;
	}
	
	public function setDebug($debug){
		$this->debug = $debug;
	}
	
	/*
	 * Fim dos métodos get e set
	 */

	//Construtor da classe DataBase :: DataBase()
	public function __construct($dbServer, $dbLog, $dbPass, $dbTables){
		$this->setDbTables($dbTables);
		try{
			parent::__construct($dbServer, $dbLog, $dbPass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		} catch(Exception $e){}
	}
	
	//Retorna um array com os resultados de uma query
	//Parametros: Resultado do método query()
	//mixed fetchArray ([int])
	public function fetchArray($result = ''){
		$numberResult = ($result == '') ? $this->numberResult : $result;
		$key = $this->fetchList[$numberResult];
		$this->fetchList[$numberResult]++;
		if(array_key_exists($key,$this->fetchedResult[$numberResult])){
			return $this->fetchedResult[$numberResult][$key];
		} else {
			$this->fetchList[$numberResult] = 0;
			return false;
		}
	}

	//Muda o ponteiro da sequencia de retorno dos dados.
	//Parametro: Resultado do método query(), Posição desejada
	//bool dataSeek([int], [int])
	public function dataSeek($result = '', $seek){		
		if (is_array($result)) {
			return array_slice($result, $seek, count($result)-1);
		} else {
			$numberResult = ($result == '') ? $this->numberResult : $result;
			if(($seek >= 0) && ($seek <= ($this->countedRows[$numberResult] - 1))){
				$this->fetchList[$numberResult] = $seek;
				return true;
			} else {
				return false;
			}
		}
	}
	
	//Retorna um array com os resultados de uma query
	//Parametros: Resultado do método query()
	//mixed fetchAll ([int])
	public function fetchAll($result = ''){
        $numberResult = ($result == '') ? $this->numberResult : $result;
        if(is_array($this->fetchedResult[$numberResult])){
            return $this->fetchedResult[$numberResult];
        } else {
            $this->fetchList[$numberResult] = 0;
            return false;

        }
    }
	
	//Retorna a quantidade de linhas afetadas por uma query
	//Parametros: Resultado do método query()
	//mixedaffectedRows ([int])
	public function affectedRows($result = ''){
		$key = ($result == '') ? $this->numberResult : $result;
		if(array_key_exists($key,$this->affected)){
			return $this->affected[$key];
		} else {
			return false;
		}
	}
	
	//Retorna a quantidade de linhas selecionadas por uma query
	//Parametros: Resultado do método query()
	//mixed numRows ([int])
	public function numRows($result=''){		
		if (is_array($result)) {
			return count($result);
		} else {
			$key = ($result == '') ? $this->numberResult : $result;
			if(array_key_exists($key,$this->countedRows)){
				return $this->countedRows[$key];
			} else {
				return false;
			}
		}		
	}
	
	//Retorna um único resultado de uma requisição query
	//Parametros: Resultado do método query(), Posição do array, Posição do array ou chave do array associativo
	//mixed fetchResult (int, int, mixed)
	public function fetchResult($result, $position, $column){
		if(array_key_exists($result,$this->fetchedResult)){
			if(array_key_exists($position,$this->fetchedResult[$result])){
				if(array_key_exists($column,$this->fetchedResult[$result][$position])){
					return $this->fetchedResult[$result][$position][$column];
				}  else {
					return false;
				}
			}  else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public function showQuery($status = 1){
		$this->showQuery = $status;
	}
	
	//Inseri as tabelas nas queries
	//Parametros: (Query)
	//string insertTables ( string $query )
	private function insertTables($query){
		if(preg_match_all('#%\[(.*?)\]%#', $query, $matches)){
			$dbTables = $this->dbTables;
			$count = count($matches[1]);
			
			for($i = 0; $i < $count; $i++){
				$query = str_replace($matches[0][$i], $dbTables[$matches[1][$i]], $query);
			}
		}
		return $query;
	}
	
	//Recebe um array e monta clausulas WHERE, GROUP BY, etc apartir do array.
	//Parametros: (Type [String do tipo de condição], Array [Array multidimensional])
	//string joinCondition ( string $type, array $array)
	public function joinCondition($type, array $array){
		if($total = count($array['condition'])){
			$join = "\n\r" . strtoupper($type);
			for($i = 0; $i < $total; $i++){
				$operator = ($i == 0) ? null : $array['operator'][$i] . chr(32);
				$join .= chr(32) . $operator . $array['condition'][$i];
			}
		} else {
			$join = NULL;
		}
		
		return $join;
	}
	
	//Executa uma query, o parametro bind se for passado irá fazer um bind automático conforme o array
	//Parametros: (Query a ser executada, Array de binds)
	//mixed query (string $query , [ array $bind ] )


	public function query($query, array $bind = array(), $cache = false, $tempo = '5 minutes'){
		
		if ($cache == true) { // faz o cache da consulta
			
			$cacheName = md5($this->mostraquery($query, $bind));
			$cache = new Cache('cache');
			$consulta = $cache->read($cacheName);
			if (!$consulta) {
				$sql = $this->query($query, $bind);
				$consulta = $this->fetchAll($sql);
				$cache->save($cacheName, $consulta, $tempo);
			}
			return $consulta;
		
		}
		
		$query = $this->insertTables($query);
		
		if($this->showQuery == self::$_SHOW_ON){
			echo $query . '<br />';
		}
		
		//Escolhe um número aleatóriamente para o sistema descançar.
		$state = parent::prepare($query);
		
		if($state->execute($bind)){
			$this->analysis($state,$query);
			return $this->numberResult;
		} else {
			$this->debug($state->errorInfo(),$query, $bind);
			return false;
		}
	}

	
	//Analisa a query e verifica quais valores devem ser atribuido a quais atributos.
	//Parametros (Objeto PDO, Query executada)
	//void analysis (Object, String)
	private function analysis(PDOStatement $state, $query){
		$numResult = ++$this->numberResult;
		$words = explode(' ', $query);
		$haystack = array('select', 'show', 'describe', 'explain', 'load', 'flush', 'reset', 'status', 'privileges', 'slave', 'user_resources', 'hosts', 'des_key_files');
		if(!in_array(strtolower($words[0]), $haystack)){
			$this->affected[$numResult] = $state->rowCount();
		} else {
			$results = $state->fetchAll();
			$count = count($results);
			$this->fetchList[$numResult] = 0;
			$this->fetchedResult[$numResult] = $results;
			$this->countedRows[$numResult] = $count;
		}
	}
	
	//Pega um array de Queries e executa todas dentro de uma trasação
	//Parametros (Array de queries a serem executadas, Array de valores para bind)
	//bool transaction (Array, [Array])
	public function transaction(array $queries, array $bind = array()){
		parent::beginTransaction();
		$count = count($queries);
		for($i = 0; $i < $count; $i++){
			$binding = (isset($bind[$i])) ? $bind[$i] : array();
			$result = $this->query($queries[$i], $binding);
			$results[] = ($result != false) ? $this->affectedRows($result) : false;
		}
		
		if(array_search(false, $results, 1) === false){
			parent::commit();
			return true;
		} else {
			parent::rollBack();
			return false;
		}
	}
	
	//Gera uma string contendo todos os erros que foram gerados
	//Parametros: (Texto com o erro gerado, A query executada)
	//void debug (String, String)
	private function debug($errors, $query, $bind){
		$this->debug .= "\n[" . ++$this->debugCode . "] Problemas com a execução da query\n";
		$this->debug .= "Erro gerado (" . end($errors) . ")\n";
		$this->debug .= "Query executada (" . $this->mostraquery($query, $bind) . ")\n";
	}
	
	//Busca no banco o maior ID inserido e retorna um ID apto para ser inserido no banco.
	//Parametros: (Nome ou identificador da tabela, Nome da coluna a ser gerado o ID)
	//int gerateId (String $table, String $column)
	public function gerateId($table, $column){
		$search = $this->query("SELECT MAX({$column}) AS maximum FROM " . $table);
		$max = ($this->numRows($search)) ? ($this->fetchResult($search, 0, 'maximum') + 1) : false;
		return $max;
	}
	
	//Apelido para o método DataBase::lastId()
	//Parametros: (void)
	//int insertId ( void )
	public function insertId(){
		return $this->lastId();
	}
	
	//Resgata o ultimo ID inserido através de um INSERT
	//Parametros: (void)
	//int insertId ( void )
	public function lastId(){
		$pointer = $this->query('SELECT LAST_INSERT_ID() AS lastId');
		$result = ($this->numRows($pointer)) ? $this->fetchResult($pointer, 0, 'lastId') : false;
		return $result;
	}
	
	//Faz um teste de conexão verificando se será executada a query
	//Parametros: void
	//bool testResource (void)
	public function testResource(){
		$state = $this->query("SELECT TRUE FROM %[0]% LIMIT 1");
		if(!$state){
			return false;
		} else {
			return true;
		}
	}
	
	//Limpa todos os grupos atributos de resultados desta classe.
	//Parametros: void
	//void cleanResults (void)
	public function cleanResults(){
		$this->affected = array();
		$this->countedRows = array();
		$this->fetchedResult = array();
		$this->fetchList = array();
		$this->numberResult = null;
	}
	
	//Limpa um único grupo de atributos de resultados desta classe.
	//Parametros (Valor retornado pelo método query)
	//void cleanSingleResult (int)
	public function cleanSingleResult($result){
		if(array_key_exists($result, $this->affected)){
			$this->affected[$result] = array();
		}
		
		if(array_key_exists($result, $this->countedRows)){
			$this->countedRows[$result] = array();
		}
		
		if(array_key_exists($result, $this->$this->fetchedResult)){
			$this->$this->fetchedResult[$result] = array();
		}
		
		if(array_key_exists($result, $this->$this->fetchList)){
			$this->$this->fetchList[$result] = array();
		}
	}
	
	public static function mostraquery($query, $params) {
		$keys = array();
	
		# build a regular expression for each parameter
		foreach ($params as $key => $value) {
			if (is_string($key)) {
				$keys[] = '/:'.$key.'/';
			} else {
				$keys[] = '/[?]/';
			}
		}
		
		for ($i=0;$i<count($params);$i++) {
			$params[$i] = "'".$params[$i]."'";
		}
	
		$query = preg_replace($keys, $params, $query, 1, $count);
	
		#trigger_error('replaced '.$count.' keys');
	
		return $query;
	}

	public function __destruct(){}
}
?>