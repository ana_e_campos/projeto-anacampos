<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["musculos"]["excluir"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  $idmusculo = isset($_GET["idmusculo"]) ? numero($_GET["idmusculo"]) : 0;

	// inativa a musculo
	$bind = array($idmusculo);
	$sql_musculo = $db->query("UPDATE musculo SET ativo = 0 WHERE idmusculo = ? LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE musculo SET ativo = 0 WHERE idmusculo = ? LIMIT 1", $bind));

  if($sql_musculo){
    // inserido com sucesso
    header("Location: ../admin.php?action=musculos&status=6");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=musculos&status=7");
  }

}

} //FIM VERIFICA A PERMISSÃO
?>
