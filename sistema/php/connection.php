<?php
session_start();

// Configurações do php.ini
ini_set('max_execution_time', '300');
ini_set('default_charset', 'UTF-8');
date_default_timezone_set('America/Sao_Paulo');
setlocale(LC_CTYPE, "ptb", "BR", "pt_BR");
ini_set('memory_limit', '2048M');

// Oculta os erros php
//error_reporting(0); ini_set("display_errors", 0 );
//mostra os erros php
error_reporting(E_ALL^ E_NOTICE); ini_set("display_errors", 1);

if (basename($_SERVER["PHP_SELF"]) == "connection.php") {
        header("Location: ../logout.php");
		exit();
}

require_once('DataBase.class.php');

// Alterar também em functions.php
$dbServer = 'mysql:dbname=evolucao_sistema;host=127.0.0.1;'; // Acesso ao servidor
$dbLog = 'evolucao_sistema'; // Nome de usuário para o banco de dados
$dbPass = 'Y$rWt$NZ(zRz'; // Senha para acesso ao banco de dados.
$db = new DataBase($dbServer, $dbLog, $dbPass, '');

// EXEMPLOS DE USO DA CLASSE DE CONEXÃO
//$sql = $db->query('SELECT * FROM tabela'); // utilizar para INSERT, UPDATE, DELETE E SELECT
//$db->numRows($sql); // conta o número de registro de 1 query
//$db->fetchArray($sql); // pegar todos registros de 1 query
//echo nl2br($db->getDebug()); // mostrar erros das querys na página
//$db->lastInsertId(); // pega o último id inserido

require_once('functions.php');

?>
