<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["assinantes"]["cadastrar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $idplano        = isset($_POST["idplano"]) ? numero($_POST["idplano"]) : 0;

  $nome        = isset($_POST["nome"]) ? filtra($_POST["nome"]) : "";
  $nascimento  = isset($_POST["nascimento"]) ? data_en($_POST["nascimento"]) : NULL;
  $cpf         = isset($_POST["cpf"]) ? filtra($_POST["cpf"]) : "";
  $celular     = isset($_POST["celular"]) ? filtra($_POST["celular"]) : "";

  /*
  $cep                  = isset($_POST["cep"]) ? filtra($_POST["cep"]) : "";
  $logradouro           = isset($_POST["logradouro"]) ? filtra($_POST["logradouro"]) : "";
  $numero               = isset($_POST["numero"]) ? filtra($_POST["numero"]) : "";
  $complemento          = isset($_POST["complemento"]) ? filtra($_POST["complemento"]) : "";
  $bairro               = isset($_POST["bairro"]) ? filtra($_POST["bairro"]) : "";
  $cidade               = isset($_POST["cidade"]) ? filtra($_POST["cidade"]) : "";
  $uf                   = isset($_POST["uf"]) ? filtra($_POST["uf"]) : "";
  $pais                 = isset($_POST["pais"]) ? filtra($_POST["pais"]) : "";
  */

  $email       = isset($_POST["email"]) ? filtra(strtolower($_POST["email"])) : "";
  $senha       = isset($_POST["senha"]) ? filtraLogin($_POST["senha"]) : "";

  $sql_perfil   = $db->query("SELECT idperfil FROM perfil WHERE ativo = 1 AND perfil = 'Assinante' LIMIT 1");
  $linha_perfil = $db->fetchArray($sql_perfil);
  $idperfil     = isset($linha_perfil["idperfil"]) ? numero($linha_perfil["idperfil"]) : "";

	// consulta o email no banco
	$bind = array($email);
	$sql_email = $db->query("SELECT email FROM usuario WHERE email = ? LIMIT 1", $bind);

	if($db->numRows($sql_email) == 0){

    require('wideimage/WideImage.php');
    $dir = "../media/";
    $fotos = isset($_FILES["foto"]) ? $_FILES["foto"] : false;
    //$fotoantiga	= $_POST['fotoantiga'];

    if(!empty($fotos['name'])){

      // renomeia o arquivo
      $foto_nome = $fotos['name'];
    	$foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    	$foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
      $foto = md5($foto_nome).strtolower($foto_extensao);
      //print $foto;break;

        // novo formato de verificação do PHP (TYPE em desuso)
        $path_info = pathinfo($fotos['name']);
        if(!preg_match( '/^(jpeg|gif|jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos["size"] > 5000000){
          // formato de imagem não suportado
          header("Location: ../admin.php?action=assinantes&status=4");
        exit;
        }
        else{
          // renomeia o arquivo, se necessário
          if(file_exists($dir.$foto)){
            $a = 1;
            while(file_exists($dir."[{$a}]{$foto}"))
              $a++;
            $foto = "[{$a}]{$foto}";
          }

          if(!@move_uploaded_file($fotos['tmp_name'], $dir.$foto)){

            // erro ao enviar a foto
            header("Location: ../admin.php?action=assinantes&status=5");
            exit;
          }
          else{
            $img = WideImage::load($dir.$foto);
            $img = $img->resize(900, 900, 'inside');
            //$img = $img->resize('50%');
            //$marca = WideImage::load("imgs/marca.png");
            //$img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto);

            $img = WideImage::load($dir.$foto);
            $img = $img->resize(170, 170, 'inside')->crop('center', 'center', 150, 150);
            $img->saveToFile($dir.'mini_'.$foto);
          }
        }
    }
    // se não selecionou uma foto para o envio
    else{
      $foto = "sem-imagem.png";
    }

    // insere no banco
    $bind = array($nome,$nascimento,$cpf,$celular,$email,$idperfil, $senha, $foto);
		$sql_assinante = $db->query("INSERT INTO usuario (tipo,nome,nascimento,cpf,celular,email,idperfil, senha, foto) VALUES ('Assinante', ?, ?, ?, ?, ?, ?, PASSWORD(?), ?)", $bind);
    //echo nl2br($db->getDebug());
    //print $sql_assinante; break;

    // insere no log
    salvaLog($db->mostraquery("INSERT INTO usuario (tipo,nome,nascimento,cpf,celular,email,idperfil, senha, foto) VALUES ('Assinante', ?, ?, ?, ?, ?, ?, 'XXXXX', ?)", $bind));


    $idusuario = $db->lastInsertId();

    // GERAÇÃO DA FATURA
    // vencimento da primeira fatura é sempre 5 dias após o cadastro
    //$vencimento = isset($_POST["vencimento"]) ? data_en($_POST["vencimento"]) : null;
    $vencimento = date('Y-m-d', strtotime(date('Y-m-d'). ' + 5 days'));

      // insere no banco
      $bind = array($idusuario, $idplano, $_SESSION["dados_evolucao"]["idusuario"], $vencimento);
  		$sql_fatura = $db->query("INSERT INTO fatura (idassinante, idplano, idusuario, vencimento, status) VALUES (?,?,?,?, 'Aberta')", $bind);
      //echo nl2br($db->getDebug());
      //print $sql_fatura; break;

      // insere no log
      salvaLog($db->mostraquery("INSERT INTO fatura (idassinante, idplano, idusuario, vencimento, status) VALUES (?,?,?,?, 'Aberta')", $bind));

      $idfatura = $db->lastInsertId();

    // PEGA OS DADOS DO ASSINANTE
    $bind = array($idusuario);
    $sql_assinante = $db->query("SELECT * FROM usuario WHERE ativo = 1 AND tipo = 'Assinante' AND idusuario = ?", $bind);
    #print $sql_assinante;break;
    $linha_assinante = $db->fetchArray($sql_assinante);

    // PEGA OS DADOS DO PLANO
    $bind = array($idplano);
    $sql_plano = $db->query("SELECT * FROM plano WHERE ativo = 1 AND idplano = ?", $bind);
    #print $sql_plano;break;
    $linha_plano = $db->fetchArray($sql_plano);


    if($sql_assinante){

      // E-MAIL DE CONFIRMAÇÃO
      require_once('class.phpmailer.php');

      // DADOS DO REMETENTE
      $nome_from1 = utf8_decode("Servico Direto.net");
      $email_from1 = "contato@servicodireto.net"; //
      $assunto1 = utf8_decode("Boas-vindas! Evolução Assessoria Esportiva");
      $msg1 = utf8_decode("Oiii, ".$nome.". <br /><br />
       A Evolução Assessoria Esportiva informa que a sua assinatura foi realizada com sucesso :)
       <br/>
       Através da nossa plataforma é possível visualizar e emitir as suas faturas bem como solicitar os serviços desejados.
       <br /><br />

       Plano: ".$linha_plano["plano"]."<br />
       Valor: R$ ".valor($linha_plano["valor"])."<br /> <br />

       Login: ".$email."<br />
       Link de acesso:  https://academiaevolucaoassessoria.com.br/sistema <br/><br/>

       Se precisar, basta nos ligar.. sem problemas!<br /><br />

      Este é um e-mail automático. Não é necessário respondê-lo, ok?!");

      // DADOS DO DESTINATÁRIO
      $nome_from2 = utf8_decode("Evolução Assessoria Esportiva");
      $email_from2 = "contato@servicodireto.net";
      $assunto2 = utf8_decode("Novo assinante - Evolução Assessoria Esportiva");
      $msg2 = utf8_decode("

      Uhull! Novo assinante na plataforma. <br /><br />

      Nome: ".$nome."<br />
      E-mail: ".$email."<br />
      Telefone: ".$celular."<br />
      Plano: ".$linha_plano["plano"]."<br />
      Valor: R$ ".valor($linha_plano["valor"])."<br /><br />

      Este é um e-mail automático.
      ");

      // DADOS DO REMETENTE
      // chamada da classe
      $Email1 = new PHPMailer();
      // opção de idioma
      $Email1->SetLanguage("br");
      // envio feito através da função mail do php
      $Email1->IsMail();

      // ativa o envio de e-mails em HTML, se false, desativa.
      $Email1->IsHTML(true);
      // email do remetente da mensagem
      $Email1->From = $email_from1;
      // nome do remetente do email
      $Email1->FromName = $nome_from1;
      // endereço de destino do e-mail
      $Email1->AddAddress($email);
      // assunto da mensagem
      $Email1->Subject = $assunto1;
      // texto da mensagem (aceita HTML)
      $Email1->Body .= $msg1;
      $Email1->Body .= utf8_decode("
      <br /><br />
      Evolução Assessoria Esportiva <br />
      Uma plataforma PSJP - CNPJ: 22.445.158/0001-08 <br />
      Avenida São Judas, 595. Bagé/RS <br />
      contato@servicodireto.net | (53) 9 9128-5844 <br />
      https://academiaevolucaoassessoria.com.br <br />
       <br /><br />
      ");

      // DADOS DO DESTINATÁRIO
      // chamada da classe
      $Email2 = new PHPMailer();
      // opção de idioma
      $Email2->SetLanguage("br");
      // envio feito através da função mail do php
      $Email2->IsMail();

      // ativa o envio de e-mails em HTML, se false, desativa.
      $Email2->IsHTML(true);
      // email do remetente da mensagem
      $Email2->From = $email_from2;
      // nome do remetente do email
      $Email2->FromName = $nome_from2;
      // endereço de destino do e-mail
      $Email2->AddAddress("contato@servicodireto.net");
      // assunto da mensagem
      $Email2->Subject = $assunto2;
      // texto da mensagem (aceita HTML)
      $Email2->Body .= $msg2;
      $Email2->Body .= utf8_decode("
      <br /><br />
      Evolução Assessoria Esportiva <br />
      Uma plataforma PSJP - CNPJ: 22.445.158/0001-08 <br />
      Avenida São Judas, 595. Bagé/RS <br />
      contato@servicodireto.net | (53) 9 9128-5844 <br />
      https://academiaevolucaoassessoria.com.br <br />
       <br /><br />");

      // envia as mensagens
      $Email1->send();
      $Email2->send();


      // inserido com sucesso
      header('Location: ../admin.php?action=assinantes&id_plataforma='.$idfatura.'&produto_codigo_1='.$idfatura.'&produto_valor_1='.$linha_plano["valor"].'&produto_descricao_1='.$linha_plano["plano"].' - R$ '.$linha_plano["valor"].'&email='.$linha_assinante["email"].'&nome='.$linha_assinante["nome"].'&cpf='.$linha_assinante["cpf"].'&celular='.$linha_assinante["celular"].'&status=1');

    }
    else{
      // erro ao inserir
      header("Location: ../admin.php?action=assinantes&status=3");
    }
  }
  else{
    // erro de email
    header("Location: ../admin.php?action=assinantes&status=2");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
