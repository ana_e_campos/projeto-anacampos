<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["assinantes"]["excluir"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  //VERIFICA A PERMISSÃO
  if($_SESSION["assinante"]["excluir"] == 1){
    $where = " AND idusuario = ".$_SESSION["dados_evolucao"]["idusuario"]."";
  }

  $idusuario = isset($_GET["idusuario"]) ? numero($_GET["idusuario"]) : 0;

	// inativa o usuario
	$bind = array($idusuario);
	$sql_usuario = $db->query("UPDATE usuario SET ativo = 0 WHERE idusuario = ? ".$where." LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE usuario SET ativo = 0 WHERE idusuario = ? ".$where." LIMIT 1", $bind));

  if($sql_usuario){

    // E-MAIL DE NOTIFICAÇÃO

    // inserido com sucesso
    header("Location: ../admin.php?action=assinantes&status=6");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=assinantes&status=7");
  }

}

} //FIM VERIFICA A PERMISSÃO
?>
