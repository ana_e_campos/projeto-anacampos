<?php
ob_start();
require_once('connection.php');

if(getenv("REQUEST_METHOD") == "POST"){

  $email  = isset($_POST["email"]) ? filtraLogin($_POST["email"]) : "";
  $token  = isset($_POST["token"]) ? filtra($_POST["token"]) : "";
  $senha = isset($_POST["newpassword"]) ? filtraLogin($_POST["newpassword"]) : "";

	//pega o ip
	$ip=getenv("REMOTE_ADDR");

	// consulta o email e o token no banco
	$bind = array($email, $token);
	$sql = $db->query("SELECT email, token FROM usuario WHERE email = ? AND token = ? AND ativo = 1 LIMIT 1", $bind);
  // insere no log
  salvaLog($db->mostraquery("SELECT email, token FROM usuario WHERE email = ? AND token = ? AND ativo = 1 LIMIT 1", $bind));

	if($db->numRows($sql)){

    // atualiza a senha
    $bind = array($senha, $email, $token);
		$sql = $db->query("UPDATE usuario SET token = '', senha = PASSWORD(?) WHERE email = ? AND token = ? LIMIT 1", $bind);
    // insere no log
    salvaLog($db->mostraquery("UPDATE usuario SET token = '', senha = 'XXXXXX' WHERE email = ? AND token = ? LIMIT 1", $bind));

    // recuperação realizada com sucesso
    header("Location: ../index.php?status=13");
  }
  else{
    // email ou token não encontrado
    header("Location: ../index.php?status=14");
  }
}
?>
