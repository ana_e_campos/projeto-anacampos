<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["perfis"]["editar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $idperfil      = isset($_POST["idperfil"]) ? numero($_POST["idperfil"]) : "";
  $perfil        = isset($_POST["perfil"]) ? filtra($_POST["perfil"]) : "";

	// consulta o nome no banco
	$bind = array($perfil, $id);
	$sql_nome = $db->query("SELECT perfil FROM perfil WHERE perfil = ? AND idperfil != ? LIMIT 1", $bind);

	if($db->numRows($sql_nome) == 0){

    // atualiza no banco o perfil
    $bind = array($perfil, $idperfil);
		$sql_perfil = $db->query("UPDATE perfil SET perfil = ?  WHERE idperfil = ? LIMIT 1", $bind);
    #print $sql_perfil; break;

    // insere no log o perfil
    salvaLog($db->mostraquery("UPDATE perfil SET perfil = ?  WHERE idperfil = ? LIMIT 1", $bind));


    // percorre as páginas listadas
    for($i=0; $i < count(array_filter($_POST['idpagina'])); $i++){

      $idpagina = isset($_POST["idpagina"][$i]) ? numero($_POST["idpagina"][$i]) : NULL;
      $ver = $_POST["ver"][$i] ? '1' : '0';
      $cadastrar = $_POST["cadastrar"][$i] ? '1' : '0';
      $editar = $_POST["editar"][$i] ? '1' : '0';
      $excluir = $_POST["excluir"][$i] ? '1' : '0';

      //verifica se foi adicionada uma nova permissão
      $bind = array($idperfil, $idpagina);
      $sql_novapermissao = $db->query("SELECT idpermissao FROM permissao WHERE idperfil = ? AND idpagina = ? AND ativo = 1 LIMIT 1", $bind);
      //print $sql_novapermissao;break;

      if($db->numRows($sql_novapermissao) == 0){

        //insere a nova permissão
        $bind = array($ver, $cadastrar, $editar, $excluir, $idperfil, $idpagina);
        $sql_permissao = $db->query("INSERT INTO permissao (ver, cadastrar, editar, excluir, idperfil, idpagina) VALUES (?, ?, ?, ?, ?, ?)", $bind);
        //print $sql_permissao; break;

        // insere no log as permissões
        salvaLog($db->mostraquery("INSERT INTO permissao (ver, cadastrar, editar, excluir, idperfil, idpagina) VALUES (?, ?, ?, ?, ?, ?)", $bind));

      }else{

        //edita a permissão
        $bind = array($ver, $cadastrar, $editar, $excluir, $idperfil, $idpagina);
        $sql_permissao = $db->query("UPDATE permissao SET ver = ?, cadastrar = ?, editar = ?, excluir = ? WHERE idperfil = ? AND idpagina = ? AND ativo = 1 LIMIT 1", $bind);
        //print $sql_permissao; break;

        // insere no log as permissões
        salvaLog($db->mostraquery("UPDATE permissao SET ver = ?, cadastrar = ?, editar = ?, excluir = ? WHERE idperfil = ? AND idpagina = ? AND ativo = 1 LIMIT 1", $bind));

      }

    }

    if($sql_perfil){
      // editado com sucesso
      header("Location: ../admin.php?action=perfis&status=8");
    }
    else{
      // erro ao editar
      header("Location: ../admin.php?action=perfis&status=3");
    }
  }
  else{
    // erro de nome
    header("Location: ../admin.php?action=perfis&status=2");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
