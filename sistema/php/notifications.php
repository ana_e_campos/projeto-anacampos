
    <li class="dropdown hidden-phone">
      <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="icon-bell"></i>
        <span class="badge red">
        7 </span>
      </a>
      <ul class="dropdown-menu notifications">
        <li class="dropdown-menu-title">
          <span>Você tem 11 notificações</span>
          <a href="#refresh"><i class="icon-repeat"></i></a>
        </li>
                      <li>
                            <a href="#">
            <span class="icon blue"><i class="icon-user"></i></span>
            <span class="message">Novo usuário registrado</span>
            <span class="time">1 min</span>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="icon green"><i class="icon-comment-alt"></i></span>
            <span class="message">Novo comentário</span>
            <span class="time">7 min</span>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="icon green"><i class="icon-comment-alt"></i></span>
            <span class="message">New comment</span>
            <span class="time">8 min</span>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="icon green"><i class="icon-comment-alt"></i></span>
            <span class="message">Novo comentário</span>
            <span class="time">16 min</span>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="icon blue"><i class="icon-user"></i></span>
            <span class="message">Novo usuário registrado</span>
            <span class="time">36 min</span>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="icon yellow"><i class="icon-shopping-cart"></i></span>
            <span class="message">2 itens vendidos</span>
            <span class="time">1 hora</span>
                            </a>
                        </li>
        <li class="warning">
                            <a href="#">
            <span class="icon red"><i class="icon-user"></i></span>
            <span class="message">Conta de usuário excluída</span>
            <span class="time">2 horas</span>
                            </a>
                        </li>
        <li class="warning">
                            <a href="#">
            <span class="icon red"><i class="icon-shopping-cart"></i></span>
            <span class="message">Novo comentário</span>
            <span class="time">6 horas</span>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="icon green"><i class="icon-comment-alt"></i></span>
            <span class="message">Novo comentário</span>
            <span class="time">ontem</span>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="icon blue"><i class="icon-user"></i></span>
            <span class="message">Novo usuário registrado</span>
            <span class="time">ontem</span>
                            </a>
                        </li>
                        <li class="dropdown-menu-sub-footer">
                        <a>Ver todas as notificações</a>
        </li>
      </ul>
    </li>
    <!-- start: Notifications Dropdown -->
    <li class="dropdown hidden-phone">
      <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="icon-calendar"></i>
        <span class="badge red">
        5 </span>
      </a>
      <ul class="dropdown-menu tasks">
        <li class="dropdown-menu-title">
          <span>Você tem 17 tarefas em progresso</span>
          <a href="#refresh"><i class="icon-repeat"></i></a>
        </li>
        <li>
                            <a href="#">
            <span class="header">
              <span class="title">Desenvolvimento iOS</span>
              <span class="percent"></span>
            </span>
                                <div class="taskProgress progressSlim red">80</div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
            <span class="header">
              <span class="title">Desenvolvimento Android</span>
              <span class="percent"></span>
            </span>
                                <div class="taskProgress progressSlim green">47</div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
            <span class="header">
              <span class="title">Desenvolvimento ARM</span>
              <span class="percent"></span>
            </span>
                                <div class="taskProgress progressSlim yellow">32</div>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="header">
              <span class="title">Development ARM</span>
              <span class="percent"></span>
            </span>
                                <div class="taskProgress progressSlim greenLight">63</div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
            <span class="header">
              <span class="title">Development ARM</span>
              <span class="percent"></span>
            </span>
                                <div class="taskProgress progressSlim orange">80</div>
                            </a>
                        </li>
        <li>
                        <a class="dropdown-menu-sub-footer">Ver todas as tarefas</a>
        </li>
      </ul>
    </li>
    <!-- end: Notifications Dropdown -->
    <!-- start: Message Dropdown -->
    <li class="dropdown hidden-phone">
      <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="icon-envelope"></i>
        <span class="badge red">
        4 </span>
      </a>
      <ul class="dropdown-menu messages">
        <li class="dropdown-menu-title">
          <span>Você tem 9 mensagens</span>
          <a href="#refresh"><i class="icon-repeat"></i></a>
        </li>
                      <li>
                            <a href="#">
            <span class="avatar"><img src="img/avatar.jpg" alt="Avatar"></span>
            <span class="header">
              <span class="from">
                  Denniz
                 </span>
              <span class="time">
                  6 min
                </span>
            </span>
                                <span class="message">
                                    Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
            <span class="avatar"><img src="img/avatar.jpg" alt="Avatar"></span>
            <span class="header">
              <span class="from">
                  Dennis Ji
                 </span>
              <span class="time">
                  56 min
                </span>
            </span>
                                <span class="message">
                                    Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
            <span class="avatar"><img src="img/avatar.jpg" alt="Avatar"></span>
            <span class="header">
              <span class="from">
                  Dennis Ji
                 </span>
              <span class="time">
                  3 horas
                </span>
            </span>
                                <span class="message">
                                    Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                </span>
                            </a>
                        </li>
        <li>
                            <a href="#">
            <span class="avatar"><img src="img/avatar.jpg" alt="Avatar"></span>
            <span class="header">
              <span class="from">
                  Dennis Ji
                 </span>
              <span class="time">
                  yesterday
                </span>
            </span>
                                <span class="message">
                                    Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
            <span class="avatar"><img src="img/avatar.jpg" alt="Avatar"></span>
            <span class="header">
              <span class="from">
                  Dennis Ji
                 </span>
              <span class="time">
                  Jul 25, 2012
                </span>
            </span>
                                <span class="message">
                                    Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                </span>
                            </a>
                        </li>
        <li>
                        <a class="dropdown-menu-sub-footer">Ver todas as mensagens</a>
        </li>
      </ul>
    </li>
