<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["equipamentos"]["cadastrar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $descricao        = isset($_POST["descricao"]) ? filtra($_POST["descricao"]) : "";
  $etiqueta         = isset($_POST["etiqueta"]) ? filtra($_POST["etiqueta"]) : "";
  $idunidade        = isset($_POST["idunidade"]) ? numero($_POST["idunidade"]) : "";

  require('wideimage/WideImage.php');
  $dir = "../media/";
  $fotos = isset($_FILES["foto"]) ? $_FILES["foto"] : false;
  $marca = $_POST["marca"] ? '0' : '0'; // nunca incluir a marca
  //$fotoantiga	= $_POST['fotoantiga'];

  if(!empty($fotos['name'])){

    // renomeia o arquivo
    $foto_nome = $fotos['name'];
    $foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    $foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
    $foto = md5($foto_nome).strtolower($foto_extensao);
    //print $foto;break;

      // novo formato de verificação do PHP (TYPE em desuso)
      $path_info = pathinfo($fotos['name']);
      if(!preg_match( '/^(jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos["size"] > 5000000){
        // formato de imagem não suportado
        header("Location: ../admin.php?action=equipamentos&status=4");
      exit;
      }
      else{
        // renomeia o arquivo, se necessário
        if(file_exists($dir.$foto)){
          $a = 1;
          while(file_exists($dir."[{$a}]{$foto}"))
            $a++;
          $foto = "[{$a}]{$foto}";
        }

        if(!@move_uploaded_file($fotos['tmp_name'], $dir.$foto)){

          // erro ao enviar a foto
          header("Location: ../admin.php?action=equipamentos&status=5");
          exit;
        }
        else{
          $img = WideImage::load($dir.$foto);
          $img = $img->resize(900, 900, 'inside');
          //$img = $img->resize('50%');

          if($marca == '1'){
            // insere a marca d'agua
            $marca = WideImage::load("../img/marca.png");
            $img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto);
          }

          $img = WideImage::load($dir.$foto);
          $img = $img->resize(170, 170, 'inside')->crop('center', 'center', 150, 150);
          $img->saveToFile($dir.'mini_'.$foto);
        }
      }
  }
  // se não selecionou uma foto para o envio
  else{
    $foto = "sem-imagem.png";
  }

  // insere no banco o equipamento
  $bind = array($descricao,$etiqueta,$foto,$idunidade);
	$sql_equipamento = $db->query("INSERT INTO equipamento (descricao,etiqueta,foto,idunidade) VALUES (?,?,?,?)", $bind);
  //print $sql_equipamento; break;

  // insere no log o equipamento
  salvaLog($db->mostraquery("INSERT INTO equipamento (descricao,etiqueta,foto,idunidade) VALUES (?,?,?,?)", $bind));

  if($sql_equipamento){
    // inserido com sucesso
    header("Location: ../admin.php?action=equipamentos&status=1");
  }
  else{
    // erro ao inserir
    header("Location: ../admin.php?action=equipamentos&status=3");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
