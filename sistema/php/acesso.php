<?php
ob_start();
require_once('connection.php');

if(getenv("REQUEST_METHOD") == "POST"){
  $email  = isset($_POST["username"]) ? filtraLogin($_POST["username"]) : "";
  $senha = isset($_POST["password"]) ? filtraLogin($_POST["password"]) : "";
  $lembrar = ($_POST["remember"]) ? '1' : '0';

	//pega o ip
	$ip=getenv("REMOTE_ADDR");

	// consulta o usuário no banco
	$bind = array($email, $senha);
	$sql = $db->query("SELECT u.*, p.perfil
                    FROM usuario u
                    LEFT JOIN perfil p
                    ON u.idperfil = p.idperfil
                    WHERE u.email = ? AND u.senha = PASSWORD(?) AND u.ativo = 1 AND u.tentativas < 11
                    GROUP BY u.idusuario
                    LIMIT 1", $bind);
  // insere no log
  salvaLog($db->mostraquery("SELECT u.*, p.perfil
                            FROM usuario u
                            LEFT JOIN perfil p
                            ON u.idperfil = p.idperfil
                            WHERE u.email = ? AND u.senha = 'XXXXXXXX' AND u.ativo = 1 AND u.tentativas < 11
                            GROUP BY u.idusuario
                            LIMIT 1", $bind));

	if($db->numRows($sql)){

		$linha = $db->fetchArray($sql);

		//cria um array com os valores da session do usuário
		$dados                	= array();
    $dados["idusuario"] 	  = $linha['idusuario'];
    $dados["idperfil"]  		= $linha['idperfil'];

		$dados["nome"]          = $linha['nome'];
		$dados["email"]   	 	  = $linha['email'];
		$dados["foto"]   	 	    = $linha['foto'];
    $dados["perfil"]  		  = $linha['perfil'];
		$dados["ip"]    	 	    = getenv("REMOTE_ADDR");
    $_SESSION["dados_evolucao"] = $dados;

    //print $_SESSION["dados_evolucao"]["nome"];break;

    // consulta as permissões do perfil do usuário
    $bind = array($linha['idperfil']);
    $sql_perfil = $db->query("SELECT pa.pagina, p.ver, p.cadastrar, p.editar, p.excluir
                              FROM permissao p
                              LEFT JOIN pagina pa
                              ON p.idpagina = pa.idpagina
                              WHERE p.idperfil = ? AND p.ativo = 1
                              GROUP BY p.idpermissao", $bind);
    //print $sql_perfil;break;

    while($linha_perfil = $db->fetchArray($sql_perfil)){

      //cria sessions com os valores dos arrays do perfil
      $perfil                	= array();
      $perfil['pagina'] = $linha_perfil['pagina'];
      $perfil['ver'] = $linha_perfil['ver'];
      $perfil['cadastrar'] = $linha_perfil['cadastrar'];
      $perfil['editar'] = $linha_perfil['editar'];
      $perfil['excluir'] = $linha_perfil['excluir'];
      $_SESSION[$linha_perfil['pagina']] = $perfil;

      //exemplo de saída: Array ( [pagina] => inicio [ver] => 1 [cadastrar] => 1 [editar] => 1 [excluir] => 0 )
      /*
      print '<br />';
      print_r($_SESSION[$linha_perfil['pagina']]);
      print '<br />';
      //exemplo de saída: 0
      print $_SESSION["inicio"]["excluir"].'<br /><br />';
      break;
      */

    } // fim while perfil

    // zera as tentativas
    $bind = array($linha['email']);
		$sql = $db->query("UPDATE usuario SET tentativas = 0 WHERE email = ? LIMIT 1", $bind);

    // insere no log
    salvaLog($db->mostraquery("UPDATE usuario SET tentativas = 0 WHERE email = ? LIMIT 1", $bind));

    // se o usuário selecionou o Lembrar-me
    if($lembrar == '1'){
      // cria os tokens
      setcookie("username",encryptCookie($email),time()+60*60*24*30,"/");
      setcookie("password",encryptCookie($senha),time()+60*60*24*30,"/");
      setcookie("remember","checked",time()+60*60*24*30,"/");
    }else{
      // expira os tokens
      setcookie("username","",time()-3600,"/");
      setcookie("password","",time()-3600,"/");
      setcookie("remember","",time()-3600,"/");
    }

    // redireciona para a página logada
    header("Location: ../admin.php?action=inicio");
  }
  else{

		// se o acesso for inválido, incrementa as tentativas com a variável vinda do input
		$bind = array($email);
		$sql = $db->query("UPDATE usuario SET tentativas = tentativas + 1 WHERE email = ? LIMIT 1", $bind);

    // pega as tentativas do email, se forem maior que 10, mostra 'Senha bloqueada'
		$bind = array($email);
		$sql = $db->query("SELECT tentativas FROM usuario WHERE email = ? LIMIT 1", $bind);
		$linha = $db->fetchArray($sql);
		$tentativas = $linha['tentativas'];
		if($tentativas < 11){
		  header("Location: ../index.php?status=9"); // email ou senha errados
		  exit;
		}
		else{
		  header("Location: ../index.php?status=10"); // senha bloqueada
		  exit;
		}
  }
}
?>
