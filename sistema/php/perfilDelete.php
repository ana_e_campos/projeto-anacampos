<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["perfis"]["excluir"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  $idperfil    = isset($_GET["idperfil"]) ? numero($_GET["idperfil"]) : 0;

	// inativa o perfil
	$bind = array($idperfil);
	$sql_perfil = $db->query("UPDATE perfil SET ativo = 0 WHERE idperfil = ? LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE perfil SET ativo = 0 WHERE idperfil = ? LIMIT 1", $bind));

  // inativa as permissoes
	$bind = array($idperfil);
	$sql_permissao = $db->query("UPDATE permissao SET ativo = 0 WHERE idperfil = ?", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE permissao SET ativo = 0 WHERE idperfil = ?", $bind));

  if($sql_perfil){
    // inserido com sucesso
    header("Location: ../admin.php?action=perfis&status=6");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=perfis&status=7");
  }

}

} //FIM VERIFICA A PERMISSÃO

?>
