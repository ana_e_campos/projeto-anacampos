<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["unidades"]["excluir"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  $idunidade = isset($_GET["idunidade"]) ? numero($_GET["idunidade"]) : 0;

	// inativa a unidade
	$bind = array($idunidade);
	$sql_unidade = $db->query("UPDATE unidade SET ativo = 0 WHERE idunidade = ? LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE unidade SET ativo = 0 WHERE idunidade = ? LIMIT 1", $bind));

  if($sql_unidade){
    // inserido com sucesso
    header("Location: ../admin.php?action=unidades&status=6");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=unidades&status=7");
  }

}

} //FIM VERIFICA A PERMISSÃO
?>
