<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["exercicios"]["excluir"] == 1){

if(getenv("REQUEST_METHOD") == "GET"){

  $idexercicio = isset($_GET["idexercicio"]) ? numero($_GET["idexercicio"]) : 0;

	// inativa a exercicio
	$bind = array($idexercicio);
	$sql_exercicio = $db->query("UPDATE exercicio SET ativo = 0 WHERE idexercicio = ? LIMIT 1", $bind);

  // insere no log
  salvaLog($db->mostraquery("UPDATE exercicio SET ativo = 0 WHERE idexercicio = ? LIMIT 1", $bind));

  if($sql_exercicio){
    // inserido com sucesso
    header("Location: ../admin.php?action=exercicios&status=6");
  }
  else{
    // erro ao excluir
    header("Location: ../admin.php?action=exercicios&status=7");
  }

}

} //FIM VERIFICA A PERMISSÃO
?>
