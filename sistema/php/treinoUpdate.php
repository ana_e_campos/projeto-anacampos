<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["treinos"]["editar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $idtreino     = isset($_POST["idtreino"]) ? numero($_POST["idtreino"]) : "";
  $titulo          = isset($_POST["titulo"]) ? filtra($_POST["titulo"]) : "";
  $observacoes   = isset($_POST["observacoes"]) ? filtra($_POST["observacoes"]) : "";

  // atualiza no banco
  $bind = array($titulo,$observacoes,$_SESSION["dados_evolucao"]["idusuario"], $idtreino);
	$sql_treino = $db->query("UPDATE treino SET titulo=?, observacoes=?, idusuario=? WHERE idtreino = ? AND ativo = 1 LIMIT 1", $bind);
  //print $sql_treino; break;
  //echo nl2br($db->getDebug());break;

  // insere no log
  salvaLog($db->mostraquery("UPDATE treino SET titulo=?, observacoes=?, idusuario=? WHERE idtreino = ? AND ativo = 1 LIMIT 1", $bind));

  if($sql_treino){
    // editado com sucesso
    header("Location: ../admin.php?action=treinos&status=8");
  }
  else{
    // erro ao editar
    header("Location: ../admin.php?action=treinos&status=3");
  }

}

} //FIM VERIFICA A PERMISSÃO

?>
