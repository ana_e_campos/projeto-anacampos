<?php

require_once('connection.php');

function numero($var){
		//verifica se é número
        if(!is_numeric($var)){	// se for diferente de numérico coloca 0
		$var = 0;
		}
	   return $var;
}

function cpf($var){
		//verifica se é número
        if(!is_numeric($var)){	// se for diferente de numérico coloca 0
		$var = 0;
		}
    //completa com os zeros
	   return str_pad($var, 11, "0", STR_PAD_LEFT);
}

function soNumero($str) {
    return preg_replace("/[^0-9]/", "", $str);
}

function filtra($var){
		$var = strip_tags($var); //remove as tags html
		$var = addslashes($var); //adiciona as uma \' na consulta ao banco.
    return $var;
}
function filtraLogin($var){
		$var = strip_tags($var); //remove as tags html
		$var = addslashes($var); //adiciona as uma \' na consulta ao banco.
    return $var;
}

function filtraArquivo($var){
		$a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕüªº°';
		$b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRru...';
		$var = strtr($var, utf8_decode($a), $b); //substitui letras acentuadas por "normais"
		$var = strtolower($var); //manda pra minúsculo
		//$var = addslashes($var); //adiciona as uma \' na consulta ao banco. Isto é apenas para colocar os dados no banco de dados, a \ não será inserida
        return $var;
}

function data_en($data) {
	if($data != null)
	return implode("-",array_reverse(explode("/",$data)));
}
function data_br($data) {
	if($data != null && $data != '0000-00-00')
	return date("d/m/Y",strtotime($data));
  else{
    return "";
  }
}
function datacurta_br($data) {
	if($data != null && $data != '0000-00-00')
	return date("d/m",strtotime($data));
  else{
    return "";
  }
}
function datahora_br($data) {
	if($data != null)
	return date("d/m/Y - H:i:s",strtotime($data));
}
function hora($hora) {
	if($hora != null)
	return date("H:i",strtotime($hora));
}
function dia_br($data) {
	if($data != null){
    $dias = ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'];
    $dia = date('w',strtotime($data));
    return ($dias[$dia]);
  }
}
// essa função considera o dia e mês de nascimento, mas o ano atual, devido ao dia da semana exibido
function dianiver_br($data) {
	if($data != null){
    $dias = ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'];
    $data_mes = date('m',strtotime($data));
    $data_dia = date('d',strtotime($data));
    $data_ano = date('Y');
    $data = $data_ano."-".$data_mes."-".$data_dia;
    //print $data;break;
    $dia = date('w',strtotime($data));
    return ($dias[$dia]);
  }
}
function diacurto_br($data) {
	if($data != null){
    $dias = ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'];
    $dia = date('w',strtotime($data));
    return ($dias[$dia]);
  }
}
function valor_save($var) {
	return preg_replace('#\D#','',$var)/100;
}
function valor($var) {
  if($var != null)
	return number_format(($var),2,",",".");
  else
  return null;
}

function print_db($var){
    $var = htmlspecialchars(stripslashes($var)); //remove as '\'
    print($var);
}

// Encrypt cookie
function encryptCookie($value){
 $key = 'Jz7@fs#XzGaJ';
 $newvalue = openssl_encrypt($value,"AES-128-ECB",$key);
 return($newvalue);
}

// Decrypt cookie
function decryptCookie($value){
 $key = 'Jz7@fs#XzGaJ';
 $newvalue = openssl_decrypt($value,"AES-128-ECB",$key);
 return($newvalue);
}

function salvaLog($mensagem) {

  // Conecta ao banco
  $dbServer_log = 'mysql:dbname=evolucao_sistema;host=127.0.0.1;'; // Acesso ao servidor
  $dbLog_log = 'evolucao_sistema'; // Nome de usuário para o banco de dados
  $dbPass_log = 'Y$rWt$NZ(zRz'; // Senha para acesso ao banco de dados.
  $db_log = new DataBase($dbServer_log, $dbLog_log, $dbPass_log, '');

  $mensagem = filtra($mensagem);
  $ip = $_SERVER['REMOTE_ADDR'];
  $idusuario = isset($_SESSION["dados_evolucao"]["idusuario"]) ? numero($_SESSION["dados_evolucao"]["idusuario"]) : NULL;
  $bind_log = array($mensagem, $ip, $idusuario);
  $sql_log = $db_log->query("INSERT INTO log (mensagem, ip, idusuario, datahora) VALUES (?, ?, ?, NOW())", $bind_log);
  //print $sql_log;

  if ($sql_log) {
    return "true";
  }
  else{
    return false;
  }
}

#Início script de aviso de invasão
/*
//pega a url
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
 $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
 $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }

//pega o sql injection
if (preg_match("/from|select|insert|delete|where|union|convert|top|and|;|drop table|show tables|char|#|\*|'|\"|--/i", "$pageURL")) {


//pega a url
$pageURL = urldecode($pageURL);

//pega o navegador
$useragent = $_SERVER['HTTP_USER_AGENT'];

$ip=getenv("REMOTE_ADDR");

$sql = $db->query("insert into TB_LogInvasao (Ip, DataHora, Url, UserAgent, IdUsuario) Values ('$ip',NOW() '$pageURL','$useragent','".$_SESSION["dados_evolucao"]["idusuario"]."')");

                        //se não for localhost manda um aviso sobre a invasão
						if($_SERVER['HTTP_HOST'] != "127.0.0.1"){

						$ip=getenv("REMOTE_ADDR");

						$email       = 'webmaster@bage.rs.gov.br';
						$assunto     = 'Tentativa de invasao do sistema: Webcontas - SEFAZ';
						$assunto	 = utf8_decode($assunto);
						$pageURL	 = urldecode($pageURL);
						$data        = date("d/m/y");
						$hora        = date("H:i");
						mail ("alerta.nti@bage.rs.gov.br", "$assunto", "Ip do invasor: $ip\nData: $data\nHora: $hora\n\nUser agent: $useragent\n\nURL: $pageURL", "FROM: $email"," -f alerta.nti@bage.rs.gov.br");

						//mostra um aviso no site
					  echo"
					  <script type='text/javascript'>
					   alert('Nucleo de Tecnologia da Informacao \\n \\n CODIGO MALICIOSO DETECTADO! \\n \\n Usuario: ".$_SESSION["dados_evolucao"]["nome"]." \\n\\n Ip: ".$ip." \\n\\n O administrador do sistema sera notificado. ');
					  </script>
					   ";
						}
}
*/
#Fim script de aviso de invasão

?>
