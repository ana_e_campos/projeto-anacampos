<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["musculos"]["cadastrar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $titulo        = isset($_POST["titulo"]) ? filtra($_POST["titulo"]) : "";

  // insere no banco o musculo
  $bind = array($titulo);
	$sql_musculo = $db->query("INSERT INTO musculo (titulo) VALUES (?)", $bind);
  //print $sql_musculo; break;

  // insere no log o musculo
  salvaLog($db->mostraquery("INSERT INTO musculo (titulo) VALUES (?)", $bind));

  if($sql_musculo){
    // inserido com sucesso
    header("Location: ../admin.php?action=musculos&status=1");
  }
  else{
    // erro ao inserir
    header("Location: ../admin.php?action=musculos&status=3");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
