<?php
ob_start();
require_once('connection.php');

if(!isset($_SESSION["dados_evolucao"])){
  header("Location: ../logout.php");
	exit;
}

//VERIFICA A PERMISSÃO
if($_SESSION["equipes"]["cadastrar"] == 1){

if(getenv("REQUEST_METHOD") == "POST"){

  $nome        = isset($_POST["nome"]) ? filtra($_POST["nome"]) : "";
  $nascimento  = isset($_POST["nascimento"]) ? data_en($_POST["nascimento"]) : NULL;
  $celular    = isset($_POST["celular"]) ? filtra($_POST["celular"]) : "";

  $email       = isset($_POST["email"]) ? filtra(strtolower($_POST["email"])) : "";
  $idperfil    = isset($_POST["idperfil"]) ? numero($_POST["idperfil"]) : "";
  $senha       = isset($_POST["senha"]) ? filtraLogin($_POST["senha"]) : "";

	// consulta o email no banco
	$bind = array($email);
	$sql_email = $db->query("SELECT email FROM usuario WHERE email = ? LIMIT 1", $bind);

	if($db->numRows($sql_email) == 0){

    require('wideimage/WideImage.php');
    $dir = "../media/";
    $fotos = isset($_FILES["foto"]) ? $_FILES["foto"] : false;
    //$fotoantiga	= $_POST['fotoantiga'];

    if(!empty($fotos['name'])){

      // renomeia o arquivo
      $foto_nome = $fotos['name'];
    	$foto_extensao = substr($foto_nome, 0, strripos($foto_nome, '.')); // get file extention
    	$foto_extensao = substr($foto_nome, strripos($foto_nome, '.')); // get file name
      $foto = md5($foto_nome).strtolower($foto_extensao);
      //print $foto;break;

        // novo formato de verificação do PHP (TYPE em desuso)
        $path_info = pathinfo($fotos['name']);
        if(!preg_match( '/^(jpeg|gif|jpeg|jpg|png)$/', strtolower($path_info['extension'])) || $fotos["size"] > 5000000){
          // formato de imagem não suportado
          header("Location: ../admin.php?action=equipes&status=4");
        exit;
        }
        else{
          // renomeia o arquivo, se necessário
          if(file_exists($dir.$foto)){
            $a = 1;
            while(file_exists($dir."[{$a}]{$foto}"))
              $a++;
            $foto = "[{$a}]{$foto}";
          }

          if(!@move_uploaded_file($fotos['tmp_name'], $dir.$foto)){

            // erro ao enviar a foto
            header("Location: ../admin.php?action=equipes&status=5");
            exit;
          }
          else{
            $img = WideImage::load($dir.$foto);
            $img = $img->resize(900, 900, 'inside');
            //$img = $img->resize('50%');
            //$marca = WideImage::load("imgs/marca.png");
            //$img = $img->merge($marca,'right','bottom');
            $img->saveToFile($dir.$foto);

            $img = WideImage::load($dir.$foto);
            $img = $img->resize(170, 170, 'inside')->crop('center', 'center', 150, 150);
            $img->saveToFile($dir.'mini_'.$foto);
          }
        }
    }
    // se não selecionou uma foto para o envio
    else{
      $foto = "sem-imagem.png";
    }

    // insere no banco
    $bind = array($nome,$nascimento,$celular,$email,$idperfil, $senha, $foto);
		$sql_equipe = $db->query("INSERT INTO usuario (nome,nascimento,celular,email,idperfil, senha, foto) VALUES (?, ?, ?, ?, ?, PASSWORD(?), ?)", $bind);
    //echo nl2br($db->getDebug());
    //print $sql_equipe; break;

    // insere no log
    salvaLog($db->mostraquery("INSERT INTO usuario (nome,nascimento,celular,email,idperfil, senha, foto) VALUES (?, ?, ?, ?, ?, 'XXXXX', ?)", $bind));

    if($sql_equipe){
      // inserido com sucesso
      header("Location: ../admin.php?action=equipes&status=1");
    }
    else{
      // erro ao inserir
      header("Location: ../admin.php?action=equipes&status=3");
    }
  }
  else{
    // erro de email
    header("Location: ../admin.php?action=equipes&status=2");
  }
}

} //FIM VERIFICA A PERMISSÃO

?>
