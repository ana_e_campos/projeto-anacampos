
		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=equipamentos">Equipamentos</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=equipamentosInsert">Cadastrar</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["equipamentos"]["cadastrar"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-tags"></i>
							<span class="break"></span>
							Equipamentos
						</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="php/equipamentoInsert.php" name="cadequipamento">
						  <fieldset>

								 <div class="control-group">
	 								<div class="control-label-bg">
	 									<label class="control-label text-bold">FOTO </label>
	 									<i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
	 								</div>
	 							</div>

	 							<div class="control-group">
	 									<label class="control-label">
	 										<img src="img/mini_sem-imagem.png" class="foto-mini" /><input class="input-file uniform_on" id="" type="file" name="foto">
	 									</label>
	 								<div class="controls">
	 								<?php /*
	 								<input type="checkbox" name="marca" /> Incluir marca d'água
	 								<img src="img/marca.png" class="foto-mini" />
	 								*/
	 								?>
	 								</div>
	 							</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label text-bold">DADOS GERAIS </label>
										</div>
									</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">Unidade </label>
										</div>
										<div class="controls">
											<select data-placeholder="Unidades cadastradas no sistema" id="selectError" data-rel="chosen" name="idunidade" style="width: 280px;">
												<optgroup label="">
													<option value=""></option>
													<?php
													$sql_unidade = $db->query("SELECT idunidade, unidade FROM unidade WHERE ativo = 1 ORDER BY unidade");
													while($linha_unidade = $db->fetchArray($sql_unidade)){
													?>
													<option value="<?php print($linha_unidade["idunidade"]); ?>"> <?php print($linha_unidade["unidade"]); ?></option>
													<?php
												} // fim while unidade
													?>
												</optgroup>
											</select>
										</div>
									</div>
									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label">Etiqueta </label>
										</div>
										<div class="controls">
											<input class="input-mini" id="etiqueta" type="text" name="etiqueta" maxlength="200" >
										</div>
									</div>
									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label">Descrição </label>
										</div>
										<div class="controls">
											<input class="input-xlarge" id="descricao" type="text" name="descricao" maxlength="200" >
										</div>
									</div>

							<div class="form-actions">
							  <input type="submit" id='btcadequipamento' class="btn btn-large btn-evolucao text-transform-none" value="Salvar">
								</form>
							  <a href="admin.php?action=equipamentos"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
