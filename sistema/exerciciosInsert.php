
		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=exercicios">Exercícios</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=exerciciosInsert">Cadastrar</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["exercicios"]["cadastrar"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>
							<i class="icon-resize-horizontal"></i>
							<span class="break"></span>
							Exercícios
						</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="php/exercicioInsert.php" name="cadexercicio">
						  <fieldset>

								<ul class="nav nav-tabs">

									<li class="active">
										<a href="#geral" data-toggle="tab">Dados gerais</a>
									</li>
									<li>
										<a href="#masculino" data-toggle="tab">Fotos masculinas</a>
									</li>
									<li>
										<a href="#feminino" data-toggle="tab">Fotos femininas</a>
									</li>
								</ul>

							<div class="tab-content">

							 <div class="tab-pane active" id="geral">

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label text-bold">DADOS GERAIS </label>
										</div>
									</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label" style="margin-left:0px;">Músculo ou grupo muscular </label>
										</div>
										<div class="controls">
											<select data-placeholder="Músculos cadastradas no sistema" id="selectError" data-rel="chosen" name="idmusculo" style="width: 280px;">
												<optgroup label="">
													<option value=""></option>
													<?php
													$sql_musculo = $db->query("SELECT idmusculo, titulo FROM musculo WHERE ativo = 1 ORDER BY titulo");
													while($linha_musculo = $db->fetchArray($sql_musculo)){
													?>
													<option value="<?php print($linha_musculo["idmusculo"]); ?>"> <?php print($linha_musculo["titulo"]); ?></option>
													<?php
												} // fim while musculo
													?>
												</optgroup>
											</select>
										</div>
									</div>

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label">Título </label>
										</div>
										<div class="controls">
											<input class="input-xlarge" id="titulo" type="text" name="titulo" maxlength="200" >
										</div>
									</div>

								</div> <!-- tab-pane  -->

								<div class="tab-pane" id="masculino">

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label text-bold">FOTOS MASCULINAS</label>
										</div>
									</div>

									<div class="control-group">
									 <div class="control-label-bg">
										 <label class="control-label text-bold">Movimento inicial </label>
										 <i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
									 </div>
								 </div>

								 <div class="control-group">
										 <label class="control-label">
											 <img src="img/mini_sem-imagem.png" class="foto-mini" /><input class="input-file uniform_on" id="" type="file" name="foto_m1">
										 </label>
								 </div>

								 <div class="control-group">
								  <div class="control-label-bg">
								 	 <label class="control-label text-bold">Movimento final </label>
								 	 <i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
								  </div>
								 </div>

								 <div class="control-group">
								 	 <label class="control-label">
								 		 <img src="img/mini_sem-imagem.png" class="foto-mini" /><input class="input-file uniform_on" id="" type="file" name="foto_m2">
								 	 </label>

								 </div>

								</div> <!-- tab-pane  -->

								<div class="tab-pane" id="feminino">

									<div class="control-group">
										<div class="control-label-bg">
											<label class="control-label text-bold">FOTOS FEMININAS</label>
										</div>
									</div>

									<div class="control-group">
									 <div class="control-label-bg">
										 <label class="control-label text-bold">Movimento inicial </label>
										 <i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
									 </div>
								 </div>

								 <div class="control-group">
										 <label class="control-label">
											 <img src="img/mini_sem-imagem.png" class="foto-mini" /><input class="input-file uniform_on" id="" type="file" name="foto_f1">
										 </label>
								 </div>

								 <div class="control-group">
								  <div class="control-label-bg">
								 	 <label class="control-label text-bold">Movimento final </label>
								 	 <i class="halflings-icon question-sign" data-rel="tooltip" data-original-title="Formatos permitidos: JPG ou PNG. Tamanho máximo: 5 MB."></i>
								  </div>
								 </div>

								 <div class="control-group">
								 	 <label class="control-label">
								 		 <img src="img/mini_sem-imagem.png" class="foto-mini" /><input class="input-file uniform_on" id="" type="file" name="foto_f2">
								 	 </label>

								 </div>

								</div> <!-- tab-pane  -->

							</div> <!-- tab-content -->

							<div class="form-actions">
							  <input type="submit" id='btcadexercicio' class="btn btn-large btn-evolucao text-transform-none" value="Salvar">
								</form>
							  <a href="admin.php?action=exercicios"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
