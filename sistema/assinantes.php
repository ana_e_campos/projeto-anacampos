
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="admin.php?action=assinantes">Assinante</a></li>
			</ul>

			<?php include_once('include/status.php'); ?>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["assinantes"]["ver"] == 1 || $_SESSION["assinante"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-user"></i>
							<span class="break"></span>
							Assinante
							<?php
							//VERIFICA A PERMISSÃO
							if($_SESSION["assinantes"]["cadastrar"] == 1){
							?>
								<a data-rel="tooltip" data-original-title="Novo cadastro" href="admin.php?action=assinantesInsert">
									<i class="halflings-icon white plus"></i>
								</a>
							<?php
								} //FIM VERIFICA A PERMISSÃO
							?>
						</h2>
						<div class="box-icon">
							<a href="relatorios.php?tipo=Assinantes"
							data-rel="tooltip" data-original-title="Gerar PDF"><i class="halflings-icon white white download-alt"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th style="width: 50px;">Foto</th>
								  <th>Assinante</th>
								  <th>Ações</th>
							  </tr>
						  </thead>
						  <tbody>

								<?php

								//VERIFICA A PERMISSÃO
								if($_SESSION["assinante"]["ver"] == 1){
									$where = " AND u.idusuario = ".$_SESSION["dados_evolucao"]["idusuario"]."";
								}


									$sql_assinantes = $db->query("SELECT u.*, p.plano, p.valor, p.desconto
																							FROM usuario u
																							LEFT JOIN plano p
																							ON u.idplano = p.idplano
																							WHERE u.ativo = 1 AND u.tipo = 'Assinante' ".$where."
																							GROUP BY u.idusuario
																							ORDER BY u.nome");
									#print $sql_assinantes;break;
									while($linha_assinantes = $db->fetchArray($sql_assinantes)){
								?>

								<tr>
									<td>
										<span id="gallery" class="gallery">
											<a href="media/<?php print($linha_assinantes["foto"]); ?>" rel="prettyPhoto" title="APUS Digital - Sistema web">
												<img src="media/mini_<?php print($linha_assinantes["foto"]); ?>" class="foto-mini" data-original="media/<?php print($linha_assinantes["foto"]); ?>" alt="" />
											</a>
										</span>
										<br />
									</td>
									<td style="vertical-align: middle;">
										<i class="icon-user"></i>
										<?php print_db($linha_assinantes["nome"]); ?>  <br />
										<span class="vermelho"><strong><?php print_db($linha_assinantes["email"]); ?></strong></span> <br />
										<i class="icon-phone-sign"></i>
										<?php print($linha_assinantes["celular"]); ?>
										<br />
										Plano:
										<strong class="azul"><?php print($linha_assinantes["plano"])." - R$ ".valor($linha_assinantes["valor"]); ?></strong>
										<br />
										Desde:
										<strong><?php print(data_br($linha_assinantes["datahora"])); ?></strong>
										<br />
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["assinantes"]["editar"] == 1){
										?>
										<?php
											if($linha_assinantes["tentativas"] < 10){
										?>
											Acesso: <strong>Permitido</strong>
										<?php
									   }else{
										?>
												Acesso: <strong class='vermelho'>Senha bloqueada</strong>
												<br />

												<a data-rel="tooltip" data-original-title="Desbloquear acesso" onclick="return confirm('Confirma o desbloqueio?');" href="php/assinanteAccess.php?idusuario=<?php print(numero($linha_assinantes['idusuario'])); ?>">
													Clique aqui para desbloquear
												</a>
												<?php
													} //FIM VERIFICA A PERMISSÃO
												?>
										<?php
											}
										?>
									</td>

									<td style="vertical-align: middle;">
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["assinantes"]["editar"] == 1 || $_SESSION["assinante"]["editar"] == 1){
										?>
										<a class="btn btn-evolucao" data-rel="tooltip" data-original-title="Perfil" href="admin.php?action=assinantesUpdate&idusuario=<?php print(numero($linha_assinantes["idusuario"])); ?>">
											Perfil
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["assinantes"]["ver"] == 1 || $_SESSION["assinante"]["ver"] == 1){
										?>
										<a class="btn btn-evolucao" data-rel="tooltip" data-original-title="Endereços cadastrados" href="admin.php?action=enderecos&idusuario=<?php print($linha_assinantes["idusuario"]); ?>">
											Endereços
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["assinantes"]["ver"] == 1 || $_SESSION["assinante"]["ver"] == 1){
										?>
										<a class="btn btn-evolucao" data-rel="tooltip" data-original-title="Serviços" href="admin.php?action=solicitacoes&idusuario=<?php print($linha_assinantes["idusuario"]); ?>">
											Solicitações
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["faturas"]["ver"] == 1 || $_SESSION["fatura"]["ver"] == 1){
										?>
										<a class="btn btn-evolucao" data-rel="tooltip" data-original-title="Faturas" href="admin.php?action=faturas&idusuario=<?php print($linha_assinantes["idusuario"]); ?>">
											Faturas
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
										<?php
										/*
										//VERIFICA A PERMISSÃO
										if($_SESSION["extratos"]["ver"] == 1 || $_SESSION["extrato"]["ver"] == 1){
										?>
										<a class="btn btn-evolucao" data-rel="tooltip" data-original-title="Extrato" href="admin.php?action=extrato">
											Extrato
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
											*/
										?>
										<?php
										//VERIFICA A PERMISSÃO
										if($_SESSION["assinantes"]["excluir"] == 1){
										?>
										<a class="btn btn-success" data-rel="tooltip" data-original-title="Assumir identidade" onclick="return confirm('Confirma a troca?');" href="php/acessoSelect.php?idusuario=<?php print_db($linha_assinantes["idusuario"]); ?>">
											<i class="icon-user"></i>
										</a>

										<a class="btn btn-danger" data-rel="tooltip" data-original-title="Excluir" onclick="return confirm('Confirma a exclusão?');" href="php/assinanteDelete.php?idusuario=<?php print_db($linha_assinantes["idusuario"]); ?>">
											<i class="halflings-icon white trash"></i>
										</a>
										<?php
											} //FIM VERIFICA A PERMISSÃO
										?>
									</td>
								</tr>

								<?php
									} // fecha consulta assinantes
								?>

						  </tbody>
					  </table>
					</div>
				</div><!--/span-->

					<a href="admin.php?action=inicio" style="float:right;"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

</div><!--/fluid-row-->
