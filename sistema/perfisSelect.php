<?php

	$idperfil = isset($_GET["idperfil"]) ? numero($_GET["idperfil"]) : "";
	$bind = array($idperfil);
	$sql_perfil = $db->query("SELECT * FROM perfil WHERE idperfil = ? AND ativo = 1 LIMIT 1", $bind);
	//print $sql_perfil;break;

	$linha_perfil = $db->fetchArray($sql_perfil);
?>
		<!-- start: Content -->
		<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-sitemap"></i>
					<a href="admin.php?action=inicio">Início</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=perfis">Perfis</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="admin.php?action=perfisSelect&idperfil=<?php print_db($idperfil); ?>">Ver</a>
				</li>
			</ul>

			<?php
			//VERIFICA A PERMISSÃO
			if($_SESSION["perfis"]["ver"] == 1){
			?>

			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-key"></i><span class="break"></span>
							Perfis
						</h2>

					</div>

					<div class="box-content">
						<form class="form-inline" onsubmit="ShowLoading();" method="post" enctype="multipart/form-data" action="" name="cadperfil">

						  <fieldset>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">DADOS GERAIS </label>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
							    <label class="control-label">Perfil </label>
							  </div>
							  <div class="controls">
									<input disabled class="input-xlarge" id="perfil" type="text" name="perfil" value="<?php print_db($linha_perfil["perfil"]); ?>" maxlength="50" >
							  </div>
							</div>

							<div class="control-group">
								<div class="control-label-bg">
									<label class="control-label text-bold">NÍVEIS DE ACESSO </label>
								</div>
							</div>

							<div class="control-group">
							  <div class="controls">
									<table class="table table-bordered bootstrap-datatable">
									  <thead>
										  <tr>
												<th width="50">Permissão</th>
												<th width="50">Ver</th>
												<th width="50">Cadastrar</th>
												<th width="50">Editar</th>
												<th width="50">Excluir</th>
										  </tr>
									  </thead>
									  <tbody>

											<?php

											$sql_paginas = $db->query("SELECT idpagina, pagina FROM pagina WHERE ativo = 1 ORDER BY pagina");
											//print $sql_paginas;break;
											while($linha_paginas = $db->fetchArray($sql_paginas)){

												// consulta as permissões separadamente, pois a inclusão de páginas é dinâmica
												$bind = array($linha_paginas["idpagina"], $idperfil);
												$sql_permissoes = $db->query("SELECT ver, cadastrar, editar, excluir FROM permissao WHERE ativo = 1 AND idpagina = ? AND idperfil = ? LIMIT 1", $bind);
												//print $sql_permissoes;break;
												$linha_permissoes = $db->fetchArray($sql_permissoes);

											?>

											<tr>
												<td style="vertical-align: middle;"><?php print_db(($linha_paginas["pagina"])); ?></td>
												<input type="hidden" value="<?php print_db($linha_paginas["idpagina"]); ?>" name="idpagina[]" />
												<td style="vertical-align: middle;">
													<input disabled type="checkbox" name="ver[]" <?php if($linha_permissoes["ver"] == '1') print('checked'); ?> />
												</td>
												<td style="vertical-align: middle;">
													<input disabled type="checkbox" name="cadastrar[]" <?php if($linha_permissoes["cadastrar"] == '1') print('checked'); ?> />
												</td>
												<td style="vertical-align: middle;">
													<input disabled type="checkbox" name="editar[]" <?php if($linha_permissoes["editar"] == '1') print('checked'); ?> />
												</td>
												<td style="vertical-align: middle;">
													<input disabled type="checkbox" name="excluir[]" <?php if($linha_permissoes["excluir"] == '1') print('checked'); ?> />
												</td>
											</tr>

											<?php
										} // fecha consulta paginas
											?>

									  </tbody>
								  </table>
							  </div>
							</div>

							<div class="form-actions">
								</form>
							  <a href="admin.php?action=perfis"><span class="btn btn-large btn-inverse" data-loading-text="<i class='icon-repeat icon-spin'></i> ">Voltar</span></a>
							</div>
						  </fieldset>


					</div>
				</div><!--/span-->

			</div><!--/row-->

			<?php
			} //FIM VERIFICA A PERMISSÃO
			else{
				include_once('include/permissao.php');
			}
			?>

		</div><!--/fluid-row-->
