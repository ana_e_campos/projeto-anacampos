<?php
ob_start();  //inicia o buffer

require_once('php/connection.php');

//oculta os erros php (devido aos headers do mPDF)
error_reporting(0); ini_set("display_errors",0);
//error_reporting(1); ini_set("display_errors",1);

if(!isset($_SESSION["dados_evolucao"])){
        header("Location: logout.php");
		exit;
}

$dados = isset($_SESSION["dados_evolucao"]) ? $_SESSION["dados_evolucao"] : "";

  echo'
  <!DOCTYPE html>
  <html lang="en">
  <head>

  	<!-- start: Meta -->
  	<meta charset="utf-8">
  	<title>APUS Digital - Sistema web</title>
  	<!-- end: Meta -->

  	<!-- start: Mobile Specific -->
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- end: Mobile Specific -->

  	<!-- start: CSS -->
  	<link id="bootstrap-style" href="css/bootstrap.css" rel="stylesheet">
  	<link href="css/bootstrap-responsive.css" rel="stylesheet">
  	<link id="base-style" href="css/style.css" rel="stylesheet">
  	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
  	<link href="font/fonts.css" rel="stylesheet" type="text/css">
  	<!-- end: CSS -->

  	<!--[if IE 9]>
  		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
  	<![endif]-->

    <!-- start: Favicon -->
  	<link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
  	<link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
  	<link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
  	<link rel="manifest" href="img/favicons/site.webmanifest">
  	<link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  	<meta name="msapplication-TileColor" content="#603cba">
  	<meta name="theme-color" content="#ffffff">
  	<!-- end: Favicon -->

  </head>

  <body style="background-color: #fff;">
  ';

// tipo de relatório
$tipo = isset($_GET["tipo"]) ? filtra($_GET["tipo"]) : "";

// subtipo de relatório
$subtipo = isset($_GET["subtipo"]) ? filtra($_GET["subtipo"]) : "";

// INÍCIO: Equipes #############
if($tipo == "Equipes"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Equipes&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["equipes"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 50px;">Foto</th>
            <th>Membro</th>
          </tr>
        </thead>
        <tbody>
'));

          //VERIFICA A PERMISSÃO
          if($_SESSION["equipe"]["ver"] == 1){
            $where = " AND u.idusuario = ".$_SESSION["dados_evolucao"]["idusuario"]."";
          }

            $color = 0;

            $sql_equipes = $db->query("SELECT u.*, f.funcao, p.perfil
                                        FROM usuario u
                                        LEFT JOIN perfil p
                                        ON u.idperfil = p.idperfil
                                        LEFT JOIN funcao f
                                        ON u.idfuncao = f.idfuncao
                                        WHERE u.ativo = 1 AND u.tipo = 'Equipe' ".$where."
                                        GROUP BY u.idusuario
                                        ORDER BY u.nome");
            #print $sql_equipes;break;
            while($linha_equipes = $db->fetchArray($sql_equipes)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

            if($linha_equipes["tentativas"] <= 10)
              $acesso = "Acesso: <strong>Permitido</strong>";
            else
              $acesso = "Acesso: <strong class='vermelho'>Senha bloqueada</strong>";

print(utf8_decode('

          <tr '.$row.'>
            <td>
              <span id="gallery" class="gallery">
                  <img src="media/mini_'.($linha_equipes["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
              </span>
            </td>
            <td style="vertical-align: middle;">
              '.$linha_equipes["nome"].' <br />
              <strong>Função: '.$linha_equipes["funcao"].'</strong> <br />
              <span class="vermelho"><strong>Login: '.$linha_equipes["email"].'</strong></span> <br />
              '.($linha_equipes["celular"]).'
              <br />
              <span class="label label-apus negrito">
                '.($linha_equipes["perfil"]).'
              </span>
              <br />
              '.($acesso).'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta equipe

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_equipes)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Equipes #############

// INÍCIO: Assinantes #############
if($tipo == "Assinantes"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Assinantes&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["assinantes"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 50px;">Foto</th>
            <th>Assinante</th>
            <th>Plano</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_assinantes = $db->query("SELECT u.*, f.funcao, p.perfil
                                        FROM usuario u
                                        LEFT JOIN perfil p
                                        ON u.idperfil = p.idperfil
                                        LEFT JOIN funcao f
                                        ON u.idfuncao = f.idfuncao
                                        WHERE u.ativo = 1 AND u.tipo = 'Assinante'
                                        GROUP BY u.idusuario
                                        ORDER BY u.nome");
            #print $sql_assinantes;break;
            while($linha_assinantes = $db->fetchArray($sql_assinantes)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

            if($linha_assinantes["tentativas"] < 10)
              $acesso = "Acesso: <strong>Permitido</strong>";
            else
              $acesso = "Acesso: <strong class='vermelho'>Senha bloqueada</strong>";

print(utf8_decode('

          <tr '.$row.'>
            <td>
              <span id="gallery" class="gallery">
                  <img src="media/mini_'.($linha_assinantes["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
              </span>
            </td>
            <td style="vertical-align: middle;">
              '.$linha_assinantes["nome"].' <br />
              <span class="vermelho"><strong>Login: '.$linha_assinantes["email"].'</strong></span> <br />
              '.($linha_assinantes["celular"]).'
              <br />
              <span class="label label-apus negrito">
                '.($linha_assinantes["perfil"]).'
              </span>
              <br />
               Assinante desde:
              <strong>
                '.data_br($linha_assinantes["datahora"]).'
              </strong>
              <br />
              '.($acesso).'
            </td>
            <td style="vertical-align: middle;">

            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta assinantes

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_equipes)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Assinantes #############

// INÍCIO: Solicitações #############
if($tipo == "Solicitacoes"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Solicitações&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["solicitacoes"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Assinante</th>
            <th>Serviço</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_solicitacoes = $db->query("SELECT so.*, se.servico, t.tipo AS tiposervico, e.nome AS tecnico, a.*, s.status
                                            FROM solicitacao so
                                            LEFT JOIN servico se
                                            ON so.idservico = se.idservico
                                            LEFT JOIN tipo t
                                            ON se.idtipo = t.idtipo
                                            LEFT JOIN usuario e
                                            ON so.idequipe = e.idusuario
                                            LEFT JOIN usuario a
                                            ON so.idassinante = a.idusuario
                                            LEFT JOIN status s
                                            ON so.idstatus = s.idstatus
                                            WHERE so.ativo = 1
                                            GROUP BY so.idsolicitacao
                                            ORDER BY so.datahora");
            //echo nl2br($db->getDebug());
            //print $sql_solicitacoes;break;
            while($linha_solicitacoes = $db->fetchArray($sql_solicitacoes)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

            if($linha_solicitacoes["idequipe"] != null)
              $tecnico = "<strong>".$linha_solicitacoes['tecnico']."</strong>";
            else
              $tecnico = "<strong>Em seleção</strong>";

print(utf8_decode('

          <tr '.$row.'>
            <td>
              '.$linha_solicitacoes["idsolicitacao"].'
            </td>
            <td style="vertical-align: middle;">
            <strong class="azul">'.$linha_solicitacoes["nome"].'</strong>
            <br />
            '.$linha_solicitacoes["email"].'
            <br />
            '.$linha_solicitacoes["celular"].'
            <br />
            '.$linha_solicitacoes["logradouro"].',
            '.$linha_solicitacoes["numero"].'. <br />
            '.$linha_solicitacoes["bairro"].'.
            '.$linha_solicitacoes["cidade"].' / '.$linha_solicitacoes["uf"].'
            <br />
            <strong class="azul">'.datahora_br($linha_solicitacoes["datahora"]).'</strong>
            </td>
            <td style="vertical-align: middle;">
            Tipo: <span class="label label-apus negrito">&nbsp;'.$linha_solicitacoes["tiposervico"].'&nbsp;</span>
            <br />
            <strong>'.$linha_solicitacoes["servico"].'</strong>
            <br />
            '.$linha_solicitacoes["descricao"].'
            <br />
            <strong>Técnico:</strong>
            '.$tecnico.'
            <br />
            Status atual:
            <span class="label label-inverse negrito">&nbsp;'.$linha_solicitacoes["status"].'&nbsp;</span>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta sql_solicitacoes

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_solicitacoes)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Solicitações #############

// INÍCIO: Andamentos #############
if($tipo == "Andamentos"){

// orientação
$orientacao = "P";

$idsolicitacao = isset($_GET["idsolicitacao"]) ? numero($_GET["idsolicitacao"]) : "";
$status = isset($_GET["status"]) ? filtra($_GET["status"]) : "";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Andamentos&nbsp;</span>
  <br />
  Solicitação:
  <span class="label label-apus negrito">&nbsp;'.$idsolicitacao.'&nbsp;</span>
  <br />
  Status atual:
  <span class="label label-apus negrito">&nbsp;'.$status.'&nbsp;</span>

</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["andamentos"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 50px;">Foto</th>
            <th>Andamento</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $bind = array($idsolicitacao);
            $sql_andamentos = $db->query("SELECT a.*, u.nome
                                            FROM andamento a
                                            LEFT JOIN solicitacao so
                                            ON a.idsolicitacao = so.idsolicitacao
                                            LEFT JOIN usuario u
                                            ON a.idusuario = u.idusuario
                                            WHERE a.ativo = 1 AND a.idsolicitacao = ?
                                            GROUP BY a.idandamento
                                            ORDER BY a.datahora", $bind);
            //echo nl2br($db->getDebug());
            //print $sql_andamentos;break;
            while($linha_andamentos = $db->fetchArray($sql_andamentos)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';


print(utf8_decode('

          <tr '.$row.'>
          <td>
            <span id="gallery" class="gallery">
                <img src="media/mini_'.($linha_andamentos["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
            </span>
          </td>
            <td style="vertical-align: middle;">
            <strong class="azul">'.$linha_andamentos["nome"].'</strong>
            <br />
            '.$linha_andamentos["descricao"].'
            <br />
            <strong class="azul">'.datahora_br($linha_andamentos["datahora"]).'</strong>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta sql_andamentos

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_andamentos)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Andamentos #############

// INÍCIO: Faturas #############
if($tipo == "Faturas"){

// orientação
$orientacao = "P";

$idusuario = isset($_GET["idusuario"]) ? numero($_GET["idusuario"]) : "";
$assinante = isset($_GET["assinante"]) ? filtra($_GET["assinante"]) : "";

// PEGA OS DADOS DO ASSINANTE
$bind = array($idusuario);
$sql_assinante = $db->query("SELECT * FROM usuario WHERE ativo = 1 AND tipo = 'Assinante' AND idusuario = ?", $bind);
#print $sql_assinante;break;
$linha_assinante = $db->fetchArray($sql_assinante);

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Faturas&nbsp;</span>
  <br />
  Assinante:
  <span class="label label-apus negrito">&nbsp;'.$linha_assinante["nome"].'&nbsp;</span>

</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["faturas"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Fatura</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $bind = array($idusuario);
            $sql_faturas = $db->query("SELECT f.*, u.*, p.plano, p.valor
                                            FROM fatura f
                                            LEFT JOIN plano p
                                            ON f.idplano = p.idplano
                                            LEFT JOIN usuario u
                                            ON f.idassinante = u.idusuario
                                            WHERE f.ativo = 1 AND f.idassinante = ? ".$where."
                                            GROUP BY f.idfatura
                                            ORDER BY f.vencimento", $bind);
            //echo nl2br($db->getDebug());
            //print $sql_faturas;break;
            while($linha_faturas = $db->fetchArray($sql_faturas)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

            if($linha_faturas["pagamento"] != null)
              $pagamento = data_br($linha_faturas["pagamento"]);
            else
              $pagamento = "Não registrado";

              if($linha_faturas["status"] == 'Paga')
                $status = '<span class="label label-success negrito">Paga</span>';

              if($linha_faturas["status"] == 'Vencida')
                $status = '<span class="label label-important negrito">Vencida</span>';

              if($linha_faturas["status"] == 'Aberta')
                $status = '<span class="label label-inverse negrito">Aberta</span>';


print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              '.$linha_faturas["idfatura"].'
            </td>
            <td style="vertical-align: middle;">
            <strong class="azul">'.$linha_faturas["nome"].'</strong>
            <br />
            <strong>'.$linha_faturas["plano"].' - R$ '.valor($linha_faturas["valor"]).'</strong>
            <br />
            Vencimento: '.data_br($linha_faturas["vencimento"]).'
            <br />
            Pagamento: '.$pagamento.'
            </td>
            <td style="vertical-align: middle;">
              '.$status.'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta sql_faturas

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_faturas)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Faturas #############

// INÍCIO: Perfis #############
if($tipo == "Perfis"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Perfis de acesso&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["perfis"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Perfil</th>
          </tr>
        </thead>
        <tbody>
'));


            $color = 0;

            $sql_perfis = $db->query("SELECT idperfil, perfil FROM perfil WHERE ativo = 1 ORDER BY perfil");
            #print $sql_perfis;break;
            while($linha_perfis = $db->fetchArray($sql_perfis)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              '.($linha_perfis["perfil"]).'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta perfis

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_perfis)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}


}
// FIM: Perfis #############

// INÍCIO: Tipos de finança #############
if($tipo == "Tipos de financa"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Tipos de finança&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["financastipo"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Tipo de finança</th>
          </tr>
        </thead>
        <tbody>
'));


            $color = 0;

            $sql_financas = $db->query("SELECT idfinancatipo, financatipo FROM financatipo WHERE ativo = 1 ORDER BY financatipo");
            #print $sql_financas;break;
            while($linha_financas = $db->fetchArray($sql_financas)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              '.($linha_financas["financatipo"]).'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta finanças

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_financas)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Tipos de finança #############

// INÍCIO: Funções #############
if($tipo == "Funcoes"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Funções&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["funcoes"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Função</th>
          </tr>
        </thead>
        <tbody>
'));


            $color = 0;

            $sql_funcoes = $db->query("SELECT idfuncao, funcao FROM funcao WHERE ativo = 1 ORDER BY funcao");
            #print $sql_funcoes;break;
            while($linha_funcoes = $db->fetchArray($sql_funcoes)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              '.($linha_funcoes["funcao"]).'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta funcoes

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_funcoes)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Funcoes #############

// INÍCIO: Departamentos #############
if($tipo == "Departamentos"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Departamentos&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["departamentos"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Departamento</th>
          </tr>
        </thead>
        <tbody>
'));


            $color = 0;

            $sql_departamentos = $db->query("SELECT iddepartamento, departamento FROM departamento WHERE ativo = 1 ORDER BY departamento");
            #print $sql_departamentos;break;
            while($linha_departamentos = $db->fetchArray($sql_departamentos)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              '.($linha_departamentos["departamento"]).'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta departamentos

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_departamentos)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Departamentos #############

// INÍCIO: Planos #############
if($tipo == "Planos"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Planos&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["planos"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Plano</th>
            <th>Valor</th>
          </tr>
        </thead>
        <tbody>
'));


            $color = 0;

            $sql_planos = $db->query("SELECT * FROM plano WHERE ativo = 1 ORDER BY plano");
            #print $sql_planos;break;
            while($linha_planos = $db->fetchArray($sql_planos)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              <strong>'.($linha_planos["plano"]).'</strong>
              <br />
              '.($linha_planos["descricao"]).'
            </td>
            <td style="vertical-align: middle;">
              <strong>R$ '.valor($linha_planos["valor"]).'</strong>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta planos

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_planos)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Planos #############

// INÍCIO: Tipos #############
if($tipo == "Tipos"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Tipos de serviço&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["tipos"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 800px;">Tipo de serviço</th>
          </tr>
        </thead>
        <tbody>
'));


            $color = 0;

            $sql_tipos = $db->query("SELECT * FROM tipo WHERE ativo = 1 ORDER BY tipo");
            #print $sql_tipos;break;
            while($linha_tipos = $db->fetchArray($sql_tipos)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              <strong>'.($linha_tipos["tipo"]).'</strong>
              <br />
              '.($linha_tipos["descricao"]).'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta tipos

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_tipos)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Tipos #############

// INÍCIO: Serviços #############
if($tipo == "Servicos"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Serviços&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["servicos"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Serviço</th>
            <th>Valor</th>
          </tr>
        </thead>
        <tbody>
'));


            $color = 0;

            $sql_servicos = $db->query("SELECT s.*, t.tipo
                                        FROM servico s
                                        LEFT JOIN tipo t
                                        ON s.idtipo = t.idtipo
                                        WHERE s.ativo = 1
                                        GROUP BY s.idservico
                                        ORDER BY s.servico");
            #print $sql_servicos;break;
            while($linha_servicos = $db->fetchArray($sql_servicos)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              <strong>'.($linha_servicos["servico"]).'</strong>
              <br />
              '.($linha_servicos["descricao"]).'
              <br />
              <span class="label label-apus negrito">'.$linha_servicos["tipo"].'</span>
            </td>
            <td style="vertical-align: middle;">
              <strong>'.valor($linha_servicos["valor"]).'</strong>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta serviços

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_servicos)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Serviços #############

// INÍCIO: Pacotes de serviço #############
if($tipo == "Pacotes"){

$plano = isset($_GET["plano"]) ? filtra($_GET["plano"]) : "";
$idplano = isset($_GET["idplano"]) ? numero($_GET["idplano"]) : "";

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Pacote de serviços&nbsp;</span>
  <br />
  Plano:
  <span class="label label-important negrito">&nbsp;'.$plano.'&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["planos"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Serviço</th>
          </tr>
        </thead>
        <tbody>
'));


            $color = 0;

            $bind = array($idplano);
            $sql_pacotes = $db->query("SELECT p.*, s.servico, s.descricao, t.tipo
                                            FROM pacote p
                                            LEFT JOIN servico s
                                            ON p.idservico = s.idservico
                                            LEFT JOIN tipo t
                                            ON s.idtipo = t.idtipo
                                            WHERE p.ativo = 1 AND s.ativo = 1 AND p.idplano = ?
                                            GROUP BY p.idpacote
                                            ORDER BY s.servico DESC", $bind);
            //echo nl2br($db->getDebug());
            //print $sql_pacotes;break;
            while($linha_pacotes = $db->fetchArray($sql_pacotes)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;">
              <strong>'.($linha_pacotes["servico"]).'</strong>
              <br />
              '.($linha_pacotes["descricao"]).'
              <br />
              <span class="label label-apus negrito">'.$linha_pacotes["tipo"].'</span>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta pacote

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_pacotes)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Pacote de serviços #############

// INÍCIO: Álbuns de fotos #############
if($tipo == "Albuns de fotos"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Álbuns de fotos&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["albuns"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 70px;">Capa</th>
            <th>Álbum</th>
            <th>Criação/Atualização</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_albuns = $db->query("SELECT a.*,
                                      COUNT(ab.idalbumfoto) AS fotos,
                                      MAX(CAST(ab.datahora AS CHAR)) AS atualizacao
                                      FROM album a
                                      LEFT JOIN albumfoto ab
                                      ON a.idalbum = ab.idalbum AND ab.ativo = 1
                                      WHERE a.ativo = 1
                                      GROUP BY a.idalbum
                                      ORDER BY a.datahora DESC");
            #print $sql_albuns;break;
            while($linha_albuns = $db->fetchArray($sql_albuns)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td>
              <span id="gallery" class="gallery">
                  <img src="media/mini_'.($linha_albuns["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
              </span>
              <br />
            '.($linha_albuns["fotos"]).' fotos
            </td>
            <td style="vertical-align: middle;">
              '.($linha_albuns["album"]).'
            </td>
            <td style="vertical-align: middle;">
              '.data_br($linha_albuns["datahora"]).'  -
              '.data_br($linha_albuns["atualizacao"]).'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta funcoes

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_albuns)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Álbuns de fotos #############

// INÍCIO: Fotos #############
if($tipo == "Fotos"){

// orientação
$orientacao = "P";

$idalbum = isset($_GET["idalbum"]) ? numero($_GET["idalbum"]) : "";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Fotos&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["albuns"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width:70px;">Foto</th>
            <th>Legenda</th>
            <th>Data</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $bind = array($idalbum);
            $sql_fotos = $db->query("SELECT a.idalbum, a.album, af.idalbumfoto, af.datahora, af.legenda, af.foto
                                            FROM album a
                                            LEFT JOIN albumfoto af
                                            ON a.idalbum = af.idalbum
                                            WHERE af.ativo = 1 AND a.idalbum = ?
                                            GROUP BY af.idalbumfoto
                                            ORDER BY af.datahora DESC", $bind);
            #print $sql_fotos;break;
            while($linha_fotos = $db->fetchArray($sql_fotos)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
            <td>
              <span id="gallery" class="gallery">
                  <img src="media/mini_'.($linha_fotos["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
              </span>
            </td>
            <td style="vertical-align: middle;">
              '.($linha_fotos["legenda"]).'
            </td>
            <td style="vertical-align: middle;">
              '.data_br($linha_fotos["datahora"]).'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta funcoes

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_fotos)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Fotos #############

// INÍCIO: Fornecedores #############
if($tipo == "Fornecedores"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Fornecedores&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["fornecedores"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 75px;">Foto</th>
            <th>Academia</th>
            <th>Contato</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_fornecedores = $db->query("SELECT u.*
                                            FROM usuario u
                                            WHERE u.ativo = 1 AND u.tipo = 'Fornecedor'
                                            GROUP BY u.idusuario
                                            ORDER BY u.nome");
            #print $sql_fornecedores;break;
            while($linha_fornecedores = $db->fetchArray($sql_fornecedores)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

            if($linha_fornecedores["cpf"] != "")
              $tipo_curto = '<span class="label label-apus negrito">P. física</span>';
            else
              $tipo_curto = '<span class="label label-apus negrito">P. jurídica</span>';

              if($linha_fornecedores["cpf"] != "")
                $tipo_completo = "CPF: ".$linha_fornecedores["cpf"];
              else
                $tipo_completo = "CNPJ: ".$linha_fornecedores["cnpj"];


print(utf8_decode('

          <tr '.$row.'>
            <td>
              <span id="gallery" class="gallery">
                  <img src="media/mini_'.($linha_fornecedores["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
              </span>
              <br />
              '.$tipo_curto.'
            </td>
            <td style="vertical-align: middle;">
            <strong>
              '.($linha_fornecedores["nome"]).'
              <br />
              '.$tipo_completo.'
               </strong>
               <br />
               '.$linha_fornecedores["logradouro"].', '.$linha_fornecedores["numero"].'
            </td>
            <td style="vertical-align: middle;">
            '.$linha_fornecedores["celular"].'
             <br />
            '.$linha_fornecedores["fixo"].'
             <br />
            '.$linha_fornecedores["email"].'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta fornecedores

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_fornecedores)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Fornecedores #############

// INÍCIO: Tarefas #############
if($tipo == "Tarefas"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Tarefas&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["tarefas"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 50px">Foto</th>
            <th>Membro</th>
            <th>Tarefa</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_tarefas = $db->query("SELECT t.*, u.nome, c.abreviatura, u.foto, u.telefone, u.tipo
                                       FROM tarefa t
                                       LEFT JOIN usuario u
                                       ON t.idusuario = u.idusuario
                                       WHERE t.ativo = 1
                                       GROUP BY t.idtarefa
                                       ORDER BY t.tarefa");
            #print $sql_tarefas;break;
            while($linha_tarefas = $db->fetchArray($sql_tarefas)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

            if($linha_tarefas["datafim"] != null)
              $fim = " à ".data_br($linha_tarefas["datafim"]);
             else
              $fim = " - Em andamento";

            if($linha_tarefas["prioridade"] == 'Urgente' && $linha_tarefas["datafim"] == null)
              $prioridade = '<strong>Prioridade: </strong> <span class="label label-important">Urgente</span>';

            if($linha_tarefas["prioridade"] == 'Alta' && $linha_tarefas["datafim"] == null)
              $prioridade = '<strong>Prioridade: </strong> <span class="label label-inverse">Alta</span>';

            if($linha_tarefas["prioridade"] == 'Normal' && $linha_tarefas["datafim"] == null)
              $prioridade = '<strong>Prioridade: </strong> <span class="label label-default">Normal</span>';

            if($linha_tarefas["prioridade"] == 'Baixa' && $linha_tarefas["datafim"] == null)
              $prioridade = '<strong>Prioridade: </strong> <span class="label label-info">Baixa</span>';

            if($linha_tarefas["datafim"] != null)
              $prioridade = '<span class="label label-success">Concluída</span>';


print(utf8_decode('

          <tr '.$row.'>
          <td>
            <span id="gallery" class="gallery">
                <img src="media/mini_'.($linha_tarefas["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
            </span>
          </td>
            <td style="vertical-align: middle;">
              '.$linha_tarefas["nome"].'
              <br />
              '.$linha_tarefas["celular"].'
              <br />
              <span class="label label-apus negrito">'.$linha_tarefas["tipo"].'</span>
            </td>
            <td style="vertical-align: middle;">
              <strong>'.$linha_tarefas["tarefa"].'</strong>
              <br />
              '.data_br($linha_tarefas["datainicio"]).'
              '.$fim.'

              <br />
              '.$prioridade.'

            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta tarefas

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_tarefas)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Tarefas #############

// INÍCIO: Aniversariantes #############
if($tipo == "Aniversariantes"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Aniversariantes&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["aniversariantes"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 50px;">Foto</th>
            <th>Nome</th>
            <th>Contato</th>
            <th>Data</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_aniversariantes = $db->query("SELECT u.*
                                      FROM usuario u
                                      WHERE u.ativo = 1 AND u.tipo != 'Fornecedor' AND MONTH(u.nascimento) = MONTH(NOW())
                                      GROUP BY u.idusuario
                                      ORDER BY DAY(u.nascimento) ASC");

            #print $sql_aniversariantes;break;
            while($linha_aniversariantes = $db->fetchArray($sql_aniversariantes)){


            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

print(utf8_decode('

          <tr '.$row.'>
          <td>
            <span id="gallery" class="gallery">
                <img src="media/mini_'.($linha_aniversariantes["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
            </span>
          </td>
            <td style="vertical-align: middle;">
              <strong>'.$linha_aniversariantes["nome"].'</strong> <br />
              <span class="label label-apus negrito">'.$linha_aniversariantes["tipo"].'</span>
            </td>
            <td style="vertical-align: middle;">
              '.$linha_aniversariantes["celular"].' <br />
              '.$linha_aniversariantes["email"].'
            </td>
            <td style="vertical-align: middle;">
              '.datacurta_br($linha_aniversariantes["nascimento"]).' -
              '.dianiver_br($linha_aniversariantes["nascimento"]).'
              </strong>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta tarefas

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_aniversariantes)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Aniversariantes #############

// INÍCIO: Patrimonios #############
if($tipo == "Patrimonios"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Equipamentos&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["equipamentos"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 50px;">Foto</th>
            <th>Descrição</th>
            <th>Lotação</th>
            <th>Aquisição/estado</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_equipamentos = $db->query("SELECT p.*, e.unidade
                                          FROM equipamento p
                                          LEFT JOIN unidade e
                                          ON p.idunidade = e.idunidade
                                          WHERE p.ativo = 1
                                          GROUP BY p.idequipamento
                                          ORDER BY p.idequipamento");
            #print $sql_equipamentos;break;
            //echo nl2br($db->getDebug());
            while($linha_equipamentos = $db->fetchArray($sql_equipamentos)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';


print(utf8_decode('

          <tr '.$row.'>
          <td>
            <span id="gallery" class="gallery">
                <img src="media/mini_'.($linha_equipamentos["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
            </span>
          </td>
            <td style="vertical-align: middle;">
            '.str_pad($linha_equipamentos["idequipamento"], 5, '0', STR_PAD_LEFT).'
            <br />
            <strong>'.$linha_equipamentos["descricao"].'</strong>
            <br />
            '.$linha_equipamentos["qtd"].' unidade(s)
            </td>
            <td style="vertical-align: middle;">
            '.$linha_equipamentos["unidade"].'
            </td>
            <td style="vertical-align: middle;">
            '.data_br($linha_equipamentos["aquisicao"]).'
            <br />
            <span class="label label-apus negrito">'.$linha_equipamentos["estado"].'</span>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta equipamentos

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_equipamentos)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Patrimonios #############

// INÍCIO: Unidades #############
if($tipo == "Unidades"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Unidades&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["unidades"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 50px;">Foto</th>
            <th>Escritório</th>
            <th>1º Responsável</th>
            <th>2º Responsável</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_unidades = $db->query("SELECT e.*, u.nome AS responsavel, u.tipo AS tipo, u2.nome AS responsavel2, u2.tipo AS tipo2
                                      FROM unidade e
                                      LEFT JOIN usuario u
                                      ON e.idusuario = u.idusuario
                                      LEFT JOIN usuario u2
                                      ON e.idusuario2 = u2.idusuario
                                      WHERE e.ativo = 1
                                      GROUP BY e.idunidade
                                      ORDER BY e.unidade");
            #print $sql_unidades;break;
            while($linha_unidades = $db->fetchArray($sql_unidades)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';


print(utf8_decode('

          <tr '.$row.'>
          <td>
            <span id="gallery" class="gallery">
                <img src="media/mini_'.($linha_unidades["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
                <br />
            </span>
          </td>
            <td style="vertical-align: middle;">
              <strong>'.$linha_unidades["unidade"].'</strong>
              <br />
              '.$linha_unidades["logradouro"].', '.$linha_unidades["numero"].'
              <br />
              <strong>Fundação: </strong><span class="label label-apus negrito">'.data_br($linha_unidades["datafundacao"]).' </span>
            </td>
            <td style="vertical-align: middle;">
              '.$linha_unidades["responsavel"].' <br />
              <strong>'.$linha_unidades["tipo"].'</strong>
            </td>
            <td style="vertical-align: middle;">
              '.$linha_unidades["responsavel2"].' <br />
              <strong>'.$linha_unidades["tipo2"].'</strong>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta unidades

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_unidades)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Unidades #############

// INÍCIO: Agendas #############
if($tipo == "Agendas"){

// orientação
$orientacao = "P";

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px; font-weight: bold;">
  Relatório:
  <span class="label label-apus negrito">&nbsp;Agendas&nbsp;</span>
</div>

<br />

'));

//VERIFICA A PERMISSÃO
if($_SESSION["agendas"]["ver"] == 1){

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 50px;">Foto</th>
            <th>Membro</th>
            <th>Agenda</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_agendas = $db->query("SELECT a.*, u.nome, u.foto, u.tipo, f.funcao
                                       FROM agenda a
                                       LEFT JOIN usuario u
                                       ON a.idusuario = u.idusuario
                                       LEFT JOIN funcao f
                                       ON u.idfuncao = f.idfuncao
                                       WHERE a.ativo = 1
                                       GROUP BY a.idagenda
                                       ORDER BY a.data DESC");
            //echo nl2br($db->getDebug());
            #print $sql_agendas;break;
            while($linha_agendas = $db->fetchArray($sql_agendas)){

            if(($color % 2) != 0) $row = 'style="background-color: #F9F9F9;"'; else $row = 'style="background-color: #FFF;"';

            if($linha_agendas["status"] == 'Agendado')
              $status = '<span class="label negrito label-success">Agendado</span>';

            if($linha_agendas["status"] == 'Cumprido')
              $status = '<span class="label negrito label-default">Cumprido</span>';

            if($linha_agendas["status"] == 'Justificado')
              $status = '<span class="label negrito label-inverse">Justificado</span>';


print(utf8_decode('

          <tr '.$row.'>
          <td>
            <span id="gallery" class="gallery">
                <img src="media/mini_'.($linha_agendas["foto"]).'" class="foto-mini" style="width: 50px;" alt="" />
            </span>
          </td>
            <td style="vertical-align: middle;">
              '.$linha_agendas["nome"].' <br />
              <strong>'.$linha_agendas["funcao"].'</strong>
            </td>
            <td style="vertical-align: middle;">
              <strong>'.$linha_agendas["descricao"].'</strong>
              <br />
              '.data_br($linha_agendas["data"]).' - '.hora($linha_agendas["hora"]).'
              <br />
              '.$status.'
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta agendas

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_agendas)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Agenda #############

// INÍCIO: Finanças #############
if($tipo == "Financas"){

// orientação
$orientacao = "P";

$inicio = isset($_GET["inicio"]) ? data_en($_GET["inicio"]) : date('Y-m-01');
$fim    = isset($_GET["fim"]) ? data_en($_GET["fim"]) : date('Y-m-t');

//VERIFICA A PERMISSÃO
if($_SESSION["financas"]["ver"] == 1){

// INÍCIO: FINANÇAS GERAL
if($subtipo == "GERAL"){

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px;">
  <strong> Relatório: </strong>
  <span class="label label-apus negrito">&nbsp;Finanças&nbsp;</span>
  <br />
  <strong>Tipo:</strong> Demonstrativo geral
  <br />
  <strong>Período:</strong>
  '.data_br($inicio).' à '.data_br($fim).'
</div>

<br />

'));

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th style="width: 250px">Tipo/Data</th>
            <th>Vínculo/Descrição</th>
            <th style="width: 120px; text-align:right;">Valor</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            // calcula o saldo atual geral
            $sql_entradas = $db->query("SELECT SUM(valor) AS entradas FROM financa WHERE ativo = 1 AND movimentacao = 'Entrada'");
            $linha_entradas = $db->fetchArray($sql_entradas);
            $sql_saidas = $db->query("SELECT SUM(valor) AS saidas FROM financa WHERE ativo = 1 AND movimentacao = 'Saída'");
            $linha_saidas = $db->fetchArray($sql_saidas);

            $saldo_atual = $linha_entradas["entradas"] - $linha_saidas["saidas"];

            //$pesquisa.=" AND (f.descricao LIKE '%".filtra($_GET['pesquisa'])."%' ";
            //$pesquisa.=" OR m.nome LIKE '%".filtra($_GET['pesquisa'])."%' ";
            //$pesquisa.=" OR ft.financatipo LIKE '%".filtra($_GET['pesquisa'])."%' ";
            //$pesquisa.=" OR u.unidade LIKE '%".filtra($_GET['pesquisa'])."%' ";
            //$pesquisa.=" OR f.movimentacao LIKE '%".filtra($_GET['pesquisa'])."%')";
            //$pesquisa.=" OR f.valor = '".valor($_GET['pesquisa'])."')";

            $sql_financas = $db->query("SELECT f.*, u.nome, ft.financatipo, e.unidade, d.iddepartamento, d.departamento
                                        FROM financa f
                                        LEFT JOIN usuario u
                                        ON f.idvinculo = u.idusuario
                                        LEFT JOIN financatipo ft
                                        ON f.idfinancatipo = ft.idfinancatipo
                                        LEFT JOIN unidade e
                                        ON f.idunidade = e.idunidade
                                        LEFT JOIN departamento d
                                        ON f.iddepartamento = d.iddepartamento
                                        WHERE f.ativo = 1 ".$pesquisa." AND f.data BETWEEN '".data_en($inicio)."' AND '".data_en($fim)."'
                                        GROUP BY f.idfinanca
                                        ORDER BY f.data ASC");
            //echo nl2br($db->getDebug());
            //print $sql_financas;break;

          while($linha_financas = $db->fetchArray($sql_financas)){

            if(($color % 2) != 0) $row = 'style="background-color: #FFF;"'; else $row = 'style="background-color: #F9F9F9;"';

            if($linha_financas["movimentacao"] == 'Entrada'){
              $font = "#578EBE";
              $label = 'info';
              $entradas += $linha_financas["valor"];
            }
            if($linha_financas["movimentacao"] == 'Saída'){
              $font = "#E25A59";
              $label = 'important';
              $saidas += $linha_financas["valor"];
            }

            if($linha_financas["iddepartamento"]!=null)
              $departamento = "(".$linha_financas["departamento"].")";



print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;color:'.$font.'">
              <strong>'.$linha_financas["financatipo"].'</strong> <br />
              '.data_br($linha_financas["data"]).'
            </td>
            <td style="vertical-align: middle;color:'.$font.'">
              '.$linha_financas["unidade"].' '.$departamento.' <br />
               <strong>'.$linha_financas["nome"].'</strong> <br />
               '.$linha_financas["descricao"].'
            </td>
            <td style="vertical-align: middle; text-align: right; color:'.$font.'">
              <span class="label label-'.$label.' negrito">R$ '.valor($linha_financas["valor"]).'</span>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta finanças

print(utf8_decode('

         <tr>
           <td colspan="3" style="vertical-align: middle;"></td>
         </tr>

         <tr>
           <td colspan="2" style="vertical-align: middle;">
             <br />
             Período:
             <span class="label label-inverse negrito">'.data_br($inicio).'</span> à
             <span class="label label-inverse negrito">'.data_br($fim).'</span> <br /><br />
             Entradas no período: <br />
             <span class="label label-info negrito">R$ '.valor($entradas).'</span><br />
             Saídas no período: <br />
             <span class="label label-important negrito">R$ '.valor($saidas).'</span><br />
             Saldo no período: <br />
             <span class="label label-inverse negrito">R$ '.valor($entradas-$saidas).'</span>
             <br /><br />
           </td>
           <td colspan="1" style="vertical-align: middle;">

             <strong>SALDO ATUAL:</strong>
             <span class="label label-inverse negrito">R$ '.valor($saldo_atual).'</span>

           </td>
         </tr>

'));

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_financas)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} // FIM: FINANÇAS GERAL

// INÍCIO: FINANÇAS POR TIPO (ENTRADAS)
if($subtipo == "ENTRADAS"){

// zera as entradas
$entradas = 0;

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px;">
  <strong> Relatório: </strong>
  <span class="label label-apus negrito">&nbsp;Finanças&nbsp;</span>
  <br />
  <strong>Tipo:</strong> Demonstrativo por tipo de finança (ENTRADAS)
  <br />
  <strong>Período:</strong>
  '.data_br($inicio).' à '.data_br($fim).'
</div>

<br />

'));

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Tipo</th>
            <th style="width: 120px; text-align:right;">Valor</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_financas = $db->query("SELECT f.*, SUM(f.valor) AS valor_entradas, ft.financatipo
                                        FROM financa f
                                        LEFT JOIN financatipo ft
                                        ON f.idfinancatipo = ft.idfinancatipo
                                        WHERE f.ativo = 1 AND f.movimentacao = 'Entrada' AND f.data BETWEEN'".data_en($inicio)."' AND '".data_en($fim)."'
                                        GROUP BY f.idfinancatipo
                                        ORDER BY ft.financatipo ASC");
            echo nl2br($db->getDebug());
            //print $sql_financas;break;

          while($linha_financas = $db->fetchArray($sql_financas)){

            if(($color % 2) != 0) $row = 'style="background-color: #FFF;"'; else $row = 'style="background-color: #F9F9F9;"';

            if($linha_financas["movimentacao"] == 'Entrada'){
              $font = "#578EBE";
              $label = 'info';
              $entradas += $linha_financas["valor_entradas"];
            }

print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;color:'.$font.'">
              <strong>'.$linha_financas["financatipo"].'</strong> <br />
            </td>
            <td style="vertical-align: middle; text-align: right; color:'.$font.'">
              <span class="label label-'.$label.' negrito">R$ '.valor($linha_financas["valor_entradas"]).'</span>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta finanças

print(utf8_decode('

         <tr>
           <td colspan="2" style="vertical-align: middle;"></td>
         </tr>

         <tr>
           <td colspan="2" style="vertical-align: middle; text-align: right;">
             <br />
             Total de entradas: <br />
             <span class="label label-info negrito">R$ '.valor($entradas).'</span><br />
             <br />
           </td>
         </tr>

'));

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_financas)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

} // FIM: FINANÇAS POR TIPO (ENTRADAS)

// INÍCIO: FINANÇAS POR TIPO (SAÍDAS)
if($subtipo == "SAÍDAS"){

// zera as saídas
$saidas = 0;

print(utf8_decode('

<br />

<div class="row" style="margin-left: 1px;">
  <strong> Relatório: </strong>
  <span class="label label-apus negrito">&nbsp;Finanças&nbsp;</span>
  <br />
  <strong>Tipo:</strong> Demonstrativo por tipo de finança (SAÍDAS)
  <br />
  <strong>Período:</strong>
  '.data_br($inicio).' à '.data_br($fim).'
</div>

<br />

'));

print(utf8_decode('

<div class="row-fluid">
  <div class="">

    <div class="">
      <table class="table table-striped bootstrap-datatable datatable table-condensed">
        <thead>
          <tr>
            <th>Tipo</th>
            <th style="width: 120px; text-align:right;">Valor</th>
          </tr>
        </thead>
        <tbody>
'));

            $color = 0;

            $sql_financas = $db->query("SELECT f.*, SUM(f.valor) AS valor_saidas, ft.financatipo
                                        FROM financa f
                                        LEFT JOIN financatipo ft
                                        ON f.idfinancatipo = ft.idfinancatipo
                                        WHERE f.ativo = 1 AND f.movimentacao = 'Saída' AND f.data BETWEEN'".data_en($inicio)."' AND '".data_en($fim)."'
                                        GROUP BY f.idfinancatipo
                                        ORDER BY ft.financatipo ASC");
            //echo nl2br($db->getDebug());
            //print $sql_financas;break;

          while($linha_financas = $db->fetchArray($sql_financas)){

            if(($color % 2) != 0) $row = 'style="background-color: #FFF;"'; else $row = 'style="background-color: #F9F9F9;"';

            if($linha_financas["movimentacao"] == 'Saída'){
              $font = "#E25A59";
              $label = 'important';
              $saidas += $linha_financas["valor_saidas"];
            }


print(utf8_decode('

          <tr '.$row.'>
            <td style="vertical-align: middle;color:'.$font.'">
              <strong>'.$linha_financas["financatipo"].'</strong> <br />
            </td>
            <td style="vertical-align: middle; text-align: right; color:'.$font.'">
              <span class="label label-'.$label.' negrito">R$ '.valor($linha_financas["valor_saidas"]).'</span>
            </td>
          </tr>

          '));
           $color++;
         } // fecha consulta finanças

print(utf8_decode('

         <tr>
           <td colspan="2" style="vertical-align: middle;"></td>
         </tr>

         <tr>
           <td colspan="2" style="vertical-align: middle; text-align: right;">
             <br />
             Total de saídas: <br />
             <span class="label label-important negrito">R$ '.valor($saidas).'</span><br />
             <br />
           </td>
         </tr>

'));

print(utf8_decode('

        </tbody>
      </table>
      '.($db->numRows($sql_financas)).' registro(s) encontrado(s)
    </div>
  </div><!--/span-->

</div><!--/row-->

'));

}// FIM: FINANÇAS POR TIPO (SAÍDAS)

} //FIM VERIFICA A PERMISSÃO
else{
  include_once('include/permissao.php');
}

}
// FIM: Finanças #############


print('
</body>
</html>
');

//echo $html; break;

$html = ob_get_clean();
// pega o conteudo do buffer, insere na variavel e limpa a mem&oacute;ria

$html = utf8_encode($html);
// converte o conteudo para uft-8

include("php/MPDF53/mpdf.php");
// inclui a classe

$mpdf = new mPDF();
// cria o objeto

$mpdf->allow_charset_conversion=true;
// permite a conversao (opcional)

$mpdf->charset_in='UTF-8';
// converte todo o PDF para utf-8

$mpdf->setAutoTopMargin = 'stretch';
$mpdf->setAutoBottomMargin = 'stretch';

// cabeçalho
$mpdf->SetHTMLHeader('
  <div style=" text-align:left; border-bottom: 1px solid #000000; padding-bottom:5px; background-color: #fff">
    <img src="img/logo.png" style="width:150px; float:left;"/>
    <div style="font-weight:bold; margin: -10px 0 0 5px;">
      <small style"font-size:10px;">
        Matriz: Avenida São Judas, 595. Bagé/RS <br />
        Fale conosco: www.servicodireto.net | contato@servicodireto.net | (53) 9 9128-5844
      </small>
    </div>

  </div>');

//rodapé
$mpdf->SetFooter(date('d/m/Y').' - '.($_SESSION["dados_evolucao"]["email"]).' | APUS Digital - Sistema web | {PAGENO}');

//orientação P (portrait - paisagem) ou L (landscape - retrato)
if($orientacao == "P") $mpdf->AddPage('P'); else $mpdf->AddPage('L');

//escreve definitivamente o conteudo no PDF
$mpdf->WriteHTML($html);

//define um nome para o arquivo PDF
$arquivo = 'APUS DIGITAL - Sistema web ('.$tipo.').pdf';

// gera o arquivo
$mpdf->Output($arquivo,'D');

// imprime na mesma página
//$mpdf->Output();

// finaliza o codigo
exit();

?>
