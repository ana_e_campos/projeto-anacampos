<?php
  if ($_GET['status'] == 1) {
    print'
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Cadastro efetuado com sucesso.</strong>
      </div>
    ';
  }
  if ($_GET['status'] == 2) {
    print'
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> email já cadastrado.
      </div>
    ';
  }
  if ($_GET['status'] == 3) {
    print'
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Opss:</strong> algo deu errado :( contate a Evolução Assessoria Esportiva, ok?!
      </div>
    ';
  }
  if ($_GET['status'] == 4) {
    print'
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Opss:</strong> formato de arquivo ou tamanho não suportados.
      </div>
    ';
  }
  if ($_GET['status'] == 5) {
    print'
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> arquivo não pode ser enviado.
      </div>
    ';
  }
  if ($_GET['status'] == 6) {
    print'
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Cadastro excluído com sucesso.</strong>
      </div>
    ';
  }
  if ($_GET['status'] == 7) {
    print'
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> contate a APUS Digital.
      </div>
    ';
  }
  if ($_GET['status'] == 8) {
    print'
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Cadastro editado com sucesso.</strong>
      </div>
    ';
  }

  if ($_GET['status'] == 9) {
    print'
      <div class="alert alert-error" style="font-size:15px; margin:5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> email ou senha errados.
      </div>
    ';
  }
  if ($_GET['status'] == 10) {
    print'
      <div class="alert alert-error" style="font-size:15px; margin:5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> senha bloqueada.
      </div>
    ';
  }
  if ($_GET['status'] == 11) {
    print'
      <div class="alert alert-success" style="font-size:15px; margin:5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Instruções enviadas para o seu e-mail.</strong> <br />
        Verifique a sua caixa de spam, se necessário.
      </div>
    ';
  }
  if ($_GET['status'] == 12) {
    print'
      <div class="alert alert-error" style="font-size:15px; margin:5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> e-mail não encontrado no sistema.
      </div>
    ';
  }
  if ($_GET['status'] == 13) {
    print'
      <div class="alert alert-success" style="font-size:15px; margin:5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Recuperação realizada com sucesso.</strong> <br />
        Para acessar o sistema, informe o email e a senha.
      </div>
    ';
  }
  if ($_GET['status'] == 14) {
    print'
      <div class="alert alert-error" style="font-size:15px; margin:5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> e-mail ou token não encontrados no sistema.
      </div>
    ';
  }

  if ($_GET['status'] == 15) {
    print'
      <div class="alert alert-error" style="font-size:15px; margin:5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> quantidade de arquivos maior que a permitido por envio.
      </div>
    ';
  }

  if ($_GET['status'] == 16) {
    print'
      <div class="alert alert-error" style="font-size:15px; margin:5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erro:</strong> tamanho total dos arquivos maior que o permitido por envio.
      </div>
    ';
  }
  if ($_GET['status'] == 17) {
    print'
      <div class="alert alert-success" style="width: 300px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Solicitação realizada. Tudo ok =)</strong>
      </div>
    ';
  }
  if ($_GET['status'] == 18) {
    print'
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Opss:</strong> formato de foto ou tamanho não suportados :(
      </div>
    ';
  }
  if ($_GET['status'] == 19) {
    print'
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Opss:</strong> sua foto não pode ser enviada :(
      </div>
    ';
  }
  if ($_GET['status'] == 20) {
    print'
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Uhull! Assinatura efetuada com sucesso :)</strong>
      </div>
    ';
  }

  if ($_GET['status'] == 21) {
    print'
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Opss:</strong> o e-mail cadastrado já consta na nossa plataforma. <br />
        <strong>Já era uma assinante? Sem problemas, entre em contato conosco :) </strong>
      </div>
    ';
  }

  if ($_GET['status'] == 22) {
    print'
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Opss!</strong> Código de segurança incorreto :( <br />
        <strong> Tente enviar a mensagem novamente, ok? =) </strong>
      </div>
    ';
  }

  if ($_GET['status'] == 23) {
    print'
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Opss!</strong> A mensagem não pode ser enviada no momento :(  <br />
        <strong> Por favor, tente o contato via telefone, ok?! =) </strong>
      </div>
    ';
  }

  if ($_GET['status'] == 24) {
    print'
      <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Uhull!</strong> Mensagem enviada com sucesso :)   <br />
        <strong> Entraremos em contato com você, ok?! =) </strong>
      </div>
    ';
  }



?>
