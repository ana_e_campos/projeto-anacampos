
	<!--Load the AJAX API-->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/geocodezip/geoxml3/master/polys/geoxml3.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/geocodezip/geoxml3/master/ProjectedOverlay.js"></script>

	<script type="text/javascript">

	google.load('visualization', '1', {'packages':['corechart', 'table', 'geomap']});
	var map;
	var labels = [];
	var layer;
	var tableid = 1499916;

	function initialize() {
	geocoder = new google.maps.Geocoder();
	map = new google.maps.Map(document.getElementById('map_canvas'), {
	center: new google.maps.LatLng(37.23032838760389, -118.65234375),
	zoom: 6,
	mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	layer = new google.maps.FusionTablesLayer(tableid);
	layer.setQuery("SELECT 'geometry' FROM " + tableid);
	layer.setMap(map);

	codeAddress();

	google.maps.event.addListener(map, "bounds_changed", function() {
	displayZips();
	});
	google.maps.event.addListener(map, "zoom_changed", function() {
	if (map.getZoom() < 11) {
	for (var i=0; i<labels.length; i++) {
	labels[i].setMap(null);
	}
	}
	});
	}

	function codeAddress() {
	var address = document.getElementById("address").value;
	geocoder.geocode( { 'address': address}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) {
	map.setCenter(results[0].geometry.location);
	var marker = new google.maps.Marker({
	map: map,
	position: results[0].geometry.location
	});
	if (results[0].geometry.viewport)
	map.fitBounds(results[0].geometry.viewport);
	} else {
	alert("Geocode was not successful for the following reason: " + status);
	}
	});
	}

	function displayZips() {
	//set the query using the current bounds
	var queryStr = "SELECT geometry, ZIP, latitude, longitude FROM "+ tableid + " WHERE ST_INTERSECTS(geometry, RECTANGLE(LATLNG"+map.getBounds().getSouthWest()+",LATLNG"+map.getBounds().getNorthEast()+"))";
	var queryText = encodeURIComponent(queryStr);
	var query = new google.visualization.Query('https://www.google.com/fusiontables/gvizdata?tq=' + queryText);
	// alert(queryStr);

	//set the callback function
	query.send(displayZipText);

	}


	var infowindow = new google.maps.InfoWindow();

	function displayZipText(response) {
	if (!response) {
	alert('no response');
	return;
	}
	if (response.isError()) {
	alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
	return;
	}
	if (map.getZoom() < 11) return;
	FTresponse = response;
	//for more information on the response object, see the documentation
	//https://code.google.com/apis/visualization/documentation/reference.html#QueryResponse
	numRows = response.getDataTable().getNumberOfRows();
	numCols = response.getDataTable().getNumberOfColumns();
	/* var queryStr = "SELECT geometry, ZIP, latitude, longitude FROM "+ tableid + " WHERE ST_INTERSECTS(geometry, RECTANGLE(LATLNG"+map.getBounds().getSouthWest()+",LATLNG"+map.getBounds().getNorthEast()+"))";
	*/
	/*
	var kml = FTresponse.getDataTable().getValue(0,0);
	// create a geoXml3 parser for the click handlers
	var geoXml = new geoXML3.parser({
	map: map,
	zoom: false
	});

	geoXml.parseKmlString("<Placemark>"+kml+"</Placemark>");
	geoXml.docs[0].gpolygons[0].setMap(null);
	map.fitBounds(geoXml.docs[0].gpolygons[0].bounds);
	*/
	// alert(numRows);
	// var bounds = new google.maps.LatLngBounds();
	for(i = 0; i < numRows; i++) {
	var zip = response.getDataTable().getValue(i, 1);
	var zipStr = zip.toString()
	while (zipStr.length < 5) { zipStr = '0' + zipStr; }
	var point = new google.maps.LatLng(
	parseFloat(response.getDataTable().getValue(i, 2)),
	parseFloat(response.getDataTable().getValue(i, 3)));
	// bounds.extend(point);
	labels.push(new InfoBox({
	content: zipStr
	,boxStyle: {
	border: "1px solid black"
	,textAlign: "center"
	,fontSize: "8pt"
	,width: "50px"
	}
	,disableAutoPan: true
	,pixelOffset: new google.maps.Size(-25, 0)
	,position: point
	,closeBoxURL: ""
	,isHidden: false
	,enableEventPropagation: true
	}));
	labels[labels.length-1].open(map);
	}
	}



	</script>
	<body onload="initialize();">

		<script></script>

	<form>
	<input id="address" type="hidden" value="Rua Antonio Flores, 535, Bairro Getulio Vargas, 96412-550, Bagé, RS, Brasil" ></input>
	<div id="map_canvas" style="width: 610px; height: 400px;"></div>
	</script>

	</body>
	</html>
